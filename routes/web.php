<?php
/*
|--------------------------------------------------------------------------
| Web Frontoffice Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/', 'WebController@getIndex')->name('home');
Route::get('/asociados', 'WebController@getIndex')->name('companies');
Route::get('/sobre-emaltur', 'WebController@getIndex')->name('about');

Route::get('/blog', 'WebController@getIndex')->name('posts');
Route::get('/felicidades', 'WebController@getIndex')->name('congratulations');
Route::get('/blog/{postSlug}', 'WebController@getIndexNews')->name('posts-profile');
Route::get('/demo', 'WebController@getIndex');
Route::get('/excel', 'SubscriptionController@getExcelOfSubscriptions')->name('excel');

//Social Login routes
Route::get('/facebook', 'Auth\SocialAuthController@redirectFacebook');
Route::get('/callback/facebook', 'Auth\SocialAuthController@callbackFacebook');
Route::get('/google', 'Auth\SocialAuthController@redirectGoogle');
Route::get('/callback/google', 'Auth\SocialAuthController@callbackGoogle');

/*
|--------------------------------------------------------------------------
| BackOffice Routes
|--------------------------------------------------------------------------
*/

Route::get('webadmin',
		['middleware' => ['guest'],
		'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('login','Auth\LoginController@authenticate');

Route::group(['middleware'=>['auth']],function(){
	Route::group(['prefix'=>'admin'],function(){

		// Upload Images
		Route::post('upload-dropzone','CompaniesController@postCoverImages')->name('upload');
		Route::post('upload-product-dropzone','ProductsController@postImages')->name('upload-product-images');
		Route::post('upload-associated-dropzone','AssociatedController@postImages')->name('upload-associated-images');
		Route::post('upload-company-dropzone','CompaniesController@postGalleryImages')->name('upload-company-images');
		Route::post('upload-post-dropzone','PostsController@postImages')->name('upload-post-images');


		Route::post('upload-service-dropzone','ServicesController@storeImage')->name('upload-service-images');

		//Rutas GET
		Route::get('empresa','CompaniesController@getViewCompany')->name('company');
		Route::get('suscripciones', 'SubscriptionController@getViewSubscriptions')->name('subscriptions');

		//Company
		Route::get('companies/first','CompaniesController@getCompanyToEdit');
		Route::post('companies','CompaniesController@postCompany');
		Route::put('companies','CompaniesController@putCompany');
		Route::put('companies/slogan','CompaniesController@putCompanySlogan');

		Route::post('companies/videos','CompaniesController@postCompanyVideo');
		Route::put('companies/videos','CompaniesController@putCompanyVideo');
		Route::delete('companies/videos','CompaniesController@deleteCompanyVideo');

		//Contents
		Route::get('contents/{contentId}','ContentsController@getContentById');
		Route::delete('contents','ContentsController@deleteContent');
		Route::post('contents/change-model-id','ContentsController@postChangeModelId');
		Route::get('contents/{modelId}/{contentId}/{type}','ContentsController@getContentsOfAItem');

		//Products
		Route::get('products', 'ProductsController@all');
		Route::put('products/{id}','ProductsController@update');
		Route::post('products','ProductsController@postProduct');
		Route::get('products/{productId}','ProductsController@getProductById');
		Route::delete('products/{id}','ProductsController@delete');
		Route::put('products/{id}/published','ProductsController@updatePublished');

		//Services
		Route::get('services','ServicesController@getServices');
		Route::get('services/{serviceId}','ServicesController@getServiceById');
		Route::post('services','ServicesController@postService');
		Route::put('services','ServicesController@putService');
		Route::delete('services','ServicesController@deleteService');
		Route::put('services/change-published-status','ServicesController@putChangePublishedStatus');

		//Customers
		Route::get('customers', 'CustomersController@getCustomers');
		Route::delete('customers/{id}', 'CustomersController@deleteCustomer');
		Route::get('customers/{id}', 'CustomersController@getCustomer');
		Route::put('customers', 'CustomersController@updateCustomer');
		Route::post('customers', 'CustomersController@saveCustomer');

		//Posts
		Route::get('posts','PostsController@getAllPosts');
		Route::get('posts/{postId}','PostsController@getPostById');
		Route::post('posts','PostsController@postPost');
		Route::put('posts','PostsController@putPost');
		Route::delete('posts','PostsController@deletePost');
		Route::put('posts/change-published-status','PostsController@putPostPublished');

		//Users
		Route::get('users', 'UsersController@getUsers');
		Route::put('users/{id}/suspend', 'UsersController@suspendUser');
		Route::put('users/{id}/activate', 'UsersController@activateUser');
		Route::post('users', 'UsersController@postUser');
		Route::put('users', 'UsersController@putUser');

		Route::get('users/{userId}','UsersController@getUser');
		Route::put('user-profile','UsersController@putProfileUser');
		Route::put('users/password','UsersController@putChangePassword');

		//Tags
		Route::get('tags', 'TagController@all');

		//Headquarters
		Route::get('headquarters', 'HeadQuarterController@all');
		Route::get('headquarters/{id}', 'HeadQuarterController@show');

		Route::post('headquarters', 'HeadQuarterController@store');
		Route::put('headquarters/{id}', 'HeadQuarterController@update');
		Route::delete('headquarters/{id}', 'HeadQuarterController@delete');
		Route::put('headquarters/{id}/published', 'HeadQuarterController@updatePublished');

		Route::get('activities', 'ActivityController@all');
		Route::get('activities/published', 'ActivityController@allPublished');
		Route::get('activities/{id}', 'ActivityController@show');
		Route::post('activities', 'ActivityController@store');
		Route::put('activities/{id}', 'ActivityController@update');
		Route::put('activities/{id}/published', 'ActivityController@updatePublished');
		Route::delete('activities/{id}', 'ActivityController@delete');

		#Associates companies
		Route::get('associates', 'AssociatedController@all');
		Route::get('associates/{id}', 'AssociatedController@show');
		Route::post('associates', 'AssociatedController@store');
		Route::put('associates/{id}', 'AssociatedController@update');
		Route::put('associates/{id}/published', 'AssociatedController@updatePublished');
		Route::delete('associates/{id}', 'AssociatedController@delete');
		Route::get('associates/{id}/seals', 'AssociatedController@getSeals');

		Route::get('seals', 'SealController@all');
		Route::get('seals/published', 'SealController@allPublished');
		Route::get('seals/{id}', 'SealController@show');
		Route::post('seals', 'SealController@store');
		Route::put('seals/{id}', 'SealController@update');
		Route::put('seals/{id}/published', 'SealController@updatePublished');
		Route::delete('seals/{id}', 'SealController@delete');

		//Subscriptions
		Route::get('subscriptions', 'SubscriptionController@all');
		Route::get('subscriptions/{id}', 'SubscriptionController@show');

  });
});

//Logout
Route::get('logout','Auth\LoginController@logout')->name('logout');
