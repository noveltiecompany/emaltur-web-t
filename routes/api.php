<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('home','LandingController@home');
Route::get('about','LandingController@about');

Route::get('companies','CompaniesController@allCompanies');
Route::get('activities','ActivityController@index');

Route::get('tags', 'TagController@allForApi');
Route::get('posts','PostsController@allForApi');
Route::get('posts/{slug}/related', 'PostsController@getRelatedForApi');
Route::get('posts/{slug}','PostsController@getBySlugForApi');

Route::post('contact','ContactsController@postContactFromLanding');
Route::post('subscription', 'SubscriptionController@store');
