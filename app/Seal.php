<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Seal extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'seals';

    protected $fillable = [ 'name', 'description'];

    public function companies()
    {
    	return $this->belongsToMany('App\Company', 'company_seal', 'seal_id', 'company_id');
    }

}
