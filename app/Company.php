<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
	use SoftDeletes;

	protected $fillable = [	'activity_id', 'name_company', 'representative', 'website', 'longitude', 'latitude', 'our_history', 'our_objectives', 'value_proposal', 'directors', 'benefits', 'requirements', 'video_cover', 'schedules',
	 								'description_company',
									 'vision',
									 'proposal_value',
									  'email',
									   'phone',
									    'cel',
									     'facebook',
										  'twitter',
										   'address',
										    'city',
										     'country',
											  'title_slogan',
											   'subtitle_slogan',
											   'google_plus',
											   	'youtube',
											   	'instagram',
											   	'pinterest',
											   	'linkedin'];

	 protected $dates = ['deleted_at'];

	 public function seals()
	 {
	 	return $this->belongsToMany('App\Seal', 'company_seal', 'company_id', 'seal_id');
	 }

	 public function activity()
	 {
	 	return $this->belongsTo('App\Activity');
	 }

	 public function images()
	 {
	 	return $this->hasMany('App\Content', 'model_id')->whereModelType(6)->whereType(1);
	 }

	 public function videos()
	 {
	 	return $this->hasMany('App\Content', 'model_id')->whereModelType(1)->whereType(2);
	 }
}
