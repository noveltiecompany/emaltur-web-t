<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
	use SoftDeletes;

    protected $fillable = [];

	protected $dates = ['deleted_at'];

	protected $table = "tags";

	public function posts()
	{
			return $this->hasMany('App\Post');
	}
}
