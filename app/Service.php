<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model
{
	use SoftDeletes;

    protected $fillable = ['name', 'title', 'price', 'steps', 'product_id', 'format', 'price_concept', 'description'];
    
	protected $dates = ['deleted_at'];
}
