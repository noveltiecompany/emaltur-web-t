<?php

namespace App\Http\Controllers;

use App\Company;
use App\Utils\MailchimpNewsletter;
use Illuminate\Http\Request;

class SubscribeController extends Controller
{

    public function subscribe(Request $request)
    {
        $company = Company::first();
        
        $data = [
            'email' => $request->email,
            'firstname' => $request->name,
            'lastname' => ''
        ];
        
        $newsletter = new MailchimpNewsletter();
        $newsletter->addEmailToList($data, $company->mailchimp_list_id, false);

        return response()->json(['success' => true], 200);
    }

}
