<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\Category;
use App\Subcategory;
use App\Uploaders\ImageUploader;
use App\Content;
use App\Http\Requests\ProductPromotionRequest;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProductUpdateRequest;
use Auth;
use SplFileInfo;

class ProductsController extends Controller
{
	protected $productDefaultImg;

	public function __construct()
	{
		$this->productDefaultImg = asset('static/images/product.png');
	}

	public function all()
	{
		$products = Product::select(['id', 'name', 'published', 'image_thumb'])->get();
		return $products;
	}

	public function postProduct(Request $request)
	{
		try {
			$data = $request->all();

			$data['slug'] = str_slug($data['name']);

			$product = new Product();
			$product->fill($data);
			$product->published = 1;

			if ($request->hasFile('product_image')) {

				$img = $request->product_image;

				$functionUpload = new ImageUploader();
				$functionUpload->upload('/products/images',$img,'png',400);

				$product->image = $functionUpload->getDropboxUrl();
				$product->image_path = $functionUpload->getDropboxPath();

				$functionUpload->upload('/products/images/thumbs',$img,'png',100);
				$product->image_thumb = $functionUpload->getDropboxUrl();
				$product->image_thumb_path = $functionUpload->getDropboxPath();
			}
			$product->save();
			return response()->json(['success'=>true],200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);
		}
	}

	public function update($id, Request $request)
	{
		try {
			$data = $request->all();
			$data['slug'] = str_slug($data['name']);
			$product = Product::find($id);
			$product->fill($data);

			if ($request->hasFile('product_image')) {

				$img = $request->product_image;

				$functionUpload = new ImageUploader();

				$functionUpload->upload('/products/images',$img,'png',950);
				$functionUpload->delete($product->image_path,$product->image);

				$product->image = $functionUpload->getDropboxUrl();
				$product->image_path = $functionUpload->getDropboxPath();

				$functionUpload->upload('/products/images/thumbs',$img,'png',450);
				$functionUpload->delete($product->image_thumb_path,$product->image_thumb);

				$product->image_thumb = $functionUpload->getDropboxUrl();
				$product->image_thumb_path = $functionUpload->getDropboxPath();
			}

			$product->save();
			return response()->json(['success'=>true],200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);
		}
	}

	public function getProductById($productId)
	{
		$product = Product::find($productId);
		return response()->json(['product'=>$product],200);
	}

	public function delete($id)
	{
		try {
			$product = Product::find($id);

			$functionUpload = new ImageUploader();

			$functionUpload->delete($product->image_path,$product->image);
			$functionUpload->delete($product->image_thumb_path,$product->image_thumb);

			$product->delete();
			return response()->json(['success'=>true],200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);
		}
	}

	public function updatePublished($id)
	{
		try {
			$product = Product::find($id);

			if ($product->published == true) {
				$product->published = false;
			} else if($product->published == false){
				$product->published = true;
			}
			$product->save();

			return response()->json(['success'=>true],200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);
		}
	}

	public function postImages(Request $request)
	{
		$img  = $request->file[0];

		$functionUpload = new ImageUploader();
		$functionUpload->upload('/products/images',$img,'png',900);

		$content  = new Content();
		$content->content = "Img dropzone products";
		$content->resource = $functionUpload->getDropboxUrl();
		$content->resource_path = $functionUpload->getDropboxPath();

		$functionUpload->upload('/products/images/thumbs',$img,'png',450);
		$content->resource_thumb = $functionUpload->getDropboxUrl();
		$content->resource_thumb_path = $functionUpload->getDropboxPath();
		$content->model_id = 0;
		$content->model_type = 2;
		$content->type = 1;
		$content->save();

		return $content;
	}


}
