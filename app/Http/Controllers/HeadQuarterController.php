<?php

namespace App\Http\Controllers;

use App\HeadQuarter;
use App\Uploaders\ImageUploader;
use Illuminate\Http\Request;

class HeadQuarterController extends Controller
{

	public function all()
	{
		$headQuaters = HeadQuarter::all();
		return $headQuaters;
	}

	public function store(Request $request)
	{
		try {
			$data = $request->all();

			$headquarter = new HeadQuarter();
			$headquarter->fill($data);
			$headquarter->published = 1;

			if ($request->hasFile('headquarter_image')) {

				$img = $request->headquarter_image;

				$functionUpload = new ImageUploader();
				$functionUpload->upload('/heradquarters/images',$img,'png',950);
					
				$headquarter->image = $functionUpload->getDropboxUrl();
				$headquarter->image_path = $functionUpload->getDropboxPath();

				$functionUpload->upload('/heradquarters/images/thumbs',$img,'png',450);
				$headquarter->image_thumb = $functionUpload->getDropboxUrl();
				$headquarter->image_thumb_path = $functionUpload->getDropboxPath();
			}
			$headquarter->save();

			return response()->json(['success' => true], 200);
		} catch (Exception $e) {
			return response()->json(['success' => false], 200);
		}

	}

	public function update($id, Request $request)
	{
		try {
			$data = $request->all();

			$headquarter = HeadQuarter::find($id);
			$headquarter->fill($data);

			if ($request->hasFile('headquarter_image')) {

				$img = $request->headquarter_image;

				$functionUpload = new ImageUploader();
				$functionUpload->upload('/heradquarters/images',$img,'png',950);
				$functionUpload->delete($headquarter->image_path, $headquarter->image);

				$headquarter->image = $functionUpload->getDropboxUrl();
				$headquarter->image_path = $functionUpload->getDropboxPath();

				$functionUpload->upload('/heradquarters/images/thumbs',$img,'png',450);
				$functionUpload->delete($headquarter->image_thumb_path, $headquarter->image_thumb);

				$headquarter->image_thumb = $functionUpload->getDropboxUrl();
				$headquarter->image_thumb_path = $functionUpload->getDropboxPath();
			}
			$headquarter->save();

			return response()->json(['success' => true], 200);
		} catch (Exception $e) {
			return response()->json(['success' => false], 200);
		}	
	}

	public function show($id)
	{
		$headquarter = HeadQuarter::find($id);
		return $headquarter;
	}

	public function delete($id)
	{
		try {

			$headquarter = HeadQuarter::find($id);
			$functionUpload = new ImageUploader();

			$functionUpload->delete($headquarter->image_path, $headquarter->image);
			$functionUpload->delete($headquarter->image_thumb_path, $headquarter->image_thumb);

			$headquarter->delete();

			return response()->json(['success' => true], 200);
		} catch (Exception $e) {
			return response()->json(['success' => false], 200);

		}
	}

	public function updatePublished($id)
	{
		try {
			$headquarter = HeadQuarter::find($id);
			
			if ($headquarter->published == true) {
				$headquarter->published = false;
			} else if ($headquarter->published == false){
				$headquarter->published = true;
			}

			$headquarter->save();
				
			return response()->json(['success' => true], 200);
		} catch (Exception $e) {
			return response()->json(['success' => false], 200);
		}

	}

}
