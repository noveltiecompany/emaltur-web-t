<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function all()
    {
    	$tags = Tag::all();
    	return $tags;
    }

    ##Metodos para la rutas API

    public function allForApi()
    {
    	$tags = Tag::whereHas('posts')->get();

    	$t = [];

    	foreach ($tags as $i => $tag) {
    		$t[] = array('slug' => $tag->slug, 'name' => $tag->name);
    	}
    	return response()->json(['data' => $t], 200);
    }


}
