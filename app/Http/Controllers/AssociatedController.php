<?php

namespace App\Http\Controllers;

use App\Company;
use App\Content;
use App\Seal;
use App\Uploaders\ImageUploader;
use Illuminate\Http\Request;

class AssociatedController extends Controller
{
	public function all()
	{
		$associates = Company::select(['id', 'logotype_thumb', 'name_company', 'published'])->where('type', 2)->get();

		return $associates;
	}

	public function show($id)
	{
		$associated = Company::find($id);
		return $associated;
	}

	public function store(Request $request)
	{
		$data = $request->all();

		$associated = new Company();
		$associated->fill($data);
		$associated->slug_company = str_slug($request->name_company);
		$associated->description_company = '';
		$associated->type = 2;

		if ($request->hasFile('image')) {

			$img = $request->image;

			$functionUpload = new ImageUploader();
			$functionUpload->upload('/company',$img,'png',950);

			$associated->logotype = $functionUpload->getDropboxUrl();
			$associated->logotype_path = $functionUpload->getDropboxPath();

			$functionUpload->upload('/company',$img,'png',450);
			$associated->logotype_thumb = $functionUpload->getDropboxUrl();
			$associated->logotype_thumb_path = $functionUpload->getDropboxPath();
		}

		$associated->save();

		if ($request->seals != 'null') {
			$seals_id_array = explode(',', $request->seals);

			foreach ($seals_id_array as $i => $id) {
				$associated->seals()->attach($id);
			}
		}

		return;
	}

	public function update($id, Request $request)
	{
		$data = $request->all();

		$associated = Company::find($id);
		$associated->fill($data);
		$associated->slug_company = str_slug($request->name_company);

		if ($request->hasFile('image')) {

			$img = $request->image;

			$functionUpload = new ImageUploader();
			$functionUpload->upload('/company',$img,'png',950);
			$functionUpload->delete($associated->logotype_path, $associated->logotype);
			$associated->logotype = $functionUpload->getDropboxUrl();
			$associated->logotype_path = $functionUpload->getDropboxPath();

			$functionUpload->upload('/company',$img,'png',450);
			$functionUpload->delete($associated->logotype_thumb_path, $associated->logotype_thumb);

			$associated->logotype_thumb = $functionUpload->getDropboxUrl();
			$associated->logotype_thumb_path = $functionUpload->getDropboxPath();
		}

		$associated->save();

		$associated->seals()->detach();
		if ($request->seals != 'null') {
			$seals_id_array = explode(',', $request->seals);

			foreach ($seals_id_array as $i => $id) {
				$associated->seals()->attach($id);
			}
		}

		return;
		return;
	}

	public function updatePublished($id)
	{
        $associated = Company::find($id);

        if ($associated->published == true) {
            $associated->published = false;
        } else if($associated->published == false){
            $associated->published = true;
        }
        $associated->save();
        return;
	}

	public function delete($id)
	{
		$associated = Company::find($id);
		$functionUpload = new ImageUploader();
		$functionUpload->delete($associated->logotype_path, $associated->logotype);
		$functionUpload->delete($associated->logotype_thumb_path, $associated->logotype_thumb);
		$functionUpload->delete($associated->logotype_white_path, $associated->logotype_white);
		$functionUpload->delete($associated->logotype_white_thumb_path, $associated->logotype_white_thumb);
		$associated->delete();
		return;
	}

	public function postImages(Request $request)
	{

		$img  = $request->file[0];

		$functionUpload = new ImageUploader();
		$functionUpload->upload('/associates/images',$img,'png',950);

		$content  = new Content();
		$content->content = "Img Associado";
		$content->resource = $functionUpload->getDropboxUrl();
		$content->resource_path = $functionUpload->getDropboxPath();

		$functionUpload->upload('/associates/images/thumbs',$img,'png',450);
		$content->resource_thumb = $functionUpload->getDropboxUrl();
		$content->resource_thumb_path = $functionUpload->getDropboxPath();
		$content->model_id = 0;
		$content->model_type = 6;
		$content->type = 1;
		$content->save();

		return $content;
	}

	public function getSeals($associated_id)
	{
		$seals = Seal::where('published', 1)->get();
		$associated = Company::with('seals')->find($associated_id);

		$seals_selected_array = [];

		foreach ($associated->seals as $key => $seal) {
			$seals_selected_array[] = array(
				'id' => $seal->id,
				);
		}

		$seals_array = [];

		foreach ($seals as $key => $seal) {
			$seals_array[] = array(
				'id' => $seal->id,
				'name' =>$seal->name,
				);
		}

		return response()->json(['seals_selected' => $seals_selected_array, 'seals' => $seals_array]);

	}
}
