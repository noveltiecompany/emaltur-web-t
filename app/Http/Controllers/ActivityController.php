<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Uploaders\ImageUploader;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    public function all()
    {
        $activities = Activity::all();
        return $activities;
    }

    public function allPublished()
    {
        $activities = Activity::select(['id', 'name'])->where('published', 1)->get();
        return $activities;
    }

    public function show($id)
    {
        $activity = Activity::find($id);
        return $activity;
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $activity = new Activity();
        $activity->fill($data);
        $activity->slug = str_slug($request->name);
        $activity->published = true;

        if ($request->hasFile('image')) {

            $img = $request->image;

            $functionUpload = new ImageUploader();
            $functionUpload->upload('/activities',$img,'png',950);

            $activity->image = $functionUpload->getDropboxUrl();
            $activity->image_path = $functionUpload->getDropboxPath();

            $functionUpload->upload('/activities',$img,'png',450);
            $activity->image_thumb = $functionUpload->getDropboxUrl();
            $activity->image_thumb_path = $functionUpload->getDropboxPath();
        }
        $activity->save();
        return;
    }

    public function update($id, Request $request)
    {
        $data = $request->all();
        $activity = Activity::find($id);
        $activity->fill($data);
        $activity->slug = str_slug($request->name);

        if ($request->hasFile('image')) {

            $img = $request->image;

            $functionUpload = new ImageUploader();
            $functionUpload->upload('/company/activities',$img,'png',950);
            $functionUpload->delete($activity->image_path, $activity->image);

            $activity->image = $functionUpload->getDropboxUrl();
            $activity->image_path = $functionUpload->getDropboxPath();

            $functionUpload->upload('/company/activities',$img,'png',450);
            $functionUpload->delete($activity->image_thumb_path, $activity->image_thumb);
            $activity->image_thumb = $functionUpload->getDropboxUrl();
            $activity->image_thumb_path = $functionUpload->getDropboxPath();
        }
        $activity->save();
        return;
    }

    public function updatePublished($id)
    {
        $activity = Activity::find($id);

        if ($activity->published == true) {
            $activity->published = false;
        } else if($activity->published == false){
            $activity->published = true;
        }
        $activity->save();
        return;
    }

    public function delete($id)
    {
        $activity = Activity::find($id);
        $functionUpload = new ImageUploader();
        $functionUpload->delete($activity->image_path, $activity->image);
        $functionUpload->delete($activity->image_thumb_path, $activity->image_thumb);
        $activity->delete();
        return;
    }

    //Metodos para el frontEnd
    //api/activities
    public function index()
    {
      $activities = Activity::whereHas('companies')->get();

      $array = [];

      if (!count($activities)) {
        return response()->json(['activities'=>$array],200);
      }

      foreach ($activities as $k => $activity) {

        $array[$k]['id'] = $activity->id;
        $array[$k]['slug'] = $activity->slug;
        $array[$k]['name'] = $activity->name;
      }

      return response()->json(['data'=>$array],200);
    }

}
