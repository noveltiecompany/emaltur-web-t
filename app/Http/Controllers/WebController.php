<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Product;
use App\Post;
use stdClass;

class WebController extends Controller
{
	protected $description;
	protected $imageUrl;
	protected $nameCompany;
	protected $pageTwitter;
	protected $title;

	public function __construct()
	{
		$company = Company::first();
		$this->description = $company->description_company;
		$this->imageUrl = (count($img  = $this->getOneRandomImage(1,1,null)) ? $img->resource_thumb : "https://dl.dropboxusercontent.com/u/82668656/IMAGEN1.png" );
		$this->nameCompany = $company->name_company;
		$this->pageTwitter = $company->twitter;
		$this->title = $company->name_company;
	}

  public function getIndex ()
  {
    $head = new stdClass;

    // Meta Company
    $head->description = $this->description;
    $head->imageUrl = $this->imageUrl;
    $head->nameCompany = $this->nameCompany;
    $head->pageTwitter = $this->pageTwitter;
    $head->title = $this->title;
    $head->url = route('home');
    // End Meta Company

    return view('web.index', compact('head'));
  }


  public function getIndexNews($newsSlug)
  {
    $head = new stdClass;
		if ($newsSlug != null) {
			$news = Post::where('slug',$newsSlug)->first();
			if (count($news)) {
				$head->description = $news->content;
				$head->imageUrl = (count($img  = $this->getOneRandomImage($news->id,3,null)) ? $img->resource_thumb : "https://dl.dropboxusercontent.com/u/82668656/IMAGEN1.png" );
				$head->nameCompany = $this->nameCompany;
				$head->pageTwitter = $this->pageTwitter;
				$head->title = $news->title;
				$head->url = route('posts-profile', $newsSlug);
			}
			else {
				return redirect()->route('news-index');
			}
		}
    return view('web.index', compact('head'));
  }

}
