<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = 'admin/empresa';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

	public function username()
	{  
		return 'username';
	}

    /* Custom Login without use Laravel's login */
    public function showLoginForm()
    {
        return view('admin.login');
    }

    public function authenticate(Request $request)
    {   
        if (Auth::attempt(['username' => $request->username, 'password' => $request->password, 'activated' => 1])) {
            return redirect()->intended('admin/empresa');
        } else {
            return redirect()->intended('webadmin');
         }
    }

}
