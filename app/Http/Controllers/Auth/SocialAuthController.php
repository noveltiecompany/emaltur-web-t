<?php

namespace App\Http\Controllers\Auth;

//use ComproEnTacna\Entities\Person;
use App\Company;
use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Mail\SendMailSubscription;
use App\Subscription;
use Illuminate\Http\Request;
use Mail;
use Socialite;
use Uuid;

class SocialAuthController extends Controller
{
    public function redirectFacebook()
    {
        return Socialite::driver('facebook')->stateless()->redirect();
    }

    public function callbackFacebook()
    {
        $providerUser = Socialite::driver('facebook')->fields(['name', 'first_name', 'last_name', 'email', 'gender', 'birthday', 'verified'])->stateless()->user();

        $existSubscription = Subscription::where('email', $providerUser->email)->first();

        if ($existSubscription) {
            return redirect('/blog?success=false');
        }

        $subscription = new Subscription();
        $subscription->email = $providerUser->email;
        $subscription->code = "-";
        $subscription->name = "{$providerUser->user['first_name']} {$providerUser->user['last_name']}";
        $subscription->phone = $request->celphone;
        $subscription->save();
        $subscription->code = 'suscrito-'.$subscription->id;
        $subscription->save();

        $t = array(
            'email' => "{$providerUser->user['first_name']} {$providerUser->user['last_name']}",
            'name' => $request->name,
            'cel' => $request->celphone,
            );

        $company = Company::first();
        $companyEmail = $company->email;
        Mail::to($companyEmail)->send(new SendMailSubscription($t, $t['email']));

        return redirect('/felicidades');
    }


    public function redirectGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function callbackGoogle()
    {
        $providerUser = Socialite::driver('google')->user();

        $existSubscription = Subscription::where('email', $providerUser->email)->first();

        if ($existSubscription) {
            return redirect('/blog?success=false');
        }

        $subscription = new Subscription();
        $subscription->email = $providerUser->email;
        $subscription->code = "-";
        $subscription->name = "{$providerUser->user['name']['givenName']} {$providerUser->user['name']['familyName']}";
        $subscription->phone = '';
        $subscription->save();
        $subscription->code = 'suscrito-'.$subscription->id;
        $subscription->save();

        $t = array(
            'email' => $providerUser->email,
            'name' => "{$providerUser->user['name']['givenName']} {$providerUser->user['name']['familyName']}",
            'cel' => '',
            );

        $company = Company::first();
        $companyEmail = $company->email;
        Mail::to($companyEmail)->send(new SendMailSubscription($t, $t['email']));

        return redirect('/felicidades');
    }
}
