<?php

namespace App\Http\Controllers;

use App\Seal;
use App\Uploaders\ImageUploader;
use Illuminate\Http\Request;

class SealController extends Controller
{
    public function all()
    {
        $seals = Seal::all();
        return $seals;
    }

    public function allPublished()
    {
        $seals = Seal::where('published', 1)->get();
        return $seals;
    }

    public function show($id)
    {
        $seal = Seal::find($id);
        return $seal;
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $seal = new Seal();
        $seal->fill($data);
        $seal->published = true;

        if ($request->hasFile('image')) {

            $img = $request->image;

            $functionUpload = new ImageUploader();
            $functionUpload->upload('/seals',$img,'png',350);

            $seal->image = $functionUpload->getDropboxUrl();
            $seal->image_path = $functionUpload->getDropboxPath();
        }
        $seal->save();
        return;
    }

    public function update($id, Request $request)
    {
        $data = $request->all();
        $seal = Seal::find($id);
        $seal->fill($data);

        if ($request->hasFile('image')) {

            $img = $request->image;

            $functionUpload = new ImageUploader();
            $functionUpload->upload('/seals',$img,'png',350);
            $functionUpload->delete($seal->image_path, $seal->image);

            $seal->image = $functionUpload->getDropboxUrl();
            $seal->image_path = $functionUpload->getDropboxPath();
        }
        $seal->save();
        return;
    }

    public function updatePublished($id)
    {
        $seal = Seal::find($id);

        if ($seal->published == true) {
            $seal->published = false;
        } else if($seal->published == false){
            $seal->published = true;
        }
        $seal->save();
        return;
    }

    public function delete($id)
    {
        $seal = Seal::find($id);
        $functionUpload = new ImageUploader();
        $functionUpload->delete($seal->image_path, $seal->image);
        $seal->delete();
        return;
    }

}
