<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use App\User;

class TestController extends Controller
{
	public function users()
	{
		$users = User::all();
		return view('test.datatable')->with('users', $users);
	}
}
