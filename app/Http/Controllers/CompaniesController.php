<?php

namespace App\Http\Controllers;

use App\Company;
use App\Content;
use App\Http\Requests\CompanyRequest;
use App\Http\Requests\VideoRequest;
use App\Uploaders\ImageUploader;
use App\Value;
use Illuminate\Http\Request;

class CompaniesController extends Controller
{
	public function getViewCompany()
	{
		$imagesCompany = Content::where('model_type',1)->where('type',1)->orderBy('id','DESC')->get();
		return view('admin.routers.company.index',['imagesCompany'=>$imagesCompany]);
	}

	public function putCompany(CompanyRequest $request)
	{
		try {
			$data =  $request->all();
			$company = Company::first();
			$company->fill($data);
			$company->slug_company = str_slug($request->name_company, "-");

			if ($request->hasFile('company_logo')) {
				$img = $request->file('company_logo');
				$functionUpload = new ImageUploader();
				$functionUpload->upload('/company',$img,'png',500);
				$functionUpload->delete($company->logotype_path,$company->logotype);

				$company->logotype = $functionUpload->getDropboxUrl();
				$company->logotype_path = $functionUpload->getDropboxPath();

				$functionUpload->upload('/company',$img,'png',300);
				$functionUpload->delete($company->logotype_thumb_path,$company->logotype_thumb);

				$company->logotype_thumb = $functionUpload->getDropboxUrl();
				$company->logotype_thumb_path = $functionUpload->getDropboxPath();
			}

			if ($request->hasFile('company_logo_white')) {
				$img = $request->file('company_logo_white');

				$functionUpload = new ImageUploader();
				$functionUpload->upload('/company',$img,'png',500);
				$functionUpload->delete($company->logotype_white_path,$company->logotype_white);

				$company->logotype_white = $functionUpload->getDropboxUrl();
				$company->logotype_white_path = $functionUpload->getDropboxPath();

				$functionUpload->upload('/company',$img,'png',300);
				$functionUpload->delete($company->logotype_white_thumb_path,$company->logotype_white_thumb);

				$company->logotype_white_thumb = $functionUpload->getDropboxUrl();
				$company->logotype_white_thumb_path = $functionUpload->getDropboxPath();
			}

			$company->save();
			return response()->json(['success'=>true],200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);

		}
	}

	public function deleteCompanyVideo(Request $request)
	{
		try {
			$content  = Content::find($request->videoId);
			$content->delete();

			$videos = Content::where('model_type',1)->where('type',2)->get();

			return response()->json(['success'=>true,'videos'=>$videos],200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);
		}

	}

	public function putCompanySlogan(Request $request)
	{
		try {
			$data =  $request->all();
			$company = Company::first();
			$company->fill($data);
			$company->save();
			return response()->json(['success'=>true],200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);
		}
	}

	public function getCompanyToEdit()
	{
		$company  = Company::first();
		$videos = Content::where('model_type',1)->where('type',2)->orderBy('id','DESC')->get();
		return response()->json(['company'=>$company,'videos'=>$videos],200);
	}

	public function postCompanyVideo(VideoRequest $request)
	{
		try {
			$content = new Content();
			$content->content = $request->company_video_name;
			$content->resource = $request->company_video_link;
			$content->resource_path = "--";
			$content->resource_thumb = "--";
			$content->resource_thumb_path = "--";
			$content->model_id = 1;
			$content->model_type = 1;
			$content->type = 2;
			$content->save();

			$videos = Content::where('model_type',1)->where('type',2)->orderBy('id','DESC')->get();

			return response()->json(['success'=>true,'videos'=>$videos],200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);
		}
	}

	public  function putCompanyVideo(VideoRequest $request)
	{
		try {
			$content = Content::find($request->company_video_id);
			$content->content = $request->company_video_name;
			$content->resource = $request->company_video_link;
			$content->save();

			$videos = Content::where('model_type',1)->where('type',2)->orderBy('id','DESC')->get();
			return response()->json(['success'=>true,'videos'=>$videos],200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);

		}

	}

	public function postCoverImages(Request $request)
	{
			$img  = $request->file[0];
			// $result  = $this->checkImageSize($Img,1350,350);
			// if (!$result) {
			// 	return 98;
			// }
			$functionUpload = new ImageUploader();
			$functionUpload->upload('/company/dropzone',$img,'png',1350);

			$content  = new Content();
			$content->content = "Img dropzone company";
			$content->resource = $functionUpload->getDropboxUrl();
			$content->resource_path = $functionUpload->getDropboxPath();

			$functionUpload->upload('/company/dropzone/thumbs',$img,'png',450);
			$content->resource_thumb = $functionUpload->getDropboxUrl();
			$content->resource_thumb_path = $functionUpload->getDropboxPath();
			$content->model_id = 1;
			$content->model_type = 1;
			$content->type = 1;
			$content->save();

			return $content;
	}

		//Metodos API para el FrontEnd

		public function allCompanies(Request $request)
		{

			$companies = Company::where('type', 2);

			if ($request->has('search')) {
				$name = $request->search;

				$companies = $companies->where('name_company', 'LIKE', "%$name%");
			}

			if ($request->has('activity_id')) {
				$companies = $companies->where('activity_id', $request->activity_id);
			}

			$companies = $companies->with('seals')->with('activity')->with('images')->get();

			$t = [];

			foreach ($companies as $key => $company) {

				$seals_array = [];

				foreach ($company->seals as $i => $seal) {
					$seals_array[] = array(
						'image_url' => $seal->image,
						'name' => $seal->name,
						);
				}

				$images_array = [];

				foreach ($company->images as $key => $image) {
					$images_array[] = array(
						'url' => $image->resource,
						'url_thumb' => $image->resource_thumb,
						);
				}

				$fanPage = '';
				if ($company->facebook!=''){
					$facebook = explode('.com/', $company->facebook);
					$fanPage = '/'.$facebook[1];
				}

				$t[] = array(
						'name' => $company->name_company,
						'activity_id' => $company->activity->id,
						'activity_name' => $company->activity->name,
						'address' => $company->address,
						'latitude' => (float)$company->latitude,
						'longitude' => (float)$company->longitude,
						'cellphone' => $company->cel,
						'phone' => $company->phone,
						'email' => $company->email,
						'website' => $company->website,
						'facebook' => $fanPage,
						'facebook_link' => $company->facebook,
						'image_url' => $company->logotype_thumb,
						'seals' => $seals_array,
						'gallery' => $images_array,
					);
			}
			return response()->json(['data' => $t], 200);
		}

		public function postGalleryImages(Request $request)
		{
			$img  = $request->file[0];

			$functionUpload = new ImageUploader();
			$functionUpload->upload('/company/gallery',$img,'png',950);

			$content  = new Content();
			$content->content = "gallery images of company";
			$content->resource = $functionUpload->getDropboxUrl();
			$content->resource_path = $functionUpload->getDropboxPath();

			$functionUpload->upload('/company/gallery/thumbs',$img,'png',450);
			$content->resource_thumb = $functionUpload->getDropboxUrl();
			$content->resource_thumb_path = $functionUpload->getDropboxPath();
			$content->model_id = 0;
			$content->model_type = 7;
			$content->type = 1;
			$content->save();

			$images = [];
			$images[0] = $content;

			return $images;
		}
}
