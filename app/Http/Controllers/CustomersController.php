<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Http\Requests\CustomerRequest;
use App\Http\Requests\CustomerUpdateRequest;
use Validator;

class CustomersController extends Controller
{

    public function getCustomers()
    {
        $customers = Customer::all();
        return response()->json(['customers' => $customers], 200);
    }

    public function deleteCustomer($id)
    {
      try {
        $customer = Customer::find($id);
        $customer->delete();
        return response()->json(['success' => true], 200);
      } catch (Exception $e) {
        return response()->json(['success' => false], 200);
      }
    }

    public function getCustomer($id)
    {
        $customer = Customer::find($id);
        return response()->json($customer, 200);
    }

    public function saveCustomer(CustomerRequest $request)
    {
      try {
          $data = $request->all();
          $customer = new Customer();
          $customer->fill($data);
          $customer->save();
          return response()->json(['success' => true], 200);
      } catch (Exception $e) {
          return response()->json(['success' => false], 200);

      }
    }

    public function updateCustomer(CustomerUpdateRequest $request)
    {
        try {
            $data = $request->all();
            $customer = Customer::find($data['customer_id']);
            $customer->fill($data);
            $customer->save();
            return response()->json(['success' => true], 200);
        } catch (Exception $e) {
            return response()->json(['success' => false], 200);

        }


    }
}
