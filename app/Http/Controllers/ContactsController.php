<?php

namespace App\Http\Controllers;

use App\Company;
use App\Mail\ContactCompany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactsController extends Controller
{
    public function postContactFromLanding(Request $request)
    {
        try {
                $llaveSecreta="6LdA-g8UAAAAAJKPdG7yh0UMaQT9IIDb0wk-IaaU";
                $ip=$_SERVER['REMOTE_ADDR'];
                $captcha = $request->recaptcha;
                //$captcha=Input::get('reCaptcha');
                $respuesta=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$llaveSecreta&response=$captcha&remoteip=$ip");
                $aux=json_decode($respuesta,true);
                if ($aux['success']==true)
                {
                        $emailData = array(
                            'name' => $request->name,
                            'email' =>$request->email,
                            'cellphone' =>$request->cellphone,
                            'city' =>$request->city,
                            'country' =>$request->country,
                            'msg' => $request->message,
                            'subject' => $request->subject,
                        );
                            
                        $company = Company::first();
                        $companyEmail = $company->email;

                        Mail::to($companyEmail)->send(new ContactCompany($emailData, $emailData['email']));
                         return response()->json(['success' => true], 201);
                }
                else{
                    return 'noCaptcha';
                }

    } catch (Exception $e) {
        return "Error procesando el captcha";
    }
    }
}
