<?php

namespace App\Http\Controllers;

use App\Category;
use App\Content;
use App\OrderProduct;
use App\Product;
use App\Service;
use App\Subcategory;
use App\Uploaders\ImageUploader;
use App\Uploaders\PdfUploader;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function getOneRandomImage($modelId,$modelType,$differentOf)
	{	
		// $image = DB::table('contents');
		$image  = Content::inRandomOrder();
		if ($modelId) {
			$image = $image->where('model_id',$modelId);
		}
		$image = $image->where('model_type',$modelType)
									->where('type',1)
									->where('id','!=',$differentOf)
									->take(1)
									->first();
		return $image;
	}
	
	public function getArrayImages($modelId,$modelType)
	{
		$arrayImages = [];
		$images = Content::select(['resource'])
										->where('model_id',$modelId)
										->where('model_type',$modelType)
										->where('type',1)
										->get();

		if (count($images)) {
			foreach ($images as $key => $image) {
				$arrayImages[$key] = $image->resource;
			}
		}
		return $arrayImages;
	}

	public function getVideos($modelId,$modelType)
	{
		$videos  = Content::select(['content','resource'])
									->where('model_id',$modelId)
									->where('model_type',$modelType)
									->where('type',2)
									->get();

		if (count($videos)) {
			foreach ($videos as $key => $video) {
				$videoArray = explode("=",$video->resource);
				if (count($videoArray)>1) {
					$video->resource = $videoArray[1];
				}
			}
			return $videos;
		}
		return $videos;
	}

	public function getServices()
	{
		$services  = Service::where('published',1)->get();
		return $services;
	}

	//Borrar categoria
	public  function deleteCategory(Request $request)
	{	
		try {
			$category = Category::find($request->categoryId);
			$this->deleteSubcategoriesByCategory($category->id);			
			$category->delete();
			
			return response()->json(['success'=>true],200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);
		}
	}


	public function deleteSubcategoriesByCategory($categoryId)
	{
		$subcategories = Subcategory::where('category_id', $categoryId)->get();

		foreach ($subcategories as $i => $subcategory) {
			$this->deleteProductsBySubcategory($subcategory->id);
			$subcategory->delete();
		}

	}

	public function deleteSubcategory(Request $request)
	{
		try {
			$subcategory = Subcategory::find($request->subcategoryId);
			$this->deleteProductsBySubcategory($subcategory->id);
			$subcategory->delete();
			return response()->json(['success'=>true],200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);
		}
	}

	// public function deleteOrderProductBySubcategory($subcategoryId)
	// {
	// 	$products = $this->getProductsBySubcategoryWithOutDataJson($subcategoryId);
	//
	// 	foreach ($products as $k => $product) {
	// 		$product->orders()->detach();
	// 	}
	// }

	public function deleteProductsBySubcategory($subcategoryId)
	{	
		$products = Product::where('subcategory_id',$subcategoryId)->get();
		foreach ($products as $i => $product) {

			$functionUploadPdf = new PdfUploader();
			$functionUploadPdf->delete($product->pdf_path, $product->pdf);


			$images = Content::where('model_id', $product->id)
						->where('model_type', 2)
						->where('type', 1)
						->get();

			$functionUploadImage = new ImageUploader();

			foreach ($images as $i => $image) {
				$functionUploadImage->delete($image->resource_thumb_path, $image->resource_thumb);
				$functionUploadImage->delete($image->resource_path, $image->resource);
				$image->delete();
			}
								
			$product->delete();
		}
	}

	public function checkImageSize($img,$widthVal, $heightVal)
	{
		$data = getimagesize($img);
		$width = $data[0];
		$height = $data[1];

		if ($width == $widthVal && $height == $heightVal) {
			return true;
		}

		return false;
	}
}
