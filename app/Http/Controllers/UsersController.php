<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\UserProfileRequest;
use App\Http\Requests\UserPasswordRequest;

use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;

use App\Uploaders\ImageUploader;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
	public function getUsers()
	{
		$users = User::all();
		return response()->json(['users' => $users], 200);
	}

    public function getUser($userId)
	{
		$user  = User::find($userId);
		return response()->json(['user'=>$user],200);
	}

	public function suspendUser($userId)
	{
		try {
			$user = User::find($userId);
			$user->activated = 0;
			$user->save();	

			return response()->json(['success' => true], 200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);
		}		
	}

	public function activateUser($userId)
	{
		try {
			$user = User::find($userId);
			$user->activated = 1;
			$user->save();

			return response()->json(['success' => true], 200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);

		}
	}

	public function postUser(UserCreateRequest $request)
	{
		try {
			$data = $request->all();
			$user = new User();
			$user->fill($data);
			$user->activated = 1;
			$user->company_id = 1;
			$user->password = bcrypt($data['password']);

			if ($request->hasFile('user_image')) {
				$img = $request->file('user_image');
				$functionUpload = new ImageUploader();
				$functionUpload->upload('/users/images',$img,'png',300);

				$user->user_image = $functionUpload->getDropboxUrl();
				$user->user_image_path = $functionUpload->getDropboxPath();

				$functionUpload->upload('/users/images',$img,'png',50);
				$user->user_image_thumb = $functionUpload->getDropboxUrl();
				$user->user_image_thumb_path = $functionUpload->getDropboxPath();
			}

			$user->save();
			return response()->json(['success' => true], 200);

		} catch (Exception $e) {
			return response()->json(['success'=>false],200);
		}
	}

	public function putUser(UserUpdateRequest $request)
	{
		try {
			$data = $request->all();
			$user = User::find($data['user_id']);
			$user->fill($data);
			
			if ($request->hasFile('user_image')) {
				$img = $request->file('user_image');
				$functionUpload = new ImageUploader();
				$functionUpload->upload('/users/images',$img,'png',300);
				$functionUpload->delete($user->user_image_path,$user->user_image);

				$user->user_image = $functionUpload->getDropboxUrl();
				$user->user_image_path = $functionUpload->getDropboxPath();

				$functionUpload->upload('/users/images',$img,'png',50);
				$functionUpload->delete($user->user_image_thumb_path,$user->user_image_thumb);
				$user->user_image_thumb = $functionUpload->getDropboxUrl();
				$user->user_image_thumb_path = $functionUpload->getDropboxPath();
			}
			$user->save();

			return response()->json(['success' => true], 200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);

		}
	}

	public function putProfileUser(UserProfileRequest $request)
	{
		try {
			$data =$request->all();
			$user = User::find($request->profile_id);
			$user->fill($data);

			if ($request->hasFile('user_image')) {
				$img = $request->file('user_image');
				$functionUpload = new ImageUploader();
				$functionUpload->upload('/users/images',$img,'png',300);
				$functionUpload->delete($user->user_image_path,$user->user_image);

				$user->user_image = $functionUpload->getDropboxUrl();
				$user->user_image_path = $functionUpload->getDropboxPath();

				$functionUpload->upload('/users/images',$img,'png',50);
				$functionUpload->delete($user->user_image_thumb_path,$user->user_image_thumb);
				$user->user_image_thumb = $functionUpload->getDropboxUrl();
				$user->user_image_thumb_path = $functionUpload->getDropboxPath();
			}
			$user->save();
			return response()->json(['success'=>true,'user'=>$user],200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);
		}
	}

	public function putChangePassword(UserPasswordRequest $request)
	{
		try {
			$userId = $request->id;
			$user = User::find($userId);
			$user->password = bcrypt($request->password);
			$user->save();

			if (Auth::user()->id == $userId) {
				Auth::logout();
			}
			return response()->json(['success'=>true],200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);

		}

	}

}
