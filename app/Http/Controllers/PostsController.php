<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Content;
use App\Http\Requests\PostRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Uploaders\ImageUploader;
use Carbon\Carbon;
use Jenssegers\Date\Date;

class PostsController extends Controller
{
	protected $defaultImage;

	public function __construct()
	{
		$this->defaultImage = asset('static/images/image_no_available.png');
	}

	public function getAllPosts()
	{
		$posts = Post::orderBy('id','DESC')->get();
		foreach ($posts as $k => $post) {
			$post->image = (count($img  = $this->getOneRandomImage($post->id,3,null)) ? $img->resource_thumb : $this->defaultImage);
		}

		return response()->json(['posts'=>$posts],200);
	}

	public function getPostById($postId)
	{
		$post = Post::find($postId);
		return response()->json(['post'=>$post],200);
	}

	public function postPost(PostRequest $request)
	{
		try {
			$data = $request->all();
			$post = new Post();
			$post->fill($data);
			$post->slug = str_slug($request->title);
			$post->published = 1;
			$post->save();

			return response()->json(['success'=>true,'post'=>$post],200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);
		}
	}

	public function putPost(PostUpdateRequest $request)
	{
		try {
			$data = $request->all();
			$post = Post::find($request->post_id);
			$post->fill($data);
			$post->slug = str_slug($request->title);
			$post->save();

			return response()->json(['success'=>true,'post'=>$post],200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);
		}
	}

	public function putPostPublished(Request $request)
	{
		try {
			$lastStatus  = $request->lastStatus;
			$post = Post::find($request->postId);
			$post->published = ($lastStatus ? 0 : 1);
			$post->save();
			return response()->json(['success'=>true],200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);
		}
	}

	public function deletePost(Request $request)
	{
		try {
			$post = Post::find($request->postId);
			$contents = Content::where('model_id',$post->id)->where('model_type',3)->where('type',1)->get();

			$functionUpload = new ImageUploader();

			foreach ($contents as $k => $content) {
				$functionUpload->delete($content->resource_path,$content->resource);
				$functionUpload->delete($content->resource_thumb_path,$content->resource_thumb);
				$content->delete();
			}
			$post->delete();
			return response()->json(['success'=>true],200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);
		}

	}

	public function postImages(Request $request)
	{
		$img  = $request->file[0];

		$functionUpload = new ImageUploader();
		$functionUpload->upload('/posts/images',$img,'png',900);

		$content  = new Content();
		$content->content = "Img dropzone posts";
		$content->resource = $functionUpload->getDropboxUrl();
		$content->resource_path = $functionUpload->getDropboxPath();

		$functionUpload->upload('/posts/images/thumbs',$img,'png',450);
		$content->resource_thumb = $functionUpload->getDropboxUrl();
		$content->resource_thumb_path = $functionUpload->getDropboxPath();
		$content->model_id = 0;
		$content->model_type = 3;
		$content->type = 1;
		$content->save();

		return $content;
	}

	private function howLongAgo($createdIn)
	{
		$createdIn = Carbon::parse($createdIn); //Fecha de creación
		$now = Carbon::now(); // Fecha de ahora

		if($createdIn->diffInDays($now) >=1){
			$howLongAgo = $createdIn->format('d').' '.$createdIn->format('M').' - '.$createdIn->format('H:m');
		}else{
			Date::setLocale('es');
			$howLongAgo = Date::parse($createdIn)->diffForHumans();
		}
		return $howLongAgo;
	}

	//Metodos para el frontEnd

	public function allForApi(Request $request)
	{
		$posts = Post::wherePublished(1)->orderBy('id','DESC')->with('randomImage');

		if (isset($request->max_elements)) {
			$posts = $posts->take($request->max_elements);
		}

		if (isset($request->tag)) {
			$tagSlug = $request->tag;
			$posts = $posts->whereHas('tag', function($query) use ($tagSlug) {
				$query->where('slug', $tagSlug);
			});
		}

		$posts = $posts->get();

		Date::setLocale('es');

		$t = [];
		foreach ($posts as $key => $post) {
			$t[] = array(
				'date' => Date::parse($post->created_at)->format('d \d\e F, Y'),
				'id' => $post->id,
				'image_url_thumb' => ($post->randomImage != null) ? $post->randomImage->resource_thumb : '',
				'image_url' => ($post->randomImage != null) ? $post->randomImage->resource : '',
				'slug' => $post->slug,
				'title' => $post->title,
				);
		}

		return response()->json(['data' => $t], 200);
	}

	public function getRelatedForApi($slug, Request $request)
	{
		$post = Post::whereSlug($slug)->first();
		$tag_id = $post->tag_id;

		$postsRelated = Post::where('tag_id', $tag_id)->wherePublished(1)->where('slug', '!=', $slug)->orderBy('id','DESC')->with('randomImage')->take(4);

		if (isset($request->max_others)) {
			$postsRelated = $postsRelated->take($request->max_others);
		}

		$postsRelated = $postsRelated->get();

		Date::setLocale('es');

		$t = [];
		foreach ($postsRelated as $u => $post) {
			$t[] = array(
				'date' => Date::parse($post->created_at)->format('d \d\e F, Y'),
				'id' => $post->id,
				'image_url_thumb' => ($post->randomImage != null) ? $post->randomImage->resource_thumb : '',
				'image_url' => ($post->randomImage != null) ? $post->randomImage->resource : '',
				'slug' => $post->slug,
				'title' => $post->title,
				);
		}

		return response()->json(['data' => $t], 200);
	}

	public function getBySlugForApi($slug)
	{
		$post = Post::whereSlug($slug)->wherePublished(1)->with('tag')->with('randomImage')->first();

		$t = array(
			'date' => Date::parse($post->created_at)->format('d \d\e F, Y'),
			'id' => $post->id,
			'image_url_thumb' => ($post->randomImage != null) ? $post->randomImage->resource_thumb : '',
			'image_url' => ($post->randomImage != null) ? $post->randomImage->resource : '',
			'image_id' => ($post->randomImage != null) ? $post->randomImage->id : '',
			'title' => $post->title,
			'content' => $post->content,
			'tag' => array(
				'name' => $post->tag->name,
				'slug' => $post->tag->slug,
				),
			);

		return response()->json(['data' => $t], 200);
	}
}
