<?php

namespace App\Http\Controllers;

use App\Company;
use App\Content;
use App\HeadQuarter;
use App\Post;
use App\Activity;
use App\Service;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Jenssegers\Date\Date;

class LandingController extends Controller
{
    public function home()
    {
        $company = Company::where('type', 1)->with('videos')->first();
        $activities = Activity::where('published', 1)->get();

        $activities_array = [];

        foreach ($activities as $i => $activity) {
            $activities_array[] = array(
                'id' => (int)$activity->id,
                'title' => $activity->name,
                'image_url' => $activity->image_thumb,
                'slug' => $activity->slug,
                );
        }

        $videos_array = [];

        foreach ($company->videos as $key => $video) {
            $video_url_array = explode('=', $video->resource);
            $videos_array[] = array(
                'title' => $video->content,
                'code' => $video_url_array[1],
                );
        }

        $videoCover = '';

        if ($company->video_cover != '') {
          $video_cover = explode('=', $company->video_cover);
          $videoCover = $video_cover[1];
        }

        $t = array(
            'logo_url' => $company->logotype,
            'logo_url_white' => $company->logotype_white,
            'name_company' => $company->name_company,
            'title' => $company->title_slogan,
            'subtitle' => $company->subtitle_slogan,
            'email' => $company->email,
            'cellphone' => $company->cel,
            'phone' => $company->phone,
            'address' => $company->address,
            'twitter' => $company->twitter,
            'facebook' => $company->facebook,
            'schedules' => $company->schedules,
            'googleplus' => $company->google_plus,
            'youtube' => $company->youtube,
            'instagram' => $company->instagram,
            'pinterest' => $company->pinterest,
            'linkedin' => $company->linkedin,
            'video' => $videoCover,
            'cover_page' => [],
            'categories' => $activities_array,
            'list_videos' => $videos_array,
            'news' => [],
            );

            $coverImages = Content::where('model_type', 1)->whereType(1)->get();

            $arrayImages = [];

            foreach ($coverImages as $u => $image) {
                $arrayImages[$u] = $image->resource;
            }

            $t['cover_page'] = $arrayImages;

            $posts = Post::wherePublished(1)->orderBy('id', 'DESC')->take(6)->get();
            $arrayPosts = [];

            foreach ($posts as $i => $post) {
                $arrayPosts[] = array(
                    'image_url' => (count($m = $this->getOneRandomImage($post->id, 3, null))) ? $m->resource : '',
                    'title' => $post->title,
                    'slug' => $post->slug,
                    'date' => Date::parse($post->created_at)->format('d - F - Y'),
                    "tag" => $post->tag->name,
                    //'summary' => substr(strip_tags($post->content), 0, 100).'...',
                    );
            }

            $t['news'] = $arrayPosts;

        return response()->json(['data' => $t], 200);
    }

    public function about()
    {
        $company = Company::first();

        $benefits_array = explode('.', $company->benefits);
        $requirements_array = explode('.', $company->requirements);

        $gallery_images_array = [];
        $gallery_images = Content::where('model_type', 7)->where('type', 1)->get();

        foreach ($gallery_images as $key => $image) {
            $gallery_images_array[] = array(
                'url' => $image->resource,
                'url_thumb' => $image->resource_thumb,
                );
        }

        $data = [
            'title' => $company->title_slogan,
            'presentation' => $company->description_company,
            'our_history' => $company->our_history,
            'mission' => $company->vision,
            'objectives' => $company->our_objectives,
            'value_proposal' => $company->value_proposal,
            'directive' => $company->directors,
            'benefits' => $benefits_array,
            'requirements' => $requirements_array,
            'gallery' => $gallery_images_array,
        ];
        return response()->json(['data' => $data], 200);
    }

    public function data()
    {
        $data = [
            'logo_url' => '',
            'logo_url_white' => '',
            'email' => 'servicios@noveltie.la',
            'celphone' => '+51 ',
            'address' => 'Tacna - Perú',
            'facebook' => 'https://www.facebook.com/',
            'twitter' => '',
            'googleplus' => '',
            'youtube' => '',
            'video_url' => '',
            'cover_page' => [
                [
                  'title' => 'La estación de Wifi con monedas',
                  'subtitle' => 'Es el innovador sistema, máquina expendedora de Wifi, para venta de Wifi para tu celular, portátil o tableta y tener internet a través de monedas cuando estás fuera de casa.',
                  'image_url' => 'https://dl.dropboxusercontent.com/s/ivx795dwksz0rhs/SLIDE.jpg'
                ]
            ],
            'product' => [
              'name' => 'Punto Innovador.',
              'title' => 'Auto servicio expendedoras de Wifi, Carga tu celular y Recargas Virtuales.',
              'image_url' => 'https://dl.dropboxusercontent.com/s/1myp6ra424cop3w/tap-coin-wifi.png',
              'description' => 'Con la máquina de Wifi con monedas la autenticación es fácil y rápida, sin complicaciones para el usuario. No requiere algun usuario y contraseña.',
            ],
            'services' => [
                [
                  'icon_url' => 'https://dl.dropboxusercontent.com/s/u5nmka7ifrvfzqq/wifi.png?dl=0',
                  'name' => 'Recargas de Wifi',
                  'format' => 1,
                  'title' => '#Para recargar Wifi',
                  'images' => [
                    'https://dl.dropboxusercontent.com/s/v7bva1dexwdgz4o/PROMOTORA.jpg',
                  ],
                  'steps' => [
                    'Sólo coloca tu celular',
                    'Inserta tu moneda',
                    'Conéctate a Wifi'
                  ],
                  'price' => 1,
                  'price_concept' => 'Recarga de Wifi',
                  'description' => '',
                ],
                [
                  'icon_url' => 'https://dl.dropboxusercontent.com/s/7e1ro1z95anzijl/celular.png',
                  'name' => 'Recargas Virtuales para celulares',
                  'format' => 2,
                  'title' => '#Recargas de créditos para celulares',
                  'images' => [
                    'https://dl.dropboxusercontent.com/s/y9oux4yyc6rjkn2/LOGOS.jpg?dl=0',
                  ],
                  'steps' => '',
                  'price' => 3,
                  'price_concept' => 'Recarga Virtual',
                  'description' => 'Recargas desde S/.3 soles hasta S/.100 soles, para cualquier operador nacional',
                ],
                [
                  'icon_url' => 'https://dl.dropboxusercontent.com/s/me9qx5zi4a6b5jf/recarga.png',
                  'name' => 'Carga de batería de Equipos Celulares',
                  'format' => 1,
                  'title' => '#Recarga la batería de tu celular',
                  'images' => [
                    'https://dl.dropboxusercontent.com/s/er76zbb4r9yfrp8/equipo.jpg',
                  ],
                  'steps' => [
                    'Sólo coloca tu celular',
                    'Inserta tu moneda',
                    'Recarga tu batería'
                  ],
                  'price' => 1,
                  'price_concept' => 'Recarga Batería',
                  'description' => '',
                ]
            ],
            'value_proposal' => [
              'title' => 'Somos los primeros en ofrecer expendedores innovadores 3 en 1.',
              'description' => 'Con la máquina de Wifi con monedas la autenticación es fácil y rápida, sin complicaciones para el usuario. No requiere algun usuario y contraseña.',
            'image_url' => 'https://dl.dropboxusercontent.com/s/yu33z2jqaqcr76y/mapa_tacna.png?dl=0',
            ],
            'headquarters' => [
              [
                'title' => 'Expendedoras en Business Neumann School',
                'image_url' => 'https://dl.dropboxusercontent.com/s/iswxebo7finagsg/IMG1.jpg?dl=0',
              ],
              [
                'title' => 'Expendedoras en Business Neumann School',
                'image_url' => 'https://dl.dropboxusercontent.com/s/iswxebo7finagsg/IMG1.jpg?dl=0',
              ],
              [
                'title' => 'Expendedoras en Business Neumann School',
                'image_url' => 'https://dl.dropboxusercontent.com/s/iswxebo7finagsg/IMG1.jpg?dl=0',
              ],
            ],
              'posts' => [

                 [
                 'image_url' => 'https://dl.dropboxusercontent.com/s/ti99cso27ytyq27/IMG4.jpg?dl=0',
                  'title' => 'Expendedoras de WIfi llegaron a Tacna',
                  'slug' => 'Expendedoras-de WIfi llegaron a Tacna',
                  'date' => '2017-09-02',
                  'summary' => 'Esta herramienta permitirá a los alumnos cargar sus celulares.'
                ],
                [
                  'image_url' => 'https://dl.dropboxusercontent.com/s/ti99cso27ytyq27/IMG4.jpg?dl=0',
                   'title' => 'Expendedoras de WIfi llegaron a Tacna',
                   'slug' => 'Expendedoras-de WIfi llegaron a Tacna',
                   'date' => '2017-09-02',
                   'summary' => 'Esta herramienta carga sus celulares.'
                ],
                [
                  'image_url' => 'https://dl.dropboxusercontent.com/s/ti99cso27ytyq27/IMG4.jpg?dl=0',
                   'title' => 'Expendedoras de WIfi llegaron a Tacna',
                   'slug' => 'Expendedoras-de WIfi llegaron a Tacna',
                   'date' => '2017-09-02',
                   'summary' => 'Esta herramienta permitirá a los alumnos cargar sus celulares.'
                ],
                [
                  'image_url' => 'https://dl.dropboxusercontent.com/s/ti99cso27ytyq27/IMG4.jpg?dl=0',
                   'title' => 'Expendedoras de WIfi llegaron a Tacna',
                   'slug' => 'Expendedoras-de WIfi llegaron a Tacna',
                   'date' => '2017-09-02',
                   'summary' => 'Esta herramienta permitirá a los alumnos cargar sus celulares.'
                ],
                [
                  'image_url' => 'https://dl.dropboxusercontent.com/s/ti99cso27ytyq27/IMG4.jpg?dl=0',
                   'title' => 'Expendedoras de WIfi llegaron a Tacna',
                   'slug' => 'Expendedoras-de WIfi llegaron a Tacna',
                   'date' => '2017-09-02',
                   'summary' => 'Esta herramienta permitirá a los alumnos cargar sus celulares.'
                ],
                [
                  'image_url' => 'https://dl.dropboxusercontent.com/s/ti99cso27ytyq27/IMG4.jpg?dl=0',
                   'title' => 'Expendedoras de WIfi llegaron a Tacna',
                   'slug' => 'Expendedoras-de WIfi llegaron a Tacna',
                   'date' => '2017-09-02',
                   'summary' => 'Esta herramienta permitirá a los alumnos cargar sus celulares.'
                ]
             ]
        ];

        return response()->json(['data' => $data], 200);
    }

}
