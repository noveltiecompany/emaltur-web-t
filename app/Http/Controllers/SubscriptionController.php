<?php

namespace App\Http\Controllers;

use App\Company;
use App\Mail\SendMailSubscription;
use App\Subscription;
use Carbon\Carbon;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SubscriptionController extends Controller
{
	public function all()
	{
		$subscriptions = Subscription::all();

		$quantity = count($subscriptions);

		return $quantity;
	}

	public function getViewSubscriptions()
	{
		$subscriptions = Subscription::all();

		return view('admin.routers.subscriptions',['subscriptions'=>$subscriptions]);
	}

	public function getExcelOfSubscriptions()
	{
		$subscriptions = Subscription::all();

		$dataFormatted = [];

		foreach ($subscriptions as $i => $subscription) {
			$dataFormatted[] = array(
				'id' => $subscription->id,
				'date' => $subscription->created_at->format('d-m-Y'),
				'hour' => $subscription->created_at->format('H:s'),
				'email' => $subscription->email,
				'name' => $subscription->name,
				'phone' => $subscription->phone,
				);
		}

		$currentDate = Carbon::now()->format('d-m-Y');
		$excelName = "Lista de suscritos {$currentDate}";

        Excel::create($excelName, function($excel) use ($dataFormatted) {
            $excel->sheet('data', function($sheet) use ($dataFormatted) {
                $sheet->setOrientation('landscape');
                $sheet->fromArray(["ID", "Fecha", "Hora","Correo Eléctronico", "Nombres", "Celular"]);

                foreach ($dataFormatted as $key => $subscription) {
                    $sheet->row($key+2, $subscription);
                }

            });
        })->export('xls');
	}

	public function show($id)
	{
		$subscription = Subscription::find($id);

		$t = array(
			'code' => $subscription->code,
			'id' => $subscription->id,
			'date' => $subscription->created_at->format('d/m/Y'),
			'email' => $subscription->email,
			'name' => $subscription->name,
			'phone' => $subscription->phone,
			'interests' => $subscription->interests,
			);

		return $t;
	}

	#Api

	public function store(Request $request)
	{
		$email = $request->email;

		$existSubscription = Subscription::where('email', $email)->first();

		if (count($existSubscription)) {
			return response()->json(['success' => false, 'message' => 'This email has been registered before' ], 500);
		}

		$subscription = new Subscription();
		$subscription->email = $email;
		$subscription->code = "-";
		$subscription->interests = $request->interests;
		$subscription->name = $request->name;
		$subscription->phone = $request->celphone;
		$subscription->save();
		$subscription->code = 'suscrito-'.$subscription->id;
		$subscription->save();


		$t = array(
			'email' => $request->email,
			'name' => $request->name,
			'cel' => $request->celphone,
			);

        $company = Company::first();
        $companyEmail = $company->email;
        Mail::to($companyEmail)->send(new SendMailSubscription($t, $t['email']));

		return response()->json(['success' => true], 200);
	}

}
