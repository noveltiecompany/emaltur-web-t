<?php

namespace App\Http\Controllers;

use App\Content;
use App\Http\Requests\ServiceRequest;
use App\Http\Requests\ServiceUpdateRequest;
use App\Service;
use App\Uploaders\ImageUploader;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
	protected $defaultImage;

	public function __construct()
	{
		$this->detaultImage = asset('static/images/image_no_available.png');
	}

	public function postService(ServiceRequest $request)
	{
		try {
			$data = $request->all();
			$service  = new Service();
			$service->fill($data);
			$service->slug = str_slug($request->name,'-');
			$service->published = 1;
			
			if ($request->hasFile('service_image')) {

				$img = $request->service_image;

				$functionUpload = new ImageUploader();
				$functionUpload->upload('/company/services',$img,'png',400);
				
				$service->image = $functionUpload->getDropboxUrl();
				$service->image_path = $functionUpload->getDropboxPath();

				$functionUpload->upload('/company/services',$img,'png',100);
				$service->image_thumb = $functionUpload->getDropboxUrl();
				$service->image_thumb_path = $functionUpload->getDropboxPath();
			}

			$service->save();
			return response()->json(['success'=>true,'service'=>$service],200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);
		}
	}

	public function putService(ServiceUpdateRequest $request)
	{
		try {
			$data = $request->all();
			$service  = Service::find($request->service_id);
			$service->fill($data);

			if ($request->hasFile('service_image')) {

				$img = $request->service_image;

				$functionUpload = new ImageUploader();

				$functionUpload->upload('/company/services',$img,'png',300);
				$functionUpload->delete($service->image_path,$service->image);

				$service->image = $functionUpload->getDropboxUrl();
				$service->image_path = $functionUpload->getDropboxPath();

				$functionUpload->upload('/company/services',$img,'png',100);
				$functionUpload->delete($service->image_thumb_path,$service->image_thumb);

				$service->image_thumb = $functionUpload->getDropboxUrl();
				$service->image_thumb_path = $functionUpload->getDropboxPath();
			}
			$service->save();
			$services = Service::orderBy('id','DESC')->get();
			return response()->json(['success'=>true,'service'=>$service],200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);
		}
	}

	public function getServiceById($serviceId)
	{
		$service = Service::find($serviceId);
		return response()->json(['service'=>$service],200);
	}

	public function getServices()
	{
		$services = Service::orderBy('id', 'ASC')->get();
		
		foreach ($services as $i => $service) {
			
			$random_image = $this->getOneRandomImage($service->id, 5, null);		

			$service->image = '';
			if (count($random_image)) {
				$service->image = $random_image->resource_thumb;
			}
		}
		return response()->json(['services'=>$services],200);
	}

	public function deleteService(Request $request)
	{
		try {
			$service = Service::find($request->serviceId);
			$functionUpload = new ImageUploader();
			$functionUpload->delete($service->image_path,$service->image);
			$functionUpload->delete($service->image_thumb_path,$service->image_thumb);
			$service->delete();
			$services = Service::orderBy('id','DESC')->get();
			return response()->json(['success'=>true,'services'=>$services],200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);
		}
	}

	public function putChangePublishedStatus(Request $request)
	{
		try {
			$lastStatus  = $request->lastStatus;

			$service = Service::find($request->serviceId);
			$service->published = ($lastStatus ? 0 : 1);
			$service->save();
			return response()->json(['success'=>true],200);
		} catch (Exception $e) {
			return response()->json(['success'=>false],200);
		}
	}

	public function storeImage(Request $request)
	{
			$img  = $request->file[0];
			// $result  = $this->checkImageSize($Img,1350,350);
			// if (!$result) {
			// 	return 98;
			// }
			$functionUpload = new ImageUploader();
			$functionUpload->upload('/services/images',$img,'png',1350);

			$content  = new Content();
			$content->content = "Img";
			$content->resource = $functionUpload->getDropboxUrl();
			$content->resource_path = $functionUpload->getDropboxPath();

			$functionUpload->upload('/services/images/thumbs',$img,'png',450);
			$content->resource_thumb = $functionUpload->getDropboxUrl();
			$content->resource_thumb_path = $functionUpload->getDropboxPath();
			$content->model_id = 0;
			$content->model_type = 5;
			$content->type = 1;
			$content->save();

			return $content;
	}

}
