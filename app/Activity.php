<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activity extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'activities';

    protected $fillable = [
        'name', 'description'
    ];

    public function companies()
    {
        return $this->hasMany('App\Company');
    }

}
