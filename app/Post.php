<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
	use SoftDeletes;

    protected $fillable = ['title', 'content', 'tag_id'];

	protected $dates = ['deleted_at'];

	public function randomImage()
	{
		return $this->hasOne('App\Content', 'model_id')->where('model_type', 3)->where('type', 1)->inRandomOrder();
	}

	public function tag()
	{
		return $this->belongsTo('App\Tag');
	}
}
