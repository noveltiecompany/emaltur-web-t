<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{
	use SoftDeletes;

    protected $fillable = [];
    
	protected $dates = ['deleted_at'];

	protected $table = "subscriptions"; 
}
