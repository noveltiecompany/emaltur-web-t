<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
	use SoftDeletes;

	protected $fillable = ['ruc', 'legal_name', 'first_name', 'last_name', 'email', 'cel_whatsapp', 'facebook', 'city', 'country', 'customer_type'];

	protected $dates = ['deleted_at'];

}
