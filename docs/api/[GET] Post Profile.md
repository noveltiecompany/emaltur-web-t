# POSTS

----

## 1. [GET] Post Profile


### PATH

  ```javascript
  `/api/posts/${slug}?max_others=${n}`
  ```

  * #### Method: `GET`



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        {
          "date": "",
          "id": 0,
          "image_url_thumb": "",
          "image_url": "",
          "image_id": "",
          "title": "",
          "content": "[HTML]",
          "tag": {
            "name": "",
            "slug": ""
          }
        }
      ]
    }
    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

   OR



### Notes:

  max_others indica la cantidad maxima de elementos que tiene tener el array others
  others: otras noticias listadas, ordenadas por fecha de publicación No necesita logeado.
  si no hay max_other envio todo
