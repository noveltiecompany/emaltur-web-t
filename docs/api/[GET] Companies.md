# Public

----

## 1. [GET] Companies



### PATH
  
  ```javascript
  `/api/${VERSION}/companies`
  `/api/${VERSION}/companies?search=${}`
  `/api/${VERSION}/companies?activity_id=${}`
  ```

  * #### Method: `GET`



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        {
          "name": "",
          "activity_id": 1,
          "activity_name": "",
          "address": "",
          "cellphone": "",
          "phone": "",
          "email": "",
          "website": "",
          "facebook": "",
          "image_url": "",
          "latitude": "",
          "longitude": "",
          "seals": [
            {
              "image_url": "/images/x.jpg",
              "name": "Innovador"
            }
          ],
          "gallery": [
            {
              "url": "/images/x.jpg",
              "url_thumb": "images/x.jpg"
            }
          ]
        }
      ]
    }
    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />
