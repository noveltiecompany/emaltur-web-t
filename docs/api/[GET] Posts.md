# NEWS

----

## 1. [GET] Posts



### PATH

  ```javascript
  // All
  `/api/posts`
  `/api/posts?max_elements=${n}`
  `/api/posts?tag=${tagSlug}`
  `/api/posts?max_elements=${n}&tag=${tagSlug}`
  ```

  * #### Method: `GET`

  * #### Queries

    * __Optional:__

      `max_elements=[integer]` <br />
      `tag=[integer]` <br />



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        {
          "date": "01 de Agosto, 2017",
          "id": 0,
          "image_url_thumb": "",
          "image_url": "",
          "slug": "",
          "title": ""
          "summary": ""
        }
      ]
    }
    ```


  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />




### Notes:

  max_elements indica la cantidada maxima de elementos que tiene tener el array
  si no hay max_other envio todo
