# PUBLIC

----

## 1. [GET] Home



### PATH

  ```javascript
  `/api/home`
  ```

  * #### Method: `GET`



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": {
        "cover_page": [ "[URL]" ],
        "title": "",
        "subtitle": "",
        "video": "[CODE YOUTUBE]",
        "categories": [
          {
            "title": "",
            "image_url": "[URL]",
            "slug": ""
          }
        ],
        "list_videos": [
          {
            "title": "",
            "code": "[CODE YOUTUBE]"
          }
        ],
        "news": [
          {
            "title": "Competir en el mercado",
            "slug": "competir-",
            "tag": "Eventos",
            "date": "[DATE]",
            "image_url": "[URL]"
          }
        ],
        "address": "",
        "cellphone": "",
        "phone": "",
        "email": "",
        "schedules": "",
        "facebook": "",
        "twitter": "",
        "youtube": "",
        "logo_url": "[URL]",
        "logo_url_white": "[URL]",
      }
    }
    ```

  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />



<!-- ### Notes: -->
