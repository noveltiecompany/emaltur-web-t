# Subscription

## 1. Post subscription

```
[POST] api/subscription
```

### Example

#### POST

```
Link: api/subscription
```

#### Response

```
Status: 200 OK
```

```json
{
    "email": "luis.charres.06@gmail.com",
    "name": "Luis Charres",
    "phone": "94995959949",
    "interests":""
}
```
