# Activities

<!--  -->

----

## 1. [GET] Activities

  <!-- Las Actividades se refiere a la actividad comercial de la empresa -->



### PATH

  <!--  -->

  ```javascript
  [GET] `/api/${VERSION}/activities/`
  ```

  * #### Method:

    <!-- <_The request type_> -->

    `GET`



### RESPONSE

  * #### Success:

    <!--  -->

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": [
        {
          "id": 0,
          "name": "",
          "slug": ""
        }
      ]
    }
    ```


  * #### Error:

    <!--  -->

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />

   OR

    __Code:__ 401 - UNAUTHORIZED <br />
    __Description:__ Si no esta logeado
    __content:__

    ```json
    {
      "error": "",
      "message": ""
    }
    ```



<!-- ### Notes:

  This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here. -->
