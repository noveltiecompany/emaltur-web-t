# PUBLIC

----

## 1. [GET] ABOUT



### PATH

  ```javascript
  `/api/about`
  ```

  * #### Method: `GET`



### RESPONSE

  * #### Success:

    __Code:__ 200 OK <br />
    __Content:__

    ```json
    {
      "data": {
        "title": "",
        "presentation": "Presentación",
        "mission": "Misión y Visión",
        "objectives": "Objetivos",
        "value_proposal": "Propuesta de Valor",
        "directive": "Directiva",
        "benefits": ["Beneficio 1","Beneficio 2"],
        "requirements": ["Requisito 1","Requisito 2"],
        "gallery": [
          {
            "url": "/images/x.jpg",
            "url_thumb": "images/x.jpg"
          }
        ]
      }
    }
    ```

  * #### Error:

    __Code:__ 404 - NOT FOUND <br />
    __Description:__ Si no hay resultados <br />



<!-- ### Notes: -->
