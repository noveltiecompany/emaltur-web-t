// imports modules || importación de modulos
// import libraries || importacion de bibliotecas
import { sync } from 'vuex-router-sync'
import registerComponents from './config/registerComponents'
import Root from 'App/components/root'
import router from './config/vue-router'
import store from 'store'
import Vue from 'vue'
import VueDisqus from 'vue-disqus'
import * as VueGoogleMaps from 'vue2-google-maps'

// register components || registro de componentes
registerComponents(Vue)

sync(store, router)

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyB5DgtOXdGEIDFUDkT9jK_EfN-UJIElU_0',
    libraries: 'places' // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)
  }
})

Vue.use(VueDisqus)

const app = new Vue({
  router,
  store,
  ...Root
})

export { app, router, store }
