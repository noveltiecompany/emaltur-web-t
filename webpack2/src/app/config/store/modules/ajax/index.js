import * as types from './types'
import * as getters from './getters'
import * as actions from './actions'

const state = {
  inProgress: 0,
  initialCompletion: false
}

const mutations = {
  [types.BEGIN_AJAX_CALL] (state) {
    state.inProgress += 1
  },
  [types.AJAX_CALL_SUCCESS] (state) {
    state.inProgress -= 1

    if (state.inProgress === 0) {
      state.initialCompletion = true
    }
  },
  [types.AJAX_CALL_ERROR] (state) {
    state.inProgress -= 1

    if (state.inProgress === 0) {
      state.initialCompletion = true
    }
  },
  [types.COMPLETE_INITIAL] (state) {
    state.initialCompletion = true
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
