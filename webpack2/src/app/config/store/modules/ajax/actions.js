import * as types from './types'

// export const beginAjaxCall = ({commit, dispatch, state, rootState}) => {}

export const beginAjaxCall = ({commit}) => {
  commit(types.BEGIN_AJAX_CALL)
}

export const ajaxCallSuccess = ({commit, state}) => {
  commit(types.AJAX_CALL_SUCCESS)
  if (state.inProgress === 0) {
    commit(types.COMPLETE_INITIAL)
  }
}

export const ajaxCallError = ({commit, state}) => {
  commit(types.AJAX_CALL_ERROR)
  if (state.inProgress === 0) {
    commit(types.COMPLETE_INITIAL)
  }
}
