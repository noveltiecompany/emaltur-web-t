import * as types from './types'
import * as getters from './getters'
import * as actions from './actions'

const state = {
  prevPath: './'
}

const mutations = {
  [types.UPDATE_PREVIOUS_PATH] (state, path) {
    state.prevPath = path
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
