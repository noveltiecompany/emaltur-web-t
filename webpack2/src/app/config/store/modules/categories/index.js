import * as types from './types'
import * as getters from './getters'
import * as actions from './actions'

const state = {
  all: []
}

const mutations = {
  [types.RECEIVE_CATEGORIES] (state, categories) {
    state.all = categories
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
