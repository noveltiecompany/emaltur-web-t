import * as keys from 'store/plugins/constantsKeys'
import * as types from './types'
import * as getters from './getters'
import * as actions from './actions'

const state = {
  added: JSON.parse(window.localStorage.getItem(keys.CART_STORAGE_KEY) || '[]')
}

const mutations = {
  [types.ADD_NEW_ITEM] (state, item) {
    state.added.push(item)
  },
  [types.CHANGE_QUANTITY] (state, param) {
    let tmp = state.added
    tmp[param.indexProduct].feature[param.indexFeature].inventory.quantity = param.value
    state.added = tmp
  },
  [types.INCREASE_QUANTITY] (state, param) {
    let tmp = state.added
    tmp[param.indexProduct].feature[param.indexFeature].inventory.quantity += param.quantity
    state.added = tmp
  },
  [types.ADD_NEW_FEATURE] (state, param) {
    state.added[param.indexProduct].feature.push(param.feature)
  },
  [types.UPDATE_PRICE_ID_CART] (state, param) {
    state.added[param.indexProduct].feature[param.indexFeature].inventory.priceId = param.priceId
  },
  [types.UPDATE_PRICE_CART] (state, param) {
    state.added[param.indexProduct].feature[param.indexFeature].price = param.price
  },
  [types.UPDATE_ATTRIBUTES_CART] (state, param) {
    state.added[param.indexProduct].feature[param.indexFeature].attributes = param.attr
  },
  [types.REMOVE_FEATURE_CART] (state, param) {
    state.added[param.indexProduct].feature.splice(param.indexFeature, 1)
  },
  [types.REMOVE_PRODUCT_CART] (state, indexProduct) {
    state.added.splice(indexProduct, 1)
  },

  [types.CHECKOUT_REQUEST] (state) {
    state.added = []
  },
  [types.CHECKOUT_FAILURE] (state, savedCartItems) {
    state.added = savedCartItems
  },
  [types.CLEAR_CART] (state) {
    state.added = []
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
