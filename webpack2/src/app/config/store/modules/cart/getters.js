export const cart = (state) => {
  return state.added
}

export const cartItems = (state) => {
  let cartItems = []
  let totalFeature = []
  let total = 0
  let tmp = 0

  for (let i = 0; i < state.added.length; i++) {
    for (let j = 0; j < state.added[i].feature.length; j++) {
      tmp = state.added[i].feature[j].price.value * state.added[i].feature[j].inventory.quantity

      if (typeof totalFeature[i] !== 'number') {
        totalFeature[i] = 0
      }

      totalFeature[i] += tmp
    }

    cartItems.push({
      ...state.added[i],
      subTotal: {
        'currencyId': 1,
        'value': totalFeature[i]
      }
    })

    total += totalFeature[i]
  }

  return {
    'items': cartItems,
    'total': {
      'currencyId': 1,
      'value': total
    }
  }
}

export const cartCount = (state) => {
  return state.added.length
  // return cart.added.reduce((total, i) => {
  //   return total + i.quantity
  // }, 0)
}
