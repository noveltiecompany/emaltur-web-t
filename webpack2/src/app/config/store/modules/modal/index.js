import * as types from './types'
import * as getters from './getters'
import * as actions from './actions'

const state = {
  stateModal: false,
  shoppingCart: false
}

const mutations = {
  [types.ENABLE_MODAL] (state) {
    state.stateModal = true
  },
  [types.DISABLE_MODAL] (state) {
    state.stateModal = false
  },
  [types.ENABLE_SHOPPING_CART] (state) {
    state.shoppingCart = true
  },
  [types.DISABLE_SHOPPING_CART] (state) {
    state.shoppingCart = false
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
