import * as types from './types'
import * as getters from './getters'
import * as actions from './actions'

const state = {
  request: '',
  response: [],
  preview: []
}

const mutations = {
  [types.RECEIVE_REQUEST] (state, request) {
    state.request = request
  },
  [types.RECEIVE_RESPONSE] (state, response) {
    state.response = response
  },
  [types.RECEIVE_PREVIEW] (state, response) {
    state.preview = response
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
