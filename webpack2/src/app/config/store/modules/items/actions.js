import axios from 'axios'

import * as ajax from 'store/modules/ajax/types'
import * as typesLocal from './types'
let types = Object.assign({}, ajax, typesLocal)

export const loadItems = ({commit}) => {
  commit(types.BEGIN_AJAX_CALL)

  axios.get(`${window.API_URL}/items?page=1`)
    .then(({data}) => {
      commit(types.RECEIVE_ITEMS, data.data)
      commit(types.UPDATE_PAGE, {
        current: data.currentPage,
        total: data.total,
        elements: data.perPage
      })
      commit(types.AJAX_CALL_SUCCESS)
    })
    .catch(() => {
      commit(types.AJAX_CALL_ERROR)
    })
}

export const loadNextItems = ({commit, dispatch, state, rootState}) => {
  if (rootState.ajax.inProgress === 0) {
    if (state.page.total > (state.page.elements * state.page.current)) {
      commit(types.BEGIN_AJAX_CALL)

      let page = state.page.current + 1
      axios({
        method: 'get',
        url: `${window.API_URL}/items`,
        params: { page: page }
      })
        .then(({ data }) => {
          commit(types.UPDATE_PAGE, {
            current: data.currentPage,
            total: data.total,
            elements: data.perPage
          })
          commit(types.ADD_ITEMS, data.data)
          commit(types.AJAX_CALL_SUCCESS)
        })
        .catch(() => {
          commit(types.AJAX_CALL_ERROR)
        })
    }
  }
}

export const updateRangeMin = ({commit}, min) => {
  commit(types.UPDATE_RANGE_MIN, parseInt(min))
}
export const updateRangeMax = ({commit}, max) => {
  commit(types.UPDATE_RANGE_MAX, parseInt(max))
}
