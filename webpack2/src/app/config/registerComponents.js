import AppFooter from 'App/shared/components/AppFooter'
import AppHeader from 'App/shared/components/AppHeader'
import ModalContact from 'App/shared/components/ModalContact'
import ModalContainer from 'App/shared/components/ModalContainer'
import Subscription from 'App/shared/components/Subscription'

export default function registerComponents (Vue) {
  Vue.component('app-footer', AppFooter)
  Vue.component('app-header', AppHeader)
  Vue.component('modal-contact', ModalContact)
  Vue.component('modal-container', ModalContainer)
  Vue.component('subscription', Subscription)
}
