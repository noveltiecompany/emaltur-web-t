import Home from 'App/screens/Home'
import About from 'App/screens/About'
import Companies from 'App/screens/Companies'
import Blog from 'App/screens/Blog'
import BlogProfile from 'App/screens/BlogProfile'

const routes = [
  About, // path: '/sobre-emaltur'
  Companies, // path: '/asociados'
  Home, // path: '/'
  BlogProfile, // path: '/blog'
  Blog // path: '/blog'
]

export default routes
