import BlogProfileScreen from './components/Screen'

export default {
  path: '/blog/:blogSlug',
  name: 'BlogProfile',
  component: BlogProfileScreen
}
