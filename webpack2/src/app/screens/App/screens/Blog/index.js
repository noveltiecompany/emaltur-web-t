import BlogScreen from './components/Screen'

export default {
  path: '/blog',
  name: 'Blog',
  component: BlogScreen
}
