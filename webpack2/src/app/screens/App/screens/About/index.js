import HomeScreen from './components/Screen'

export default {
  path: '/sobre-emaltur',
  name: 'About',
  component: HomeScreen
}
