<div id="subscription-modal" tabindex="-1" aria-hidden="true" role="dialog" class="modal fade" data-backdrop="static">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body u-px4 row">
        <div class="col-xs-12 text-right u-mb3">
          <button class="modal-close glyphicon glyphicon-remove" id="" type="button" data-dismiss="modal" aria-hidden="true"></button>
        </div>

        <div class=" col-xs-12 u-flex u-justify-between">
          <h3 class='u-mt0'>
            <span class='u-color-text u-fw-regular' id="subscription_code"></span>
          </h3>
{{--
          <p class='u-flex u-items-center' id="driver-request_date">
            <i class='glyphicon glyphicon-calendar u-color-primary u-mr2' ></i>
            20-12-2016
          </p> --}}
        </div>

        <div class="col-xs-12 col-md-12">
          <div class="col-xs-12 u-flex u-flex-wrap u-line-1 u-mb2 u-fz-h2">
            <span class='u-mr2'>Fecha:</span>
            <p class='u-color-text u-mb0' id="subscription_date"></p>
          </div>

          <div class="col-xs-12 u-flex u-flex-wrap u-line-1 u-mb2 u-fz-h2">
            <span class='u-mr2'>Email:</span>
            <p class='u-color-text u-mb0' id="subscription_email"></p>
          </div>

          <div class="col-xs-12 u-flex u-flex-wrap u-line-1 u-mb2 u-fz-h2">
            <span class='u-mr2'>Nombres:</span>
            <p class='u-color-text u-mb0' id="subscription_name"></p>
          </div>

          <div class="col-xs-12 u-flex u-flex-wrap u-line-1 u-mb2 u-fz-h2">
            <span class='u-mr2'>Teléfono:</span>
            <p class='u-color-text u-mb0' id="subscription_phone"></p>
          </div>

          <div class="col-xs-12 u-flex-wrap u-line-1 u-mb2 u-fz-h2">
            <span class='u-mr2'>Intereses:</span>
            <p class='u-color-text u-mb0' id="subscription_interests"></p>
          </div>
        </div>

        <div class="col-xs-12 u-center ">
          <hr>
          <button class="btn btn-success btn-modal u-uppercase u-fw-s-bold u-mb3 u-mr2" data-dismiss="modal" id="subscription__close">
            Cerrar
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
