<div id="service-images-modal" tabindex="-1" aria-hidden="true" role="dialog" class="modal fade" data-backdrop="static">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body row">
        <div class="col-md-12 text-right prl">
          <button class="modal-close glyphicon glyphicon-remove" type="button" data-dismiss="modal" aria-hidden="true"></button>
        </div>

        <h3 class="col-md-12 text-center u-primary u-mb4">
          <b>Imágenes</b>
        </h3>
        <input type="hidden" name="service_id" id="service-images_service-id" value="">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
          {!! Form::open(['route'=> 'upload-service-images', 'method' => 'POST', 'files'=>'true', 'id' => 'service-dropzone' , 'class' => 'dropzone']) !!}
          {!! Form::close() !!}

          <hr>

          <div id="service-gallery_swiper-container" class="swiper-container" data-number="" style="text-align: center;">
              <div class="swiper-wrapper">
              </div>

              <div id="service-gallery_swiper-pagination" style="display: inline-block;"></div>
              <div id="service-gallery_swiper-button-next"></div>
              <div id="service-gallery_swiper-button-prev"></div>
          </div>
        </div>

        <div class="col-md-2">
        </div>
        <div class="col-md-12 mbl text-center">
          <button type="button" class="btn btn-primary btn-modal" id="service__close">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
</div>
