<div id="product-modal" tabindex="-1" aria-hidden="true" role="dialog" class="modal fade" data-backdrop="static">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body row">
        <div class="col-md-12 text-right prl">
          <button class="modal-close glyphicon glyphicon-remove" type="button" data-dismiss="modal" aria-hidden="true"></button>
        </div>

        <h3 class="col-md-12 text-center u-primary u-mb4">
          <b>Información del Servicio</b>
        </h3>
    	<div class="col-md-12 u-mb3 u-center u-color-error"></div>

      	{!! Form::open(array('id'=>'form_product','role' => 'form', 'files' => true, 'enctype' => 'multipart/form-data')) !!}
      	  <input type="hidden" name="id" id="product_id" value="">
          <div class="col-xs-10 u-px0 col-xs-offset-1 u-mb4">

            <div class="col-md-12">
                <div class="col-md-5">
                    <div class="col-md-12 form-group">
                        <label class="control-label" id="">
                            <i class="glyphicon glyphicon-camera"></i>Foto: (650x650px) Max 2MB
                        </label>
                        <div class="dropzone" id="product_container-image">
                            <div class="dropzone_image"  id="product_preview-image">
                            </div>
                            <input type="file" accept="image/jpeg, image/png" name="product_image" id="product_image" value="">
                        </div>
                    </div>
                </div>

                <div class="col-md-7">
                    <div class="form-group">
                        <label class="control-label">Nombre: </label>
                        <input class="form-control" name="name" id="product_name" placeholder="Ingrese el Nombre del Servicio">
                        <div class="mensaje-error" id="product-error-name"></div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Título: </label>
                        <input class="form-control" name="title" id="product_title" placeholder="Ingrese el Título del Servicio">
                        <div class="mensaje-error" id="product-error-title"></div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Descripción: </label>
                        <label class="control-label">Descripción: </label>
                        <textarea class="form-control" name="description" placeholder="Describa las características del Servicio" id="product_description" rows="4" cols="40"></textarea>
                        <div class="mensaje-error" id="product-error-description"></div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">¿Servicio destacado?: </label>
                        <select class="form-control" name="featured_product" id="product_featured">
                            <option value="1">Servicio destacado - Se publica en Página de Inicio</option>
                            <option value="0">Servicio no destacado</option>
                        </select>
                    </div>

                </div>
            </div>

         </div>
        {!! Form::close() !!}

        <div class="col-md-12 mbl text-center">
          <button type="button" class="btn btn-primary btn-modal" id="product__save">CREAR</button>
          <button type="button" class="btn btn-primary btn-modal" id="product__update">GUARDAR</button>
        </div>
      </div>
    </div>
  </div>
</div>
