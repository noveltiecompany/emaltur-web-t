<div id="activity-modal" tabindex="-1" aria-hidden="true" role="dialog" class="modal fade" data-backdrop="static">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body row">
        <div class="col-md-12 text-right prl">
          <button class="modal-close glyphicon glyphicon-remove" type="button" data-dismiss="modal" aria-hidden="true"></button>
        </div>

        <h3 class="col-md-12 text-center u-primary u-mb4">
          <b>Información del rubro</b>
        </h3>

        <div class="col-md-12 u-mb3 u-center u-color-error"></div>

        {!! Form::open(array('id'=>'form_activity','role' => 'form', 'files' => true, 'enctype' => 'multipart/form-data')) !!}
            <input type="hidden" name="id" id="activity_id" value="">

            <div class="col-xs-10 u-px0 col-xs-offset-1 u-mb4">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="col-md-12 form-group">
                            <label class="control-label" id="">
                                <i class="glyphicon glyphicon-camera"></i>Imágen
                            </label>
                            <div class="dropzone" id="activity_image-container">
                                <div class="dropzone_image"  id="activity_preview-image">
                                </div>
                                <input type="file" accept="image/jpeg, image/png" name="image" id="activity_image" value="">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label">Título: </label>
                            <input type="text" name="name" placeholder="Título" class="form-control" id="activity_name">
                            <div class="mensaje-error" id="activity-name-error"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Descripción: </label>
                            <textarea class="form-control" name="description" placeholder="Redacte una descripción" id="activity_description" rows="4" cols="40"></textarea>
                            <div class="mensaje-error" id="activity-description-error"></div>
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}

        <div class="col-md-12 mbl text-center">
          <button type="button" class="btn btn-primary btn-modal" id="activity__save">CREAR</button>
          <button type="button" class="btn btn-primary btn-modal" id="activity__update">GUARDAR</button>
          <button type="button" class="btn btn-primary btn-modal" id="activity__close">CERRAR</button>
        </div>
      </div>
    </div>
  </div>
</div>
