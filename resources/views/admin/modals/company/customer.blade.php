<div id="add-customers" tabindex="-1" aria-hidden="true" role="dialog" class="modal fade" data-backdrop="static">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body row">
        <div class="col-md-12 text-right prl">
          <button class="modal-close glyphicon glyphicon-remove" type="button" data-dismiss="modal" aria-hidden="true"></button>
        </div>

        <h3 class="col-md-12 text-center u-primary u-mb4">
          <b>Información del consumidor</b>
        </h3>

    		<div class="col-md-12 u-mb3 u-center u-color-error titulo-error" id="customer_error"></div>

      	{!! Form::open(array('id'=>'form_customers','role' => 'form', 'files' => true, 'enctype' => 'multipart/form-data')) !!}
      		<input type="hidden" name="_method" id="customer_method" value="PUT" />
      		<input type="hidden" name="customer_id" id="customer_id" value="">
          <div class="col-xs-10 u-px0 col-xs-offset-1 u-mb4">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">Tipo: </label>
                <select class="form-control" id="customer_type" name="customer_type">
                  <option value="1">Persona</option>
                  <option value="2">Empresa</option>
                </select>
              </div>

              <div class="form-group customer_type-2">
                <label class="control-label">RUC: </label>
                <input class="form-control" name="ruc" id="customer_ruc" placeholder="RUC...">
              </div>

              <div class="form-group customer_type-2">
                <label class="control-label">Razón social: </label>
                <input class="form-control" name="legal_name" id="customer_legal-name" placeholder="Razon social...">
                <div class="mensaje-error" id="customer-error_legal-name"></div>
              </div>

              <div class="form-group customer_type-1">
                <label class="control-label">Nombres: </label>
                <input class="form-control" name="first_name" id="customer_firstname" placeholder="Nombres...">
                <div class="mensaje-error" id="customer-error-firstname"></div>
    		      </div>

              <div class="form-group customer_type-1">
                <label class="control-label">Apellidos: </label>
                <input class="form-control" name="last_name" id="customer_lastname" placeholder="Apellidos...">
                <div class="mensaje-error" id="customer-error-lastname"></div>
              </div>

              <div class="form-group">
                <label class="control-label">Email: </label>
		            <input class="form-control" name="email" placeholder="Email..." id="customer_email">
                <div class="mensaje-error" id="customer-error-email"></div>
              </div>
    	  </div>
          <div class="col-md-6">
              <div class="form-group">
                  <label class="control-label">Facebook: </label>
                  <input class="form-control" name="facebook" id="customer_facebook" placeholder="Facebook...">
              </div>

              <div class="form-group">
                  <label class="control-label">Ciudad: </label>
                  <input class="form-control" name="city" id="customer_city" placeholder="Ciudad...">
              </div>

              <div class="form-group">
                  <label class="control-label">Pais: </label>
                  <input class="form-control" name="country" id="customer_country" placeholder="Pais...">
              </div>

              <div class="form-group">
                <label class="control-label">Cel: </label>
                <input class="form-control" name="cel_whatsapp" id="customer_cel" placeholder="Celular...">
              </div>
          </div>
        </div>

        {!! Form::close() !!}

        <div class="col-md-12 mbl text-center">
          <button type="button" class="btn btn-primary btn-modal" id="customer_save">CREAR</button>
          <button type="button" class="btn btn-primary btn-modal" id="customer_update">GUARDAR</button>
        </div>
      </div>
    </div>
  </div>
</div>
