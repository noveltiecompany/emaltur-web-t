<div id="add-services" tabindex="-1" aria-hidden="true" role="dialog" class="modal fade" data-backdrop="static">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body row">
        <div class="col-md-12 text-right prl">
          <button class="modal-close glyphicon glyphicon-remove" type="button" data-dismiss="modal" aria-hidden="true"></button>
        </div>

        <h3 class="col-md-12 text-center u-primary u-mb4">
          <b>Información</b>
        </h3>

    		<div class="col-md-12 u-mb3 u-center u-color-error"></div>

      	{!! Form::open(array('id'=>'form_services','role' => 'form', 'files' => true, 'enctype' => 'multipart/form-data')) !!}
      		<input type="hidden" name="_method" id="service_method" value="PUT" />
      		<input type="hidden" name="service_id" id="service_id" value="">
          <div class="col-xs-10 u-px0 col-xs-offset-1 u-mb4">

            <div class="col-md-12">
                <div class="col-md-4">
                    <div class="col-md-12 form-group">
                        <label class="control-label" id="">
                            <i class="glyphicon glyphicon-camera"></i>Ícono: (200x200px) .PNG
                        </label>
                        <div class="dropzone" id="div_image-container_services-modal">
                            <div class="dropzone_image"  id="service_preview_image" style="background-color: gray;">
                            </div>
                            <input type="file" accept="image/jpeg, image/png" name="service_image" id="service_image" value="">
                        </div>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Seleccione servicio: </label>
                            <select name="product_id" class="form-control" id="service_product-id">
                                <option value="1">Servicio Uno</option>
                            </select>
                            <div class="mensaje-error" id="service-error-name"></div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Formato: </label>
                            <select name="format" class="form-control" id="service_format">
                                <option value="1">Primer formato</option>
                                <option value="2">Segundo formato</option>
                            </select>
                            <div class="mensaje-error" id="service-error-format"></div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Nombre: </label>
                            <input class="form-control" name="name" id="service_name" placeholder="Nombre de la caracteristica">
                            <div class="mensaje-error" id="service-error-name"></div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Título: </label>
                            <input class="form-control" name="title" id="service_title" placeholder="#Título de la caracteristica">
                            <div class="mensaje-error" id="service-error-title"></div>
                        </div>
                    </div>

                    <div class="col-md-6 second-format">
                        <div class="form-group">
                            <label class="control-label">Precio: </label>
                            <input class="form-control" name="price" id="service_price" placeholder="Precio">
                            <div class="mensaje-error" id="service-error-price"></div>
                        </div>
                    </div>

                    <div class="col-md-6 second-format">
                         <div class="form-group">
                            <label class="control-label">Concepto del precio: </label>
                            <input class="form-control" name="price_concept" id="service_price-concept" placeholder="Concepto del precio">
                            <div class="mensaje-error" id="service-error-price-concept"></div>
                        </div>
                    </div>

                    <div class="col-md-12 first-format">
                        <div class="form-group">
                            <label class="control-label">Pasos: (Separe con puntos ".") </label>
                            <textarea class="form-control" name="steps" placeholder="Redacte los pasos y sepárelos por puntos (.)." id="service_steps" rows="4" cols="40"></textarea>
                            <div class="mensaje-error" id="service-error-steps"></div>
                        </div>
                    </div>

                    <div class="col-md-12 second-format">
                        <div class="form-group">
                            <label class="control-label">Descripción: </label>
                            <textarea class="form-control" name="description" placeholder="Redacte una descripción y/o condiciones" id="service_description" rows="4" cols="40"></textarea>
                            <div class="mensaje-error" id="service-error-description"></div>
                        </div>
                    </div>
                </div>
            </div>

         </div>
        {!! Form::close() !!}

        <div class="col-md-12 mbl text-center">
          <button type="button" class="btn btn-primary btn-modal" id="service_save">CREAR</button>
          <button type="button" class="btn btn-primary btn-modal" id="service_update">GUARDAR</button>
        </div>
      </div>
    </div>
  </div>
</div>
