<div id="headquarter-modal" tabindex="-1" aria-hidden="true" role="dialog" class="modal fade" data-backdrop="static">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body row">
        <div class="col-md-12 text-right prl">
          <button class="modal-close glyphicon glyphicon-remove" type="button" data-dismiss="modal" aria-hidden="true"></button>
        </div>

        <h3 class="col-md-12 text-center u-primary u-mb4">
          <b>Sede</b>
        </h3>

    		<div id="value-error" class="col-md-12 u-mb3 u-center u-color-error titulo-error"></div>

      	{!! Form::open(array('id'=>'form_headquarter','role' => 'form', 'files' => true, 'enctype' => 'multipart/form-data')) !!}
      		<input type="hidden" name="id" id="headquarter_id" value="">
          <div class="col-xs-10 u-px0 col-xs-offset-1 u-mb4">
            <div class="col-md-6 u-px0">
              <div class="col-md-12 form-group">
                <label class="control-label" id="">
                  <i class="glyphicon glyphicon-camera"></i>Imagen: (500x500px)
                </label>
                <div class="dropzone" id="headquarter_container-image">
                  <div class="dropzone_image"  id="headquarter_preview-image">
                  </div>
                  <input type="file" accept="image/jpeg, image/png" name="headquarter_image" id="headquarter_image" value="">
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">Titulo: </label>
                <input class="form-control" name="title" id="headquarter_title" placeholder="Ingrese el título">
                <div class="mensaje-error" id="headquarter-error-title"></div>
    		      </div>
    			  </div>
          </div>
        {!! Form::close() !!}
        <div class="col-md-12 mbl text-center">
          <button type="button" class="btn btn-primary btn-modal" id="headquarter__save">CREAR</button>
          <button type="button" class="btn btn-primary btn-modal" id="headquarter__update">GUARDAR</button>
        </div>
      </div>
    </div>
  </div>
</div>
