<div id="add-video" tabindex="-1" aria-hidden="true" role="dialog" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body u-py4 row">
        <div class="col-md-12 text-right u-pr3">
          <button class="modal-close glyphicon glyphicon-remove" type="button" data-dismiss="modal" aria-hidden="true"></button>
        </div>

        <h3 class="col-md-12 text-center u-primary u-mb4 u-mt0">
          <b>Datos del Video</b>
        </h3>

    		<div id="company-video-error" class="col-md-12 u-color-error u-center u-mb3"></div>
    		{!! Form::open(array('id' => 'form_company_video', 'role' => 'form', 'files' => true, 'enctype' => 'multipart/form-data')) !!}
    			<input type="hidden" id="company_video_method" name="_method" value="PUT" />
    			<input type="hidden" id="company_video_id" name="company_video_id" value="">
    			<div class="col-xs-10 u-px0 col-xs-offset-1 u-mb3">
            <div class="col-md-12 form-group">
            	<label class="control-label">
              	<i class="glyphicon glyphicon-file"></i>Título del Video:
            	</label>
            	<input type="text" class="form-control" id="company_video_content" name="company_video_name" placeholder="Ingrese el título del vídeo">
  		      	<div id="company-video-error-name" class="mensaje-error"></div>
	  	      </div>

            <div class="col-md-12 form-group">
              <label class="control-label">
                <i class="glyphicon glyphicon-file"></i>URL del Video:
              </label>
              <input type="text" class="form-control" id="company_video_resource" name="company_video_link" placeholder="Copiar URL. Ejm: https://www.youtube.com/watch?v=xBU_bVxph-Y">
    		      <div id="company-video-error-link" class="mensaje-error"></div>
    			  </div>
          </div>
        {!! Form::close() !!}

        <div class="col-md-12 u-mb4 text-center">
          <button type="button" class="btn btn-primary btn-modal" id="company_save_video">AÑADIR VIDEO</button>
          <button type="button" class="btn btn-primary btn-modal" id="company_update_video">ACTUALIZAR VIDEO</button>
        </div>
      </div>
    </div>
  </div>
</div>
