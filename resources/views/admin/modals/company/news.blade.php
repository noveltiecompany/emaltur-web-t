<div id="add-news" tabindex="-1" aria-hidden="true" role="dialog" class="modal fade" data-backdrop="static">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body row">
        <div class="col-md-12 text-right prl">
          <button class="modal-close glyphicon glyphicon-remove" id="btn-figure-x_modal-news__close" type="button" data-dismiss="modal" aria-hidden="true"></button>
        </div>

        <h3 class="col-md-12 text-center u-primary u-mb4 u-mt0">
          <b id="label-news-title">Información de la Publicación</b>
        </h3>

    	<div id="post-error" class="col-md-12 u-mb3 u-center u-color-error titulo-error"></div>

      	{!! Form::open(array('class' => 'col-md-12','id'=>'post-form', 'role' => 'form', 'files' => true, 'enctype' => 'multipart/form-data')) !!}
			<input type="hidden" id="post-method" name="_method" value="PUT" />
			<input type="hidden" id="post-id" name="post_id" value="">
			<div class="col-xs-10 u-px0 col-xs-offset-1 u-mb4">

				<div class="col-md-8">
					<div class="form-group">
						<label class="control-label">Titulo: </label>
						<input class="form-control" id="post-title" name="title" placeholder="Titulo de la publicación">
						<div class="mensaje-error" id="post-title-error"></div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label">Categoría:</label>
						<select class="form-control" name="tag_id" id="post_tag-id">


						</select>
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
						  <label class="control-label">Descripción: </label>
						  <textarea rows="10" class="form-control" id="post-content" name="content" placeholder="Redacción de la publicación"></textarea>
						  <div class="mensaje-error" id="post-content-error"></div>
					</div>
				</div>
			</div>
        {!! Form::close() !!}

        <div class="col-md-10 col-xs-10 u-mb4 col-xs-offset-1 u-px0" id="post-dropzone-area">
          <div class="col-xs-5" id="post-info__after-saved">
            <div class="u-info u-flex u-items-center u-justify-between">
              <span class="u-mr2 u-fw-bold">Título</span>
              {{-- <button class="btn btn-success">Editar</button> --}}
            </div>

            <div class="u-info" id="post-title__after-saved">
              Titulo de la noticia
            </div>

<!-- 			<div class="u-info u-fw-bold">
              Descripción
            </div>

            <div class="u-info" id="post-content__after-saved">
              Descripción de la noticia
            </div> -->
          </div>
          <div class="col-xs-7">
            {!! Form::open(['route'=> 'upload-post-images', 'method' => 'POST', 'files'=>'true', 'id' => 'post-dropzone' , 'class' => 'dropzone']) !!}
            {!! Form::close() !!}
			<hr>
			<div id="post-swiper-container" class="swiper-container" data-number style="text-align: center;">
				<div class="swiper-wrapper">
					<div class="swiper-slide">Slide 1</div>
					<div class="swiper-slide">Slide 1</div>
					<div class="swiper-slide">Slide 1</div>
					<div class="swiper-slide">Slide 1</div>
					<div class="swiper-slide">Slide 1</div>
					<div class="swiper-slide">Slide 1</div>
				</div>

				<div id="post-swiper-pagination" style="display: inline-block;"></div>
				<div id="post-swiper-button-next"></div>
				<div id="post-swiper-button-prev"></div>
			</div>

			{{-- Pruebas --}}
			{{-- <hr>
			<div id="post-slick-container">
				  <div>1</div>
				  <div>2</div>
				  <div>3</div>
			</div> --}}
			{{-- End Pruebas --}}
          </div>
        </div>

        <div class="col-md-12 mbl text-center">
          	<button type="button" class="btn btn-primary btn-modal" id="post-button__save">Siguiente -></button>
	        <button type="button" class="btn btn-primary btn-modal" id="post-button__update">Guardar</button>
			<button type="button" class="btn btn-primary btn-modal" id="post-button__close">Finalizar</button>
        </div>
      </div>
    </div>
  </div>

	<script type="text/javascript">
		// setTimeout(function () {
		// 	console.log("dsokko");
		// 	swiperPost = new Swiper('#post-swiper-container', {
		// 		loop: false,
		// 		pagination: '#post-swiper-pagination',
		// 		nextButton: '#post-swiper-button-next',
		// 		prevButton: '#post-swiper-button-prev',
		// 		slidesPerView: 3,
		// 		//centeredSlides: true,
		// 		//paginationClickable: true,
		// 		spaceBetween: 30,
		// 	});
		// }, 1000);
	</script>

</div>
