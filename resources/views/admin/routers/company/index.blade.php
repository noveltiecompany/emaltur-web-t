@extends('admin.layouts.index')

@section('content')
  <div class="col-md-12 u-px0 u-bg-white u-shadow-bottom">
    <ul class="col-md-12 nav nav-tabs ul-edit u-pr0">
      <li class="active"><a href="#tab-info" data-toggle="tab">Datos Generales</a></li>
      <li><a href="#tab-cover" data-toggle="tab">Portada</a></li>
      <li><a href="#tab-users" data-toggle="tab">Usuarios</a></li>
{{--       <li><a href="#tab-products" data-toggle="tab">Servicios</a></li>
 --}}{{--       <li><a href="#tab-services" data-toggle="tab">Características</a></li>
 --}}<!--       <li><a href="#tab-customers" data-toggle="tab">Clientes</a></li>
-->  <li><a href="#tab-activities" data-toggle="tab">Rubros</a></li>
     <li><a href="#tab-seals" data-toggle="tab">Sellos</a></li>
     <li><a href="#tab-associates" data-toggle="tab">Asociados</a></li>
      <li><a href="#tab-news" data-toggle="tab">Publicaciones</a></li>

      {{-- <li><a href="#tab-headquarters" data-toggle="tab">Sedes</a></li> --}}
    </ul>
  </div>

  <div class="tab-content u-px0">
    <div id="tab-cover" class="tab-pane fade in active">
      @include('admin.routers.company.cover')
    </div>

    <div id="tab-users" class="tab-pane fade">
      @include('admin.routers.company.users')
    </div>

    <div id="tab-info" class="tab-pane fade">
      @include('admin.routers.company.info')
    </div>

{{--     <div id="tab-services" class="tab-pane fade ">
      @include('admin.routers.company.services')
    </div> --}}

    <div id="tab-customers" class="tab-pane fade">
      @include('admin.routers.company.customers')
    </div>

    <div id="tab-news" class="tab-pane fade ">
      @include('admin.routers.company.news')
    </div>

    <div id="tab-activities" class="tab-pane fade ">
      @include('admin.routers.company.activities')
    </div>

    <div id="tab-seals" class="tab-pane fade ">
      @include('admin.routers.company.seals')
    </div>

    <div id="tab-associates" class="tab-pane fade ">
      @include('admin.routers.company.associates')
    </div>

{{--     <div id="tab-headquarters" class="tab-pane fade ">
      @include('admin.routers.company.headquarters')
    </div> --}}

{{--     <div id="tab-products" class="tab-pane fade ">
      @include('admin.routers.company.products')
    </div> --}}

  </div>
@endsection

@section('scripts')
  {{-- Company --}}
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Company/actions.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Company/scripts/loadCompany.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Company/scripts/loadCompanyVideos.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Company/scripts/loadCompanyImages.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Company/scripts/saveCompanyInfo.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Company/scripts/saveCompanySlogan.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Company/scripts/saveCompanyVideo.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Company/scripts/getVideoToEdit.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Company/scripts/updateCompanyVideo.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Company/scripts/deleteCover.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Company/scripts/deleteVideo.js') }}"></script>

  {{-- Services --}}
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Services/scripts/loadGridServices.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Services/actions.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Services/scripts/getServiceToEdit.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Services/scripts/saveService.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Services/scripts/updateService.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Services/scripts/deleteService.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Services/scripts/getImagesToEdit.js') }}"></script>

  {{-- Posts --}}
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Posts/actions.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Posts/scripts/loadGridPosts.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Posts/scripts/getPostToEdit.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Posts/scripts/savePost.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Posts/scripts/updatePost.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Posts/scripts/deletePost.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Posts/scripts/publishedPost.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Posts/scripts/getPostToEditImages.js') }}"></script>

  {{-- Customers --}}
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Customers/actions.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Customers/scripts/loadGridCustomers.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Customers/scripts/deleteCustomer.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Customers/scripts/getCustomerToEdit.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Customers/scripts/saveCustomer.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Customers/scripts/updateCustomer.js') }}"></script>

{{-- Users --}}
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Users/scripts/loadGridUsers.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Users/scripts/deleteUser.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Users/scripts/getUserToEdit.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Users/scripts/saveUser.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Users/scripts/updateUser.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Users/scripts/suspendUser.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Users/scripts/activateUser.js') }}"></script>

  {{-- Productos --}}
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Products/actions.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Products/scripts/getProductToEditImages.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Products/scripts/changeStatusPublishedProduct.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Products/scripts/saveProduct.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Products/scripts/getProductToEdit.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Products/scripts/updateProduct.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Products/scripts/deleteProduct.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Products/scripts/loadGridProducts.js') }}"></script>

  {{-- HeadQuarters --}}
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/HeadQuarters/headQuarters.js') }}"></script>

  {{-- Activities --}}
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Activities/activities.js') }}"></script>

  {{-- Seals --}}
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Seals/seals.js') }}"></script>

  {{-- Associates --}}
  <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Associates/associates.js') }}"></script>

  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5DgtOXdGEIDFUDkT9jK_EfN-UJIElU_0&libraries=places"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-locationpicker/0.1.12/locationpicker.jquery.min.js"></script>

@endsection
