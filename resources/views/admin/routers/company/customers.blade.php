<div class="col-md-12">
  <div class="tab-wrapper row u-px3 u-py5">
    <div class="col-md-12 u-mb4">
      <button type="button" class="btn btn-primary" id="customer_add" data-target="#add-customers" data-toggle="modal">
        <i class="glyphicon glyphicon-plus u-mr2"></i>Nuevo cliente
      </button>
    </div>

    <ul class="col-md-12" id="customers_grid">

    </ul>

  </div>
</div>
