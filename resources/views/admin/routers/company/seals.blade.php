<div class="col-md-12">
  <div class="tab-wrapper row u-px3 u-py5">
    <div class="col-md-12 u-mb4">
      <button type="button" class="btn btn-primary" id="seal__add" data-target="" data-toggle="modal">
        <i class="glyphicon glyphicon-plus u-mr2"></i>Nuevo Sello
      </button>
    </div>

    <ul class="col-md-12" id="seals_grid">

    </ul>
  </div>
</div>