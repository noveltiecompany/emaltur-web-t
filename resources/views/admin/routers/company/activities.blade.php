<div class="col-md-12">
  <div class="tab-wrapper row u-px3 u-py5">
    <div class="col-md-12 u-mb4">
      <button type="button" class="btn btn-primary" id="activity__add" data-target="" data-toggle="modal">
        <i class="glyphicon glyphicon-plus u-mr2"></i>Nueva actividad
      </button>
    </div>

    <ul class="col-md-12" id="activities_grid">

    </ul>
  </div>
</div>