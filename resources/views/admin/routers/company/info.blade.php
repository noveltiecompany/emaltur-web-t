<div class="col-md-12">
  <div class="tab-wrapper row u-px4 u-py5">
	  <div id="divPhotos"></div>
	  <div id="divContents"></div>
	  <div id="divExtensions"></div>

  <div class="col-md-12 text-center u-mb4">
	  <div id="company-error" class="col-md-10 titulo-error"></div>
  </div>

  {!! Form::open(array('id' => 'form_company', 'role' => 'form', 'files' => true, 'enctype' => 'multipart/form-data')) !!}
    <input type="hidden" id="company_method" name="_method" value="PUT" />
    <input type="hidden" id="company_id" name="company_id" value="">
    <div class="col-md-4">
      <div class="form-group">
        <label class="control-label">
          <i class="glyphicon glyphicon-file"></i>Título de la Web:
        </label>
        <input type="text" class="form-control" id="company_name" name="name_company" placeholder="Escribe el nombre de la empresa">
	      <div id="company-error-name" class="mensaje-error"></div>
      </div>

      <div class="form-group">
        <label class="control-label">
          <i class="glyphicon glyphicon-camera"></i>Logo color: (Max. 2MB)
        </label>
        <div id="dropzone_company" class="dropzone">
          <label id="company-preview-image-text">
            <i class="glyphicon glyphicon-picture"></i>
            <span class="u-ml3">Añadir Foto</span>
          </label>
          <div class="dropzone_image" id="preview_logotype">
          </div>
          <input id="company_logo" name="company_logo" type="file" accept="image/gif, image/jpeg, image/png">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label">
          <i class="glyphicon glyphicon-camera"></i>Logo blanco: (Max. 2MB)
        </label>
        <div id="dropzone_company" class="dropzone">
          <label id="company-preview-image-white-text">
            <i class="glyphicon glyphicon-picture"></i>
            <span class="u-ml3">Añadir Foto</span>
          </label>
          <div class="dropzone_image" id="preview_logotype_white" style="background-color: #3e3e3e;">
          </div>
          <input id="company_logo_white" name="company_logo_white" type="file" accept="image/gif, image/jpeg, image/png">
        </div>
      </div>

    </div>

    <div class="col-md-4">
        <div class="form-group">
          <div class="box box-solid">
             <div class="box-body">
              <div class="box-group" id="accordion">
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                <div class="panel box box-danger">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        Presentación
                      </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="box-body">
                      <div class="form-group">
                        <textarea rows="9" cols="40" class="form-control" name="description_company" id="company_description" placeholder="Breve descripción de la Organización"></textarea>
                        </div>
                      </div>
                    </div>
                </div>

                <div class="panel box box-primary">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                        Nuestra Historia
                      </a>
                    </h4>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse">
                    <div class="box-body">
                      <div class="form-group">
                        <textarea rows="9" cols="40" class="form-control" id="company_our-history" name="our_history" placeholder="Historia de la Organización"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel box box-danger">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                        Visión y misión
                      </a>
                    </h4>
                  </div>
                  <div id="collapseThree" class="panel-collapse collapse">
                    <div class="box-body">
                      <div class="form-group">
                        <textarea rows="9" cols="40" class="form-control" name="vision" id="company_vision" placeholder="Redacta la Visión y Misión"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="panel box box-primary">
                    <div class="box-header with-border">
                      <h4 class="box-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                          Nuestros objetivos
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                      <div class="box-body">
                        <div class="form-group">
                          <textarea rows="9" cols="40" class="form-control" name="our_objectives" id="company_our-objectives" placeholder="Describa los objetivos de la Organización"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="panel box box-primary">
                    <div class="box-header with-border">
                      <h4 class="box-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                          Propuesta de valor
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse">
                      <div class="box-body">
                        <div class="form-group">
                          <textarea rows="9" cols="40" class="form-control" name="value_proposal" id="company_value-proposal" placeholder="Propuesta de valor de la Organización"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="panel box box-primary">
                    <div class="box-header with-border">
                      <h4 class="box-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                          Junta Directiva
                        </a>
                      </h4>
                    </div>
                    <div id="collapseSix" class="panel-collapse collapse">
                      <div class="box-body">
                        <div class="form-group">
                          <textarea rows="9" cols="40" class="form-control" name="directors" id="company_directors" placeholder="Junta directiva de la Organización"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="panel box box-primary">
                    <div class="box-header with-border">
                      <h4 class="box-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
                          Beneficios (Separe con punto ".")
                        </a>
                      </h4>
                    </div>
                    <div id="collapseSeven" class="panel-collapse collapse">
                      <div class="box-body">
                        <div class="form-group">
                          <textarea rows="9" cols="40" class="form-control" name="benefits" id="company_benefits" placeholder="Redacte aquí los beneficios de la organizaciòn y separe con puntos (.) cada beneficio"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="panel box box-primary">
                    <div class="box-header with-border">
                      <h4 class="box-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
                          Requisitos (Separe con punto ".")
                        </a>
                      </h4>
                    </div>
                    <div id="collapseEight" class="panel-collapse collapse">
                      <div class="box-body">
                        <div class="form-group">
                          <textarea rows="9" cols="40" class="form-control" name="requirements" id="company_requirements" placeholder="Redacte aquí los requisitos para ser parte de la organización y separe con puntos (.) cada requisito"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
    </div>

    <div class="col-md-4">
      <div class="form-group">
        <label class="control-label">
          <i class="glyphicon glyphicon-send"></i>Datos de Contacto de la Empresa:
        </label>
        <input type="text" class="form-control u-mb3" id="company_cel" name="cel" placeholder="Celular">
        <input type="text" class="form-control u-mb3" id="company_phone" name="phone" placeholder="Teléfono">
        <input type="email" class="form-control u-mb3" id="company_email" name="email" placeholder="Correo">
	      <div id="company-error-email" class="mensaje-error"></div>
        <input type="text" class="form-control u-mb3" id="company_facebook" name="facebook" placeholder="Url fan page facebook">
    		<input type="text" class="form-control u-mb3" id="company_twitter" name="twitter" placeholder="Url Twitter">
        <input type="text" class="form-control u-mb3" id="company_youtube" name="youtube" placeholder="Url Youtube">
        <input type="text" class="form-control u-mb3" id="company_instagram" name="instagram" placeholder="Url Instagram">
        <input type="text" class="form-control u-mb3" id="company_pinterest" name="pinterest" placeholder="Url Pinterest">
        <input type="text" class="form-control u-mb3" id="company_linkedin" name="linkedin" placeholder="Url Linkedin">

        <input type="text" class="form-control u-mb3" id="company_google-plus" name="google_plus" placeholder="Url Google+">
    		<textarea rows="3" cols="40" class="form-control u-mb3" id="company_address" name="address" placeholder="Dirección"></textarea>
        <textarea rows="3" cols="40" class="form-control u-mb3" id="company_schedules" name="schedules" placeholder="Horarios de atención"></textarea>
        <input type="text" class="form-control u-mb3" id="company_city" name="city" placeholder="Ciudad">
	      <div id="company-error-city" class="mensaje-error"></div>
        <input type="text" class="form-control u-mb3" id="company_country" name="country" placeholder="País">
	      <div id="company-error-country" class="mensaje-error"></div>
      </div>
    </div>

    <div class="col-md-12 text-right">
      <button type="button" class="btn btn-success" id="company-info-save">
        <i class="glyphicon glyphicon-floppy-disk u-mr2"></i>Actualizar información
      </button>
    </div>
  {!! Form::close() !!}
  </div>
</div>
