<div class="col-md-12">
  <div class="tab-wrapper row u-px3 u-py5">
    <div class="col-md-12 u-mb4">
      <button type="button" class="btn btn-primary" id="associated__add" data-target="" data-toggle="modal">
        <i class="glyphicon glyphicon-plus u-mr2"></i>Nuevo asociado
      </button>
    </div>

    <ul class="col-md-12" id="associates_grid">

    </ul>

    <div class="col-md-12" id="associates_actions" style="display: none;">

        <h3 class="col-md-12 text-center u-primary u-mb4">
            <b>Información del asociado</b>
        </h3>

        <div class="col-md-12 u-mb3 u-center u-color-error titulo-error" id="associated_error"></div>

        {!! Form::open(array('id'=>'form_associated','role' => 'form', 'files' => true, 'enctype' => 'multipart/form-data')) !!}

            <input type="hidden" name="id" id="associated_id" value="">

            <div class="col-xs-10 u-px0 col-xs-offset-1 u-mb4">

                <div class="col-md-6 u-px0">

                    <div class="col-md-12 form-group" style="padding-left: 0px;">
                        <label class="control-label" id="">
                            <i class="glyphicon glyphicon-camera"></i>Logo de la empresa:
                        </label>
                        <div class="dropzone" id="associated_image-container">
                            <div class="dropzone_image"  id="associated_preview-image">
                            </div>
                            <input type="file" accept="image/jpeg, image/png" name="image" id="associated_image" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Rubro al que representa: </label>
                        <select name="activity_id" class="form-control" id="associated_activities"></select>
                        <div class="mensaje-error" id="associated-error-activity"></div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Sellos asignados: </label>
                        <select name="type" multiple="multiple" style="width: 100%;" class="form-control" id="associated_seals">
                            <option value="1">Socio</option>
                            <option value="2">Aliado</option>
                            <option value="3">Proveedor</option>
                            <option value="4">Invitada</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Nombre de la Empresa: </label>
                        <input class="form-control" name="name_company" id="associated_name" placeholder="Ingrese el nombre de la empresa">
                        <div class="mensaje-error" id="troop_error_company-registration"></div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Representante legal / Dueño del negocio: </label>
                        <input class="form-control" name="representative" id="associated_representative" placeholder="Ingrese nombres y apellidos">
                        <div class="mensaje-error" id="troop-error-representative"></div>
                    </div>

                </div>

                <div class="col-md-6 u-px0">

                    <div class="form-group">
                        <label class="control-label">Email: </label>
                        <input class="form-control" name="email" id="associated_email" placeholder="Ingrese su Email">
                        <div class="mensaje-error" id="troop-error-email"></div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Celular / Whatsapp: </label>
                        <input class="form-control" name="cel" id="associated_cel" placeholder="Ingrese su Celular / Whatsapp">
                        <div class="mensaje-error" id="troop-error-cel"></div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Teléfono: </label>
                        <input class="form-control" name="phone" id="associated_phone" placeholder="Ingrese su teléfono de oficina">
                        <div class="mensaje-error" id="troop-error-phone"></div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Sitio Web: </label>
                        <input type="text" class="form-control" name="website" id="associated_website" placeholder="Ingrese la dirección del sitio web . Ejm. http://www.miempresa.com">
                    </div>

                    <div class="form-group">
                        <label class="control-label">Fanpage Facebook: </label>
                        <input class="form-control" name="facebook" id="associated_facebook" placeholder="Ingrese su teléfono de oficina">
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label">Dirección: </label>
                        <input class="form-control" name="address" id="associated_address" placeholder="Dirección...">
                        <div class="mensaje-error" id="associated-error-address"></div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Ubícate en el mapa: </label>
                        <input class="form-control" name="" id="associated_geolocation" placeholder="Dirección...">
                        <div class="mensaje-error" id="associated-error-address"></div>
                        <input type="hidden" class="form-control u-mb3" id="latitud" name="latitude"/>
                        <input type="hidden" class="form-control u-mb3" id="longitud" name="longitude"/>
                        <div id="location" style="width: 100%; height: 200px;">

                        </div>
                    </div>
                </div>
            </div>

        {!! Form::close() !!}
        <div class="col-md-12 mbl text-center">
            <button type="button" class="btn btn-primary btn-modal" id="associated__save">CREAR</button>
            <button type="button" class="btn btn-primary btn-modal" id="associated__update">EDITAR</button>
            <button type="button" class="btn btn-primary btn-modal" id="associated__close">CERRAR</button>
        </div>


    </div>

    <div class="col-md-12" id="associates_images" style="display: none;">

        <h3 class="col-md-12 text-center u-primary u-mb4">
            <b>Imagenes del asociado</b>
        </h3>

        <input type="hidden" id="associated-image_id">

        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                {!! Form::open(['route'=> 'upload-associated-images', 'method' => 'POST', 'files'=>'true', 'id' => 'associated_dropzone' , 'class' => 'dropzone']) !!}
                {!! Form::close() !!}
                <hr>
                <div id="associated-swiper-container" class="swiper-container" data-number style="text-align: center;">
                    <div class="swiper-wrapper">
                    <div class="swiper-slide">Slide 1</div>
                    <div class="swiper-slide">Slide 1</div>
                    </div>

                    <div id="associated-swiper-pagination" style="display: inline-block;"></div>
                    <div id="associated-swiper-button-next"></div>
                    <div id="associated-swiper-button-prev"></div>
                </div>


            </div>
            <div class="col-md-1"></div>

            <div class="col-md-12 mbl text-center">
                <button type="button" class="btn btn-primary btn-modal" id="associated-image__close">CERRAR</button>
            </div>

        </div>
    </div>


  </div>
</div>
