<div class="col-md-12">
  <div class="tab-wrapper row u-px3 u-py5">
    <div class="col-md-12 u-mb4">
      
      <button type="button" class="btn btn-primary" id="user_add" data-target="#add-users" data-toggle="modal">
        <i class="glyphicon glyphicon-plus u-mr2"></i>Nuevo Usuario
      </button>
    </div>
    <ul class="col-md-12" id="users_grid">
    </ul>
  </div>
</div>
