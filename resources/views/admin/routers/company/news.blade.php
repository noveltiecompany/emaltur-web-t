<div class="col-md-12">
  <div class="tab-wrapper row u-px3 u-py5">
    <div class="col-md-12 u-mb4">
      <button type="button" class="btn btn-primary" id="post-button__create" data-target="#add-news" data-toggle="modal">
        <i class="glyphicon glyphicon-plus u-mr2"></i>Nueva Publicación
      </button>
    </div>

    {{-- Modificar la lista --}}
    <ul class="col-md-12" id="posts-grid">
      {{-- @for($i=0; $i < 2 ; $i++)
        <div class="col-lg-3 col-md-4 col-sm-6 u-px2">
          <li class="item-account">
            <figure class="item-image">
              <img src="" alt="" />
            </figure>
            <div class="item__controls">
              <button type="button" class="btn btn-warning" title="Publicar">
                <i class="glyphicon glyphicon-eye-open"></i>
              </button>
              <button type="button" class="btn btn-success" data-target="#add-news" data-toggle="modal" title="Editar">
                <i class="glyphicon glyphicon-pencil"></i>
              </button>
              <button type="button" class="btn btn-danger" title="Eliminar">
                <i class="glyphicon glyphicon-trash"></i>
              </button>
            </div>
          </li>
        </div>
      @endfor --}}
    </ul>
  </div>
</div>
