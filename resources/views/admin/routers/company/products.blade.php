<div class="col-md-12">
  <div class="tab-wrapper row u-px3 u-py5">
    <div class="col-md-12 u-mb4">
      <button type="button" class="btn btn-primary" id="product__add" data-target="#product-modal" data-toggle="modal">
        <i class="glyphicon glyphicon-plus u-mr2"></i>Nuevo Servicio
      </button>
    </div>
    <ul class="col-md-12" id="products_grid">
    </ul>
  </div>
</div>
