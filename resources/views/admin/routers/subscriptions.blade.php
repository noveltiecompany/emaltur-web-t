@extends('admin.layouts.index')
@section('content')
  <header class="c-sub-header u-bg-white">
    <div class="u-flex u-flex-1">

    </div>
  </header>

	<section class="row u-mx4 u-mb4 u-mt5 u-px4 u-pt5 u-bg-white" id="order-grid">
		<div class="col-md-12 u-mb4" id="excel-container">
			<a href="{{ route('excel') }}" class="btn btn-primary">Exportar en Excel</a>
		</div>

	@foreach($subscriptions as $i => $subscription)
		<div class="col-lg-3 col-md-4 col-sm-6">
		  <button class="c-order view-subscription" data-subscription_id="{{$subscription->id}}" data-target="#subscription-modal" data-toggle="modal">
				<div class="u-flex u-items-start u-flex-wrap u-justify-between">
					<p class='u-flex u-items-center u-mb2'>
						<i class='glyphicon glyphicon-calendar u-color-primary u-mr2'></i>
						{{$subscription->created_at}}
					</p>
				</div>
			<h3 class='u-color-primary'>{{$subscription->code}}</h3>

			<hr class='u-mt0'>

			<div class="u-flex u-flex-wrap u-line-1 u-mb2 u-fz-h2">
			  <span class='u-mr2'>Email:</span>
			  <p class='u-color-text u-mb0'>{{$subscription->email}}</p>
			</div>

			<div class="u-flex u-flex-wrap u-line-1 u-mb2 u-fz-h2">
			  <span class='u-mr2'>Nombres:</span>
			  <p class='u-color-text u-mb0'>{{$subscription->name}}</p>
			</div>

			<div class="u-flex u-flex-wrap u-line-1 u-mb2 u-fz-h2">
			  <span class='u-mr2'>Teléfono:</span>
			  <p class='u-color-text u-mb0'>{{$subscription->phone}}</p>
			</div>

		  </button>
		</div>
	@endforeach
  </section>
@endsection

@section('scripts')
	<script type="text/javascript" src="{{ URL::asset('admin/panel/js/Subscriptions/actions.js') }}"></script>
@endsection
