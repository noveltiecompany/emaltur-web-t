<header class="c-header">
  <a href="" class="c-header__logo">
    <img class="c-header__logo-image hidden-xs hidden-sm" src="{{App\Company::first()->logotype}}"  />
    <img class="c-header__logo-image hidden-md hidden-lg" src="{{App\Company::first()->logotype_thumb}}"  />
  </a>

  <nav class="c-header__nav">
    <ul class="u-flex--header nav nav-tabs ul-edit">
      <li class="c-header__link"><a href="{{ route('company')}}">La Empresa</a></li>
      <li class="c-header__link"><a href="{{ route('subscriptions')}}">Suscripciones</a></li>
    </ul>

    <ul class="u-flex--header u-pl0">
      <li class="c-header__link " >
        <a href="{{ asset('')}}" target="_blank">Ver Sitio Web</a>
      </li>
      <li class="dropdown c-header__user">
        <button type="button" data-toggle="dropdown" data-hover="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
          <figure>
            <img src="{{Auth::user()->user_image_thumb}}" id="img-user-photo">
          </figure>
          <span class="hidden-xs hidden-sm">{{Auth::user()->first_name}}</span>
          <span class="caret"></span>
        </button>

        <ul class="dropdown-menu dropdown-user pull-right">
          <li><a href="#" id="user_editar_perfil" data-index='{{Auth::id()}}'>Editar Perfil</a></li>
	        {{-- <li><a href="#" id="user_editar_perfil" data-index="{{Auth::user()->id}}">Editar Perfil</a></li> --}}
          <li><a href="#" id="user_editar_password">Cambiar contraseña</a></li>
          <li><a href="{{ route('logout') }}">Cerrar Sesión</a></li>
        </ul>
      </li>
    </ul>
  </nav>
</header>
