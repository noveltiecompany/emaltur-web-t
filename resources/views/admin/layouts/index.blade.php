<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Plataforma | {{ App\Company::first()->name_company }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

  	<link href="{{ URL::asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet">

    {{-- Select2 from web --}}
    <link href="{{ URL::asset('admin/plugins/select2/select2.css') }}" rel="stylesheet">

  	{{-- Dropzone --}}
  	<link href="{{ URL::asset('admin/plugins/dropzone/dropzone.css') }}" rel="stylesheet">

  	{{-- Sweet Alert --}}
  	<link href="{{ URL::asset('admin/plugins/sweet-alert/sweetalert.css') }}" rel="stylesheet">

  	{{-- growl alert --}}
  	<link href="{{ URL::asset('admin/plugins/growl/jquery.growl.css') }}" rel="stylesheet">

    {{-- Owl Carousel --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.6/assets/owl.carousel.min.css" rel="stylesheet">

  	{{-- Swiper --}}
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/css/swiper.min.css">

  	{{-- Slick carousel --}}
  	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

    {{-- Bootstrap --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" rel="stylesheet">

    {{-- Bootstrap Toggle --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet" >

    {{-- Datatables style --}}
    <link href="http://cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" rel="stylesheet" >

    {{-- SummerNote editor --}}
    <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote.css') }}">

	{{-- Estilos del carousel --}}
	<style>
		.swiper-container {
			width: 100%;
			height: 100%;
		}

		.swiper-slide {
			text-align: center;
			font-size: 18px;
			background: #fff;
			/* Center slide text vertically */
			display: -webkit-box;
			display: -ms-flexbox;
			display: -webkit-flex;
			display: flex;
			-webkit-box-pack: center;
			-ms-flex-pack: center;
			-webkit-justify-content: center;
			justify-content: center;
			-webkit-box-align: center;
			-ms-flex-align: center;
			-webkit-align-items: center;
			align-items: center;
		}

	</style>

	{{-- Estilos generales --}}
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('admin/panel/css/custom-app.css') }}"/>


	@yield('styles')
    <link rel="stylesheet" href="{{asset('admin/css/main.css')}}">
  </head>

  <body>
    @include('admin.layouts.header')

    @yield('content')

    @include('admin.modals.company.video')
    @include('admin.modals.company.services')
    @include('admin.modals.company.services.service_images')
    @include('admin.modals.company.news')
    @include('admin.modals.company.customer')
    @include('admin.modals.company.user')
    @include('admin.modals.company.products.product')
    @include('admin.modals.company.headquarter')
    @include('admin.modals.company.activity')
    @include('admin.modals.company.seal')

    @include('admin.modals.subscription.subscription')

    @include('admin.modals.user.profile')
    @include('admin.modals.user.password')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-hover-dropdown/2.2.1/bootstrap-hover-dropdown.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.responsive-tabs/1.6.0/jquery.responsiveTabs.min.js"> </script>

  	{{-- Sweet alert --}}
  	<script type="text/javascript" src="{{ URL::asset('admin/plugins/sweet-alert/sweetalert.min.js') }}"></script>

    {{-- Bootstrap --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

    {{-- Owl Carousel --}}
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.6/owl.carousel.min.js"></script> --}}

    {{-- Bootstrap Toggle --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    {{--Datatable js--}}
    <script type="text/javascript" src="http://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

  	{{-- growl alert --}}
  	<script type="text/javascript" src="{{ URL::asset('admin/plugins/growl/jquery.growl.js') }}"></script>

    {{-- Select2 --}}
    <script type="text/javascript" src="{{ URL::asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js') }}"></script>

	{{-- Swiper --}}
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/js/swiper.min.js"></script>

	{{-- Slick carousel --}}
	<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>

    {{-- Dropzone --}}
    <script type="text/javascript" src="{{ URL::asset('admin/plugins/dropzone/dropzone.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Dropzone/my-dropzone.js') }}"></script>

    {{-- Jquery BlockUI --}}
    <script type="text/javascript" src="https://malsup.github.io/jquery.blockUI.js"></script>

    {{-- Select2 --}}
    <script src="{{asset('admin/plugins/select2/select2.min.js')}}"></script>

    {{-- SummerNote editor --}}
    <script src="{{asset('admin/plugins/summernote/summernote.js')}}"></script>

    {{-- SummerNote español --}}
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/lang/summernote-es-ES.min.js"></script>

    {{-- Funciones generales --}}
    <script type="text/javascript" src="{{ URL::asset('admin/panel/js/custom-app.js') }}"></script>

    {{-- Users --}}
    <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Users/actions.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Users/scripts/userProfileEdit.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('admin/panel/js/Users/scripts/userProfileSave.js') }}"></script>

    {{-- Ajax --}}
    <script type="text/javascript" src="{{ URL::asset('admin/panel/js/ajax.js') }}"></script>

    @yield('scripts')
    <script src="{{asset('admin/js/owl.carousel.js')}}"></script>
    <script src="{{asset('admin/js/main.js')}}"></script>
  </body>
</html>
