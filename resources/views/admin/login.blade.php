<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Iniciar Sesión - {{App\Company::first()->name_company}}</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" rel="stylesheet">
		<link href="{{asset('admin/css/main.css')}}" rel="stylesheet" >
		{{-- <link href="{{asset('admin/css/login.css')}}" rel="stylesheet" > --}}
	</head>

	<body class="c-login">
		<img class="c-login__logo" src='{{App\Company::first()->logotype_thumb}}'  />
		<h4>Bienvenido al Panel de Administración</h4>

		@if ($errors->has('username') || $errors->has('password'))
      <p class="login-box-msg u-color-error u-primary">Username y/o Contraseña Incorrectas</p>
  	@endif

		{!! Form::open(['class' => 'c-login__form ', 'url' => 'login', 'method' => 'POST', 'role' => 'form']) !!}
			{{ csrf_field() }}
			<div class="form-group">
				<input type="text" name="username" class="form-control" placeholder="Ingresa tu Usuario">
			</div>

			<div class="form-group">
				<input type="password" name="password" class="form-control" placeholder="Ingresa tu Contraseña">
			</div>

			<div class="c-login__remember">
				<label >
					{!! Form::checkbox('remember') !!} Recordárme
				</label>
			</div>

			<div class="text-center">
				<button type="submit" class="btn btn-primary">Iniciar Sesión</button>
			</div>
		{!! Form::close() !!}

		<footer class="c-login__footer">Copyright &copy {{date('Y')}} {{App\Company::first()->name_company}}. Panel Admin v2. Desarrollado por Noveltie</footer>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</body>
</html>
