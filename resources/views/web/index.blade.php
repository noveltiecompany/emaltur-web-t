<!DOCTYPE html>
<html lang='es'>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"/>

    <title>{{ $head->nameCompany }}</title>

    {{-- Templates --}}
    @include('web/templates/meta-socials')
    @include('web/templates/fonts')
    @include('web/templates/styles')

    <script src="https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit" async defer></script>
  </head>

  <body>
    <div id="root"></div>

    <script>
      window.PATH = '{{ url('') }}';
      window.API_URL = '{{ url('').'/api/v1' }}';
    </script>

    {{-- Templates --}}
    @include('web/templates/scripts')
  </body>
</html>
