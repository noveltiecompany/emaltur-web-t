{{-------------------------------
  Styles
--------------------------------}}

@if(env('APP_ENV') !== 'local')
  <link rel="stylesheet" href="{{ asset('static/css/app.css') }}"/>
@endif
