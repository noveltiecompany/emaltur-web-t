{{-- ------------------------------------
  #Base Fonts
------------------------------------ --}}

{{-- Roboto Slab --}}
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet">



{{-- ------------------------------------
  #Icon Fonts
------------------------------------ --}}

<link href="/icon-font/style.css" rel="stylesheet" />
