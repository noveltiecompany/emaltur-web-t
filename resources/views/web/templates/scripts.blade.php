{{-------------------------------
  WOW.JS
--------------------------------}}

	<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
	<script>
    new WOW().init();
  </script>


{{-------------------------------
  WEBPACK
--------------------------------}}

@if(env('APP_ENV') === 'local')
	<script src="http://192.168.1.118:8080/app.js"></script>
@else
	<script src="{{ asset('static/js/manifest.js') }}"></script>
	<script src="{{ asset('static/js/vendor.js') }}"></script>
	<script src="{{ asset('static/js/app.js') }}"></script>
@endif

{{-------------------------------
	Global site tag (gtag.js) - Google Analytics
--------------------------------}}

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110522816-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag () { dataLayer.push(arguments); }
		gtag('js', new Date());
		gtag('config', 'UA-110522816-1');
	</script>


{{-------------------------------
	Disqus
--------------------------------}}
{{-- <script>
	/**
	*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
	*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/

	var disqus_config = function () {
	this.page.url = 'http://emaltur.org/';
	this.page.identifier = PAGE_IDENTIFIER;
	};
	// Replace PAGE_IDENTIFIER with your page's unique identifier variable

	(function() { // DON'T EDIT BELOW THIS LINE
	var d = document, s = d.createElement('script');
	s.src = 'https://emaltur.disqus.com/embed.js';
	s.setAttribute('data-timestamp', +new Date());
	(d.head || d.body).appendChild(s);
	})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript> --}}