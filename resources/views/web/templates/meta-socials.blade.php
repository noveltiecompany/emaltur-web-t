{{-- ------------------------------------
  #Meta Social Tags
------------------------------------ --}}

<meta name="description" content="{{ $head->description }}"/>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="{{ '@'.$head->pageTwitter }}">

{{-- <meta property="fb:app_id" content="app_id" /> App Id--}}
<meta property="og:locale" content="es_ES" />
<meta property="og:type" content="article" />
<meta property="og:description" content="{{ $head->description }}" />
<meta property="og:image" content="{{ $head->imageUrl }}" />
<meta property="og:title" content="{{ $head->title }}" />
<meta property="og:site_name" content="{{ $head->url }}" />
<meta property="og:url" content="{{ $head->url }}" />
