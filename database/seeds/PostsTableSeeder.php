<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('tags')->delete();
  		DB::table('tags')->insert([
  			'name' => 'Noticias',
  			'slug' =>'noticias',
  		]);

  		DB::table('tags')->insert([
  			'name' => 'Eventos',
  			'slug' =>'eventos',
  		]);

      DB::table('tags')->insert([
  			'name' => 'Comercio',
  			'slug' =>'comercio',
  		]);

      DB::table('tags')->insert([
  			'name' => 'Turismo',
  			'slug' =>'turismo',
  		]);
    }
}
