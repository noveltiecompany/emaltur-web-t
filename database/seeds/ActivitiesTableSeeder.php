<?php

use Illuminate\Database\Seeder;

class ActivitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('activities')->delete();
  		DB::table('activities')->insert([
  			'name' => 'Comercio',
        'description' => 'Comercio',
  			'slug' =>'comercio',
        'published'=>true,
  		]);

  		DB::table('activities')->insert([
  			'name' => 'Alojamientos',
        'description' => 'Alojamientos',
  			'slug' =>'alojamientos',
        'published'=>true,
  		]);

      DB::table('activities')->insert([
  			'name' => 'Restaurantes',
        'description' => 'Restaurantes',
  			'slug' =>'restaurantes',
        'published'=>true,
  		]);

      DB::table('activities')->insert([
  			'name' => 'Salud',
        'description' => 'Salud',
  			'slug' =>'salud',
        'published'=>true,
  		]);

      DB::table('activities')->insert([
  			'name' => 'Transporte',
        'description' => 'Transporte',
  			'slug' =>'transporte',
        'published'=>true,
  		]);

      DB::table('activities')->insert([
  			'name' => 'Entretenimiento',
        'description' => 'Entretenimiento',
  			'slug' =>'entretenimiento',
        'published'=>true,
  		]);

      DB::table('activities')->insert([
  			'name' => 'Industria',
        'description' => 'Industria',
  			'slug' =>'industria',
        'published'=>true,
  		]);

      DB::table('activities')->insert([
  			'name' => 'Servicios',
        'description' => 'Servicios',
  			'slug' =>'servicios',
        'published'=>true,
  		]);

    }
}
