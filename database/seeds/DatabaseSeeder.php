<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Solo ejecutar la primera vez
        $this->call('PostsTableSeeder');
        $this->call('CompaniesTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('SealsTableSeeder');
        $this->call('ActivitiesTableSeeder');
    }
}
