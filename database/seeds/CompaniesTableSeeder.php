<?php

use Illuminate\Database\Seeder;
use App\Company;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('companies')->delete();

		Company::create([
				'name_company' => 'Empresa',
				'title_slogan' => '',
				'subtitle_slogan' => '',
				'representative' =>'Empresario Digital',
				'slug_company' => 'noveltie',
				'logotype' =>'',
				'logotype_path' =>'',
				'logotype_thumb' =>'',
				'logotype_thumb_path' =>'',
				'description_company' => '',
				'email' => 'servicios@noveltie.la',
				'address' => 'Tacna',
				'phone' => '052 ',
				'cel' => '+51 952949785',
				'facebook' =>'https://www.facebook.com/noveltiePeru',
				'twitter' =>'',
				'meta_keywords' =>'',
				'ga_code' => '',
				'city' => 'Tacna',
				'country' =>'Perú',
        'type' =>1
		]);
    }
}
