<?php

use Illuminate\Database\Seeder;

class SealsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('seals')->delete();
  		DB::table('seals')->insert([
  			'name' => 'Emprendedor',
        'description' => 'Emprendedor',
        'published' => true,
  		]);

  		DB::table('seals')->insert([
  			'name' => 'RSE',
        'description' => 'RSE',
        'published' => true,
  		]);

      DB::table('seals')->insert([
  			'name' => 'Innovador',
        'description' => 'Innovador',
        'published' => true,
  		]);

    }
}
