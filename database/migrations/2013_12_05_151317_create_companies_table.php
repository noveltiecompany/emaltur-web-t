<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::create('companies', function (Blueprint $table) {
				$table->increments('id');
				$table->string('name_company');
				$table->string('title_slogan')->nullable();
				$table->string('subtitle_slogan')->nullable();
				$table->string('representative');
				$table->string('slug_company');
				$table->string('logotype')->nullable();
				$table->string('logotype_path')->nullable();
				$table->string('logotype_thumb')->nullable();
				$table->string('logotype_thumb_path')->nullable();
				$table->string('logotype_white')->nullable();
				$table->string('logotype_white_path')->nullable();
				$table->string('logotype_white_thumb')->nullable();
				$table->string('logotype_white_thumb_path')->nullable();
				$table->text('description_company');
				$table->string('email')->nullable();
				$table->string('address')->nullable();
				$table->string('website')->nullable();
				$table->string('phone')->nullable();
				$table->string('cel')->nullable();
				$table->string('facebook')->nullable();
			 	$table->string('twitter')->nullable();
				$table->string('google_plus')->nullable();
				$table->string('vision')->nullable();
				$table->string('meta_keywords')->nullable();
				$table->string('ga_code')->nullable();
                $table->string('video_cover')->nullable();
				$table->string('city')->nullable();
				$table->string('country')->nullable();
		        $table->integer('activity_id')->unsigned()->nullable();
		        $table->integer('type')->default(2)->nullable(); //1= Emaltur, 2= Asociado
                $table->text('our_history')->nullable();
                $table->text('our_objectives')->nullable();
                $table->text('value_proposal')->nullable();
                $table->text('directors')->nullable();
                $table->text('benefits')->nullable();
                $table->text('requirements')->nullable();
                $table->text('schedules')->nullable();
		        $table->string('longitude')->default('0');
		        $table->string('latitude')->default('0');
		        $table->boolean('published')->default(1);
				$table->timestamps();
				$table->softDeletes();
		});
	}


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('companies');
    }
}
