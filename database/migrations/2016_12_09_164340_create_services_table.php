<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services',function(Blueprint $table){
    			$table->increments('id');
    			$table->string('image')->nullable();
    			$table->string('image_path')->nullable();
    			$table->string('image_thumb')->nullable();
    			$table->string('image_thumb_path')->nullable();
    			$table->boolean('published');
          $table->string('name');
          $table->string('slug');
          $table->string('title')->nullable();
          $table->decimal('price', 20, 2)->default(0)->nullable();
          $table->text('steps')->nullable();
          $table->integer('product_id')->unsigned();
          $table->integer('format'); // Format 1 and format 2
          $table->string('price_concept')->nullable();
          $table->text('description')->nullable();
    			$table->timestamps();
    			$table->softDeletes();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('services');
    }
}
