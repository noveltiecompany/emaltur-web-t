<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSocialNetworksToCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::table('companies',function(Blueprint $table){
            $table->string('youtube')->after('twitter')->nullable();
            $table->string('instagram')->after('youtube')->nullable();
            $table->string('pinterest')->after('instagram')->nullable();
            $table->string('linkedin')->after('pinterest')->nullable();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('youtube');
            $table->dropColumn('instagram');
            $table->dropColumn('pinterest');
            $table->dropColumn('linkedin');
        });
    }
}
