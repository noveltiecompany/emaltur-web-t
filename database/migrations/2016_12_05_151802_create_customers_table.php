<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('customers',function(Blueprint $table){
			$table->increments('id');
      $table->string('ruc')->nullable(); //empresa
      $table->string('legal_name')->nullable(); //empresa
			$table->string('first_name')->nullable(); //persona
			$table->string('last_name')->nullable(); //persona
			$table->string('email')->unique();
			$table->string('cel_whatsapp')->nullable();
			$table->string('facebook')->nullable();
			$table->integer('customer_type'); //1 = persona, 2 = empresa
			$table->string('city')->nullable();
			$table->string('country')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('customers');
    }
}
