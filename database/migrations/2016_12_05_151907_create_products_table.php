<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('products',function(Blueprint $table){
			$table->increments('id');
			$table->string('name');
			$table->string('slug');
			$table->string('title');
			$table->text('description');
			$table->string('image')->nullable();
			$table->string('image_path')->nullable();
			$table->string('image_thumb')->nullable();
			$table->string('image_thumb_path')->nullable();
      $table->boolean('featured_product')->default(0); //true=Producto destacado que se visualiza en Home
			$table->boolean('published');
			$table->timestamps();
			$table->softDeletes();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('products');
    }
}
