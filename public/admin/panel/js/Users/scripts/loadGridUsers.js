function loadGridUsers(users)
{
	var _content= '';
	var _elementActivated = '';
	var _userImage = '';

	if (users) {
		$.each(users,function(k,user){

			_userImage = 'https://image.freepik.com/free-icon/male-user-shadow_318-34042.jpg';
			if (user.user_image != null && user.user_image != '') {
				_userImage = user.user_image;
			}

			if (user.activated == true) {
				_elementActivated = '<button type="button" style="width: 50%;" data-index="'+user.id+'" class="btn btn-danger user_suspend" title="Suspender usuario">'+
								 		'<i class="glyphicon glyphicon-ban-circle"></i>'+
							 		'</button>';
			} else {
				_elementActivated = '<button type="button" style="width: 50%;" data-index="'+user.id+'" class="btn btn-success user_activate" title="Activar usuario">'+
								 		'<i class="glyphicon glyphicon-ok-circle"></i>'+
							 		'</button>';
			}
			
			_content = 
			_content +  '<div class="col-lg-3 col-md-4 col-sm-6 phs" style="width: 15%;">'+
				          '<li class="item-account">'+
				            '<figure class="item-image">'+
				              '<img src="'+_userImage+'" alt="" />'+
							'</figure>'+
							'<span style="display: block;text-align: center;">'+user.first_name+' '+user.last_name+'</span>'+
				            '<div class="item__controls">'+
							 '<button type="button" style="width: 50%;" data-index="'+user.id+'" class="btn btn-success user_edit"  data-target="#add-users" data-toggle="modal" title="Editar">'+
								 '<i class="glyphicon glyphicon-pencil"></i>'+
							 '</button>'+
							  _elementActivated+
							 '<button type="button" style="width: 50%;" data-index="'+user.id+'" class="btn btn-success user_change-password"  data-target="#miModalpassword" data-toggle="modal" title="Cambiar la contraseña">'+
								 '<i class="glyphicon glyphicon-ruble"></i>'+
							 '</button>'+
				            '</div>'+
				          '</li>'+
				        '</div>';
		});
		$('#users_grid').empty();
		$('#users_grid').append(_content);
	}
}