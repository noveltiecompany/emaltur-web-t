$(document).on('ready',function(){
	$.get('customers',function(data){
		loadGridCustomers(data.customers);
	});
});

$(document).on('click','.customer_delete', function(){
	deleteCustomer($(this).data('index'));
});

$(document).on('click','.customer_edit', function(){
	cleanError();
	cleanModalCustomer();
	addInputPut($('#form_customers'), 'customer_method');
	$('#customer_update').show();
	getCustomerToEdit($(this).data('index'));
});

$('#customer_save').on('click', function(){
	saveCustomer();
});

$('#customer_update').on('click', function()
{
	updateCustomer();
});

$('#customer_add').on('click', function(){
	cleanError();
	cleanModalCustomer();
	$('#customer_save').show();
	
	$('.customer_type-1').show();
	$('.customer_type-2').hide();

})

$('#customer_type').on('change', function(){
	if ($(this).val() == 1) {
		$('.customer_type-1').show();
		$('.customer_type-2').hide();
	} else if($(this).val() == 2){
		$('.customer_type-1').hide();
		$('.customer_type-2').show();
	}
});


function cleanModalCustomer()
{	
	$('#customer_method').remove();	
	$('#customer_save').hide();
	$('#customer_update').hide();
	
	$('#customer_id').val('');
	$('#customer_firstname').val('');	
	$('#customer_lastname').val('');	
	$('#customer_email').val('');	
	$('#customer_cel').val('');	
	$('#customer_facebook').val('');
	$('#customer_city').val('');
	$('#customer_country').val('');	
}


