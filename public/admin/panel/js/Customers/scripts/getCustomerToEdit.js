function getCustomerToEdit(customerId)
{
	$.get('customers/'+customerId, function(data)
	{
		$('#customer_id').val(data.id);
		$('#customer_firstname').val(data.first_name);
		$('#customer_lastname').val(data.last_name);
		$('#customer_email').val(data.email);
		$('#customer_cel').val(data.cel_whatsapp);
		$('#customer_facebook').val(data.facebook);
		$('#customer_city').val(data.city);
		$('#customer_country').val(data.country);
	});
}