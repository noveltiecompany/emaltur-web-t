function saveCustomer()
{
	cleanError();
	saveOrUpdateFormData('form_customers','customers',successSaveCustomer,errorSaveCustomer);
}


function successSaveCustomer(data)
{
	if (data.success == true) {
		$.growl.notice({ message: "Se ha creado al cliente" });
			
		$.get('customers',function(data){
			loadGridCustomers(data.customers);
		});

		$('#add-customers').modal('hide');
	}
	else if(data.success == false) {
		$.growl.error({ message: "Ha ocurrido un error" });
	}
}

function errorSaveCustomer(jqXHR, textStatus, errorThrown)
{
	$('#customer_error').append("Corrija los siguientes campos por favor!");
	$.each(jqXHR.responseJSON, function( key, value ) {
		if (key == "first_name") {
			$.each(value, function( errores, eror ) {
				$('#customer-error-firstname').append("<li class='error-block'>"+eror+"</li>");
			});
		}
		else if (key == "last_name") {
			$.each(value, function( errores, eror ) {
				$('#customer-error-lastname').append("<li class='error-block'>"+eror+"</li>");
			});
		}
		else if (key == "email") {
			$.each(value, function( errores, eror ) {
				$('#customer-error-email').append("<li class='error-block'>"+eror+"</li>");
			});
		};
	});
}