function deleteCustomer(customerId)
{
	swal({   title: 'Borrar el cliente',
	  text: '¿Está usted seguro?',
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: "#DD6B55",
	  confirmButtonText: 'Aceptar',
	  cancelButtonText:'Cancelar',
	  closeOnConfirm: true },
	  function(){
		 var ruta = "customers/"+customerId;
		 lockWindow();
		$.ajaxSetup({
		  headers: {
			  'X-CSRF-TOKEN': $('input[name=_token]').val()
		  }
		});
		$.ajax({
			url: ruta,
			data: {},
			type: 'DELETE',
			success: function(result) {
				unlockWindow();
				successDeleteCustomer(result)
			}
		});
	});
}

function successDeleteCustomer(data)
{
	if (data.success == true) {
		$.growl.notice({ message: "Se ha borrado al cliente" });
		$.get('customers',function(data){
			loadGridCustomers(data.customers);
		});

	}
	else if(data.success = false){
		$.growl.error({ message: "Ha ocurrido un error al eliminar al cliente" });
	}
}
