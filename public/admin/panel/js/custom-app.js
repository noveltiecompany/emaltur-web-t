
var msgError = "Corrija los siguientes campos por favor!";
var msgFailedAction = "Ha ocurrido un error. Inténtelo de nuevo."

var msgSavePost = "La noticia se ha guardado correctamente.";
var msgUpdatePost = "La noticia se ha actualizado correctamente.";
var msgPublishedChangePost = "Se ha cambiado el estado de la noticia";
var msgPublishedChangePostError = "Ha ocurrido un error al intentar cambiar el estado de la noticia"
var msgDeletePost = "Se ha borrado la noticia correctamente.";
var msgDeletePostError = "Ha ocurrido un error. Inténtelo de nuevo.";


var msgImageUploaded = "La imágen se ha cargado correctamente.";
var msgImageDeleted = "La imágen ha sido borrada completamente.";


//Variables Globales
var statusCloseModalSubcategory = 0;
var statusCloseModalPost = false;
var statusCloseModalProduct = false;

var postDropzoneInstance = "";
var postDropzoneInstanceFile = "";

var statusCarouselPost = false;

var statusCarouselProduct = false;

$('.mensaje-error').addClass('text-error');
$('.titulo-error').addClass('text-error');

function editor2(selector){
    $("#"+selector).wysihtml5({
		"locale":"es-ES",
        toolbar: {
            "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
            "emphasis": true, //Italics, bold, etc. Default true
            "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
            "html": false, //Button which allows you to edit the generated HTML. Default false
            "link": false, //Button to insert a link. Default true
            "image": false, //Button to insert an image. Default true,
            "color": false, //Button to change color of font
            "blockquote": false, //Blockquote
            "size": "xs" //default: none, other options are xs, sm, lg
        }
    });
}

function editorTitle(selector){
    $("#"+selector).wysihtml5({
		"locale":"es-ES",
      toolbar: {
        "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
        "emphasis": false, //Italics, bold, etc. Default true
        "lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
        "html": false, //Button which allows you to edit the generated HTML. Default false
        "link": false, //Button to insert a link. Default true
        "image": false, //Button to insert an image. Default true,
        "color": false, //Button to change color of font
        "blockquote": false, //Blockquote
        "size": "xs", //default: none, other options are xs, sm, lg
      }
    });
}

function addSummerNoteEditor(element)
{
    element.summernote({
        lang: 'es-ES',
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['link', 'table']],
        ]
    });
}

function cleanError(){
	$('.mensaje-error').empty();
	$('.titulo-error').empty();
}

function cleanEditor(element)
{
	element.next().next().remove();
	element.prev().remove();
	element.css('display', 'block');
}

function cleanDropzone(identificador){
	var myDropzone = Dropzone.forElement("#"+identificador);
	var files = myDropzone.files;
	myDropzone.removeAllFiles(true)
}

function beautifulAlert(title,text,type,confirmText,ruta,idForm,successFunction,errorFunction){
	swal({   title: title,
	  text: text,
	  type: type,
	  showCancelButton: true,
	  confirmButtonColor: "#DD6B55",
	  confirmButtonText: confirmText,
	  closeOnConfirm: true },
	  function(){
		  var formData = new FormData($('#'+idForm)[0]);
		  ajaxall_POST_formData(ruta,formData,successFunction,errorFunction);
	  });
}

function saveOrUpdateFormData(formId,ruta,successFunction,errorFunction){
	cleanError();
	var formData = new FormData($('#'+formId)[0]);
	ajaxall_POST_formData(ruta,formData,successFunction,errorFunction);
}

$('#user_editar_perfil').on('click',function(){
	$('#modalProfile').modal('show');
});

$('#user_editar_password').on('click',function(){
	$('#password_user-id').val('');
	$('#form_password')[0].reset();
	$('#miModalpassword').modal('show');
	$('#password_user-id').val($('#user_editar_perfil').data('index'));
});

$('#change_password').click(function(event){
    event.preventDefault();
	$('#error-password').empty();
	$('#error-password_confirmation').empty();
	saveOrUpdateFormData('form_password','users/password',successChangePassword,errorChangePassword);
});

function successChangePassword(data){
	if (data.success == true) {
		window.location.replace("../plataforma");
	}
	else if(data.success == false){
		$.growl.error({ message: "Ha ocurrido un error actualizando la contraseña. Inténtelo de nuevo" })
	}
}

function errorChangePassword(jqXHR, textStatus, errorThrown){
	$('#password_error').append(msgError);
	$.each(jqXHR.responseJSON, function( key, value ) {
		if (key == "password") {
			$.each(value, function( errores, eror ) {
				$('#error-password').append("<li class='error-block'>"+eror+"</li>");
			});
		}
		else if (key == "password_confirmation") {
			$.each(value, function( errores, eror ) {
				$('#error-password_confirmation').append("<li class='error-block'>"+eror+"</li>");
			});
		}
	});
}

function lockWindow()
{
	$.blockUI(
		{
			message: "<h1>Espere por favor...</h1>",
			css: {
			   border: 'none',
			   padding: '15px',
			   backgroundColor: '#000',
			   opacity: .5,
			   color: '#fff',
			   display: 'flex',
			   top: 0,
			   bottom: 0,
			   left: 0,
			   right: 0,
			   'z-index': 1051,
			   width :'100%',
			   'justify-content': 'center',
			   'align-items': 'center',
	   } });
}

function unlockWindow()
{
	$.unblockUI();
}

function getSimbolPublished(publishedVal)
{
	var data = {};
	if (publishedVal == 0) {
		data.simbol = 'glyphicon glyphicon-eye-open';
		data.name = 'Publicar';
		return data;
	}

	if (publishedVal == 1) {
		data.simbol = 'glyphicon glyphicon-eye-close';
		data.name = 'Dejar de publicar';
		return data;
	}
}

function charge_slider(area,data,container){
  area.empty();
	if (data == null) {
	 }
	 else {
	      area.append('<div id="'+container+'" class="owl-carousel"></div>');
	        $.each(data, function(id, content){
	                // $('#'+config).append('<div class="item"><img class="lazyOwl" data-src="'+content.resource+'" alt="" height="100"><div align="center"><button class="btn btn-default recorta_slide" id="'+content.id+'" value="'+content.resource+'">Recortar</button><button class="btn btn-default elimina_slide" value="'+content.id+'">Eliminar</button></div></div>');
					$('#'+container).append('<div class="item"><img class="lazyOwl" data-src="'+content.resource+'" alt="" height="100" style="display: block;margin: 0 auto 2rem;"><div align="center"><button class="btn btn-default elimina_slide" value="'+content.id+'">Eliminar</button></div></div>');
		    });
	      $("#"+container).owlCarousel({
	        items : 4,
	        lazyLoad : true,
	        navigation : true
	      });
	 }
}

function addImageToSlide(swiper,swiperContainer,images)
{
	var number = swiperContainer.attr('data-number');
	if (number == "") {
		number = -1;
	}
	else {
		number = parseInt(number);
	}
	$.each(images,function(i,image) {
		number = number + 1;
		swiper.appendSlide('<div class="swiper-slide" data-index="'+number+'" style="display:flex;flex-direction:column"><img src="'+image.resource_thumb+'" alt="" /><button class="image-slide-button__delete form-control" data-image_id="'+image.id+'" style="margin-top: 10px;max-width:8rem;">Eliminar</button></div>');
	});
		swiperContainer.attr('data-number',number);
}

function addImageToSliderService(swiper,swiperContainer,images)
{
	var number = swiperContainer.attr('data-number');
	if (number == "") {
		number = -1;
	}
	else {
		number = parseInt(number);
	}
	$.each(images,function(i,image) {
		number = number + 1;
		swiper.appendSlide('<div class="swiper-slide" data-index="'+number+'" style="display:flex;flex-direction:column"><img src="'+image.resource_thumb+'" alt="" /><button class="btn_image-slider-service__delete form-control" data-image_id="'+image.id+'" style="margin-top: 10px;max-width:8rem;">Eliminar</button></div>');
	});
		swiperContainer.attr('data-number',number);
}

function deleteSlide(index,imageId,swiper,swiperContainer)
{
	swal({   title: "Borrar Imagen",
	  text: "¿Estás seguro?",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonColor: "#DD6B55",
	  confirmButtonText: "Si,borrar",
	  closeOnConfirm: true },
	  function(){
		  var ruta = "contents";
		  lockWindow();
		  $.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('input[name=_token]').val()
			}
		  });
		  $.ajax({
			  url: ruta,
			  data: {'contentId':imageId},
			  type: 'DELETE',
			  success: function(result) {
				  unlockWindow();
				  if (result.success == true) {
					  swiper.removeSlide(index);
					  var number = swiperContainer.attr('data-number');
					  swiperContainer.attr('data-number',parseInt(number)-1);
					  $.growl.notice({ message: msgImageDeleted });
				  }
				  if (result.success == false){
					  $.growl.error({ message: msgFailedAction });

				  }
			  }
		  });
	  });
}

function lockElement(element)
{
	element.attr("disabled",true);
}

function unlockElement(element)
{
	element.attr("disabled",false);
}

function addInputPut(form, id)
{
	form.append('<input type="hidden" name="_method" id="'+id+'" value="PUT" />');
}
