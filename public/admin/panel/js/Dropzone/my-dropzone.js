Dropzone.options.myDropzone = {
         autoProcessQueue: false,
         uploadMultiple: true,
         maxFilezise: 10,
		 parallelUploads: 1,
         maxFiles: 5,

         init: function() {
             myDropzone = this;

             this.on("addedfile", function(file) {
				 var _that  = this;
 				 setTimeout(function () {
 					 var _imageWidth = 0, _imageHeight = 0;

 					 _imageWidth = file.width;
 					 _imageHeight = file.height;

 					 if (_imageWidth >= 1350) {
						 _that.processQueue();
 					 }else {
						 swal('La imagen no posee las dimensiones indicadas 1350x350.')
 						 _that.removeFile(file);
 					 }
 				 }, 700);
	         });

             this.on("complete", function(file) {
				unlockWindow();
				if (file.status == "canceled") {

				}else {
					myobjeto = JSON.parse(file.xhr.response);
					$('#company_carousel').trigger('add.owl.carousel',['<li class="item item__photo"><img src="'+myobjeto.resource_thumb+'" width="100%"/><div class="item__controls"><button type="button" class="btn btn-primary company_delete_cover" data-index="'+myobjeto.id+'" title="Eliminar"><i class="glyphicon glyphicon-trash"></i></button> </div></li>',0]).trigger('refresh.owl.carousel');
				}
				myDropzone.removeFile(file);
             });

			this.on("uploadprogress", function(file) {
				lockWindow();
			});

			// this.on("totaluploadprogress",function(file,progress,bytesSend){
			// 	console.log(progress);
			// });

             this.on("success",
                 myDropzone.processQueue.bind(myDropzone)
             );
         }
     };

	 var productDropzone = $('#product-dropzone').dropzone({
		 dictDefaultMessage: "Arrastre sus imágenes aquí!.",
		 autoProcessQueue: true,
		 parallelUploads: 1,
		 uploadMultiple: true,
		 maxFilezise: 10,
		 maxFiles: 5,

		 init: function() {
			 productDropzone = this;

			 this.on("addedfile", function(file) {

			 });

			 this.on("complete", function(file) {
				   unlockElement($('#product_close'));
				   this.removeFile(file);
			 });

			 this.on("uploadprogress", function(progress) {
				 lockElement($('#product_close'));
		   });

		   this.on('canceled',function(file){
		   });

		   this.on('error',function(file){
		   });

		   this.on("success",function(file){
		   		var _that  = "", _myobjeto = {};
				_that  = this;
				_myobjeto = JSON.parse(file.xhr.response);

			   $.ajaxSetup({
				   headers: {
					   'X-CSRF-TOKEN': $('input[name=_token]').val()
				   }
			   });
			   $.post('contents/change-model-id',{'modelId':$('#product_id').val(),'contentId':_myobjeto.id},function(data){
				   if (data.success == true) {
					   $.growl.notice({ message: msgImageUploaded });
					   addImageToSlide(swiperProduct,$('#product-swiper-container'),data.images);
					   $('#product-swiper-container').show();
				   }
				   if (data.success == false) {
					   $.growl.error({ message: msgFailedAction })
				   }
			   });
			   _that.removeFile(file);
		   });
		 }
	 });

	 var postDropzone = $('#post-dropzone').dropzone({
		 dictDefaultMessage: "Arrastre sus imágenes aquí!.",
		 autoProcessQueue: true,
		 uploadMultiple: true,
		 parallelUploads: 1,
		 maxFilezise: 10,
		 maxFiles: 5,

		 init: function() {
			 postDropzone = this;

			 this.on("addedfile", function(file) {
			 });

			 this.on("complete", function(file) {
				 unlockElement($('#post-button__close'));
				 this.removeFile(file);
			 });

			 this.on('canceled',function(file){
			 });

			 this.on('sending',function(file){

			 });

			 this.on('error',function(file){
			 });

			 this.on("uploadprogress", function(progress) {
				 lockElement($('#post-button__close'));
		   	});

			this.on("success",function(file){
				var _that = "", _myobjeto = {};

				_that = this;
				_myobjeto = JSON.parse(file.xhr.response);

				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('input[name=_token]').val()
					}
				});
				$.post('contents/change-model-id',{'modelId':$('#post-id').val(),'contentId':_myobjeto.id},function(data){
					if (data.success == true) {
						$.growl.notice({ message: msgImageUploaded });
						addImageToSlide(swiperPost,$('#post-swiper-container'),data.images);
						$('#post-swiper-container').show();
					}
					if (data.success == false) {
						$.growl.error({ message: msgFailedAction })
					}
				});
				_that.removeFile(file);
			});
		 }
	 });

	 var serviceDropzone = $('#service-dropzone').dropzone({
		 dictDefaultMessage: "Arrastre sus imágenes aquí!.",
		 autoProcessQueue: true,
		 uploadMultiple: true,
		 parallelUploads: 1,
		 maxFilezise: 10,
		 maxFiles: 5,

		 init: function() {
			 serviceDropzone = this;

			 this.on("addedfile", function(file) {
			 });

			 this.on("complete", function(file) {
				 this.removeFile(file);
			 });

			 this.on('canceled',function(file){
			 });

			 this.on('sending',function(file){

			 });

			 this.on('error',function(file){
			 });

			 this.on("uploadprogress", function(progress) {
		   	});

			this.on("success",function(file){
				var _that = "", _myobjeto = {};

				_that = this;
				_myobjeto = JSON.parse(file.xhr.response);

				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('input[name=_token]').val()
					}
				});
				$.post('contents/change-model-id',{'modelId':$('#service-images_service-id').val(),'contentId':_myobjeto.id},function(data){
					if (data.success == true) {
						$.growl.notice({ message: msgImageUploaded });
						addImageToSliderService(swiperService,$('#service-swiper-container'),data.images);
					}
					if (data.success == false) {
						$.growl.error({ message: msgFailedAction })
					}
				});
				_that.removeFile(file);
			});
		 }
	 });

	 var associatedDropzone = $('#associated_dropzone').dropzone({
		 dictDefaultMessage: "Arrastre sus imágenes aquí!.",
		 autoProcessQueue: true,
		 uploadMultiple: true,
		 parallelUploads: 1,
		 maxFilezise: 10,
		 maxFiles: 5,

		 init: function() {
			associatedDropzone = this;

			this.on("addedfile", function(file) {
			});

			this.on("complete", function(file) {
			this.removeFile(file);
			});

			this.on('canceled',function(file){
			});

			this.on('sending',function(file){

			});

			this.on('error',function(file){
			});

			this.on("uploadprogress", function(progress) {
			});

			this.on("success",function(file){
				var _that = "", _myobjeto = {};

				_that = this;
				_myobjeto = JSON.parse(file.xhr.response);

				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('input[name=_token]').val()
					}
				});
				$.post('contents/change-model-id',{'modelId':$('#associated-image_id').val(),'contentId':_myobjeto.id},function(data){
					if (data.success == true) {
						$.growl.notice({ message: msgImageUploaded });
						addImageToSliderAssociated(swiperAssociated,$('#associated-swiper-container'),data.images);
					}
					if (data.success == false) {
						$.growl.error({ message: msgFailedAction })
					}
				});
				_that.removeFile(file);
			});
		 }
	 });

var companyGalleryDropzone = $('#company-gallery_dropzone').dropzone({
	dictDefaultMessage: "Arrastre sus imágenes aquí!.",
	autoProcessQueue: true,
	parallelUploads: 1,
	uploadMultiple: true,
	maxFilezise: 10,
	maxFiles: 5,

	init: function() {
		companyGalleryDropzone = this;

		this.on("addedfile", function() {
		});

		this.on("complete", function(file) {
		this.removeFile(file);
		});

		this.on("uploadprogress", function(progress) {
		});

		this.on('canceled',function(file){
		});

		this.on('error',function(file){
		$.growl.error({ message: msgFailedAction })
		});

		this.on("success",function(file){
		let _that = "", _myobjeto = {};

		_that = this;
		console.log(file.xhr);
		_myobjeto = JSON.parse(file.xhr.response);

		addImageToSliderCompanyGallery(swiperCompanyGallery,$('#company-gallery_swiper-container'), _myobjeto);

		_that.removeFile(file);
		});
	}
});