var swiperCompanyGallery;


$(document).on('ready',function(){
	//Cargar automaticamente la empresa
	loadCompany();
});

$('#company-info-save').on('click',function(){
	saveCompanyInfo();
});

$('#company-slogan-save').on('click',function(){
	saveCompanySlogan();
});

$('#company_update').on('click',function(){
	updateCompany();
});

$('#company_add_video').on('click',function(){
	cleanModalCompanyVideo('add');
	cleanError();
});

$('#company_save_video').on('click',function(){
	saveCompanyVideo();
});

$(document).on('click','.edit_video',function(){
	cleanModalCompanyVideo('edit');
	getVideoToEdit($(this).data('index'));
});

$('#company_update_video').on('click',function(){
	updateCompanyVideo();
});

$(document).on('click','.delete_video',function(){
	cleanModalCompanyVideo('add');
	deleteVideo($(this).data('index'));
});

$(document).on('click','.company_delete_cover',function(){
		itemCover = $(this).parent().parent().parent();
		cleanModalCompanyVideo('add');
		deleteCover($(this).data('index'));
});

function cleanModalCompanyVideo(operation){
	$('#company_video_content').val('');
	$('#company_video_resource').val('');
	$('#company_video_id').val('');

	if (operation == 'add') {
		$('#company_save_video').show();
		$('#company_update_video').hide();
		$('#company_video_method').remove();
	}
	else if(operation == 'edit'){
		$('#company_save_video').hide();
		$('#company_update_video').show();
		$('#company_video_method').remove();
		$('#form_company_video').append('<input type="hidden" id="company_video_method" name="_method" value="PUT" />');
	}
}

function startCarouselCompanyGallery()
{
		swiperCompanyGallery = new Swiper('#company-gallery_swiper-container', {
			loop: false,
			pagination: '#company-gallery_swiper-pagination',
			nextButton: '#company-gallery_swiper-button-next',
			prevButton: '#company-gallery_swiper-button-prev',
			slidesPerView: 3,
			//centeredSlides: true,
			//paginationClickable: true,
			spaceBetween: 30,
		});
}

function addImageToSliderCompanyGallery(swiper,swiperContainer,images)
{
	var number = swiperContainer.attr('data-number');
	if (number == "") {
		number = -1;
	}
	else {
		number = parseInt(number);
	}
	$.each(images,function(i,image) {
		number = number + 1;
		swiper.appendSlide('<div class="swiper-slide" data-index="'+number+'" style="display:flex;flex-direction:column"><img src="'+image.resource_thumb+'" alt="" /><button class="image-slider-company-gallery__delete form-control" data-image_id="'+image.id+'" style="margin-top: 10px;max-width:8rem;">Eliminar</button></div>');
	});
	swiperContainer.attr('data-number',number);
}


$(document).on('click', '.image-slider-company-gallery__delete', function(){
	deleteSlide($(this).parent().data('index'),$(this).data('image_id'),swiperCompanyGallery,$('#company-gallery_swiper-container'));
});
