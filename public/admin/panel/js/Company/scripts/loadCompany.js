function loadCompany(){
	var ruta = 'companies/first';
	ajaxAll_GET(ruta,successLoadCompany)
}

function successLoadCompany(data){
	if (data.company) {

		$('#company_id').val(data.company.id);
		$('#company_name').val(data.company.name_company);
		$('#company_description').val(data.company.description_company);
		addSummerNoteEditor($('#company_description'));

		$('#company_vision').val(data.company.vision);
		addSummerNoteEditor($('#company_vision'));

		$('#company_our-history').val(data.company.our_history);
		addSummerNoteEditor($('#company_our-history'));
		$('#company_our-objectives').val(data.company.our_objectives);
		addSummerNoteEditor($('#company_our-objectives'));

		$('#company_value-proposal').val(data.company.value_proposal);
		addSummerNoteEditor($('#company_value-proposal'));
		$('#company_directors').val(data.company.directors);
		addSummerNoteEditor($('#company_directors'));
		$('#company_benefits').val(data.company.benefits);
		$('#company_requirements').val(data.company.requirements);

		$('#company_proposal-value').val(data.company.proposal_value);
		//editor2('company_proposal-value');

		$('#company_cel').val(data.company.cel);
		$('#company_phone').val(data.company.phone);
		$('#company_email').val(data.company.email);
		$('#company_facebook').val(data.company.facebook);
		$('#company_twitter').val(data.company.twitter);
		$('#company_google-plus').val(data.company.google_plus);
		$('#company_youtube').val(data.company.youtube);
		$('#company_instagram').val(data.company.instagram);
		$('#company_pinterest').val(data.company.pinterest);
		$('#company_linkedin').val(data.company.linkedin);
		$('#company_address').val(data.company.address);
		$('#company_city').val(data.company.city);
		$('#company_country').val(data.company.country);
		$('#company_schedules').val(data.company.schedules);

		if (data.company.logotype) {
			$('#company-preview-image-text').hide();
			$('#preview_logotype').append('<img src="'+data.company.logotype+'" alt="Image" style="display: inline;" />')
		}

		if(data.company.logotype_white){
			$('#company-preview-image-white-text').hide();
			$('#preview_logotype_white').append('<img src="'+data.company.logotype_white+'" alt="Image" style="display: inline;" />')

		}

		$('#company_title_slogan').val(data.company.title_slogan);
		//editorTitle('company_title_slogan');
		$('#company_subtitle_slogan').val(data.company.subtitle_slogan);
		//editorTitle('company_subtitle_slogan');
		$('#company_video-cover').val(data.company.video_cover);

		loadCompanyVideos(data.videos);

		//Charge gallery images
		$('#company-gallery_swiper-container .swiper-wrapper').empty();

		setTimeout(function () {
			$.get('contents/0/7/1', function(r){
				startCarouselCompanyGallery();
				if (r.images.length) {
					addImageToSliderCompanyGallery(swiperCompanyGallery,$('#company-gallery_swiper-container'),r.images);
				}
			});
			}, 500);
	}
	else {
		alert('No existe una compañía registrada')
	}
}
