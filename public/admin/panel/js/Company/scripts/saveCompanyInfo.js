function saveCompanyInfo(){
		cleanError();
		var formData = new FormData($('#form_company')[0]);
		var ruta ="companies";
		ajaxall_POST_formData(ruta,formData,successSaveCompany,errorSaveCompany);
}

function successSaveCompany(data){
	if (data.success == false) {
		$.growl.error({ message: "Ha ocurrido un error. Inténtelo de nuevo!" })
	}
	else if(data.success == true){
		$.growl.notice({ message: "Se ha actualizado los datos de la empresa" })
	}
}

function errorSaveCompany(jqXHR, textStatus, errorThrown){
	console.log(jqXHR.responseJSON);
	$('#company-error').append(msgError);
	$.each(jqXHR.responseJSON, function( key, value ) {
		if (key == "name_company") {
			$.each(value, function( errores, eror ) {
				$('#company-error-name').append("<li class='error-block'>"+eror+"</li>");
			});
		}
		if (key == "email") {
			$.each(value, function( errores, eror ) {
				$('#company-error-email').append("<li class='error-block'>"+eror+"</li>");
			});
		}
		else if (key == "description_company") {
			$.each(value, function( errores, eror ) {
				$('#company-error-description').append("<li class='error-block'>"+eror+"</li>");
			});
		}
		else if (key == "city") {
			$.each(value, function( errores, eror ) {
				$('#company-error-city').append("<li class='error-block'>"+eror+"</li>");
			});
		}
		else if (key == "country") {
			$.each(value, function( errores, eror ) {
				$('#company-error-country').append("<li class='error-block'>"+eror+"</li>");
			});
		};
	});
}
