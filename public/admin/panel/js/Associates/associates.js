var swiperAssociated;

$(document).on('ready', function(){
    $.get('associates', function(associates){
        loadGridAssociates(associates);
    });

    $('#associated_seals').select2();
});

$('#associated__add').on('click', function(){
    cleanAssociatedModal();
    $('#associates_grid').hide();
    $('#associates_actions').show();
    $('#associated__update').hide();

    //Filling activities
    $.get('activities/published', function(activities){
        let _content;
        _content = '';

        $.each(activities, function(i, activity){
            _content += '<option value="'+activity.id+'">'+activity.name+'</option>'
        });
        $('#associated_activities').append(_content);
    });

    //Filling seals
    $.get('seals/published', function(seals){
        let _content;
        _content = '';

        $.each(seals, function(i, seal){
            _content += '<option value="'+seal.id+'">'+seal.name+'</option>'
        });
        $('#associated_seals').append(_content);
    });

    //Google maps
    $('#location').locationpicker({
     enableAutocomplete: true,
     enableReverseGeocode: true,
     radius: 0,
     inputBinding: {
         latitudeInput: $('#latitud'),
         longitudeInput: $('#longitud'),
         locationNameInput: $('#associated_geolocation')
     }
    });

});

$('#associated__save').on('click', function(){
    let _route, _formData;
    cleanError();

    _route  = 'associates';
    _formData = new FormData($('#form_associated')[0]);
    _formData.append('seals', $('#associated_seals').val());

    lockWindow();
    $.ajaxSetup({
        headers: {
            'csrftoken': $('input[name=_token]').val()
        }
    });
    $.ajax({
       url : _route,
       type: 'POST',
       data: _formData,
       contentType: false,
       processData: false,
       success: function(e){
            unlockWindow();
            $.growl.notice({ message: "Se ha guardado el asociado." });
            $('#associates_actions').hide();
            $('#associates_grid').show();

            $.get('associates', function(associates){
                loadGridAssociates(associates);
            });
     },
     error:function(jqXHR, textStatus, errorThrown)
     {
            unlockWindow();
            $.growl.error({ message: "Ha ocurrido un error." });
     }

     });
});

$(document).on('click', '.associated__edit', function(){
    let _associatedId = $(this).data('index');

    $.get('associates/'+_associatedId, function(associated){
        cleanAssociatedModal();
        $('#associated__save').hide();

        $('#associated_id').val(associated.id);
        $('#associated_name').val(associated.name_company);
        $('#associated_representative').val(associated.representative);
        $('#associated_email').val(associated.email);
        $('#associated_cel').val(associated.cel);
        $('#associated_phone').val(associated.phone);
        $('#associated_website').val(associated.website);
        $('#associated_facebook').val(associated.facebook);
        $('#associated_address').val(associated.address);
        $('#latitud').val(associated.latitude);
        $('#longitud').val(associated.longitude);

        //Map
        $('#location').locationpicker({
            enableAutocomplete: true,
            enableReverseGeocode: true,
            radius: 0,
            location: {
                latitude: $('#latitud').val(),
                longitude: $('#longitud').val()
            },
            inputBinding: {
                latitudeInput: $('#latitud'),
                longitudeInput: $('#longitud'),
                locationNameInput: $('#associated_geolocation')
            }
        });

        $.get('activities/published', function(activities){
            let _content;
            _content = '';

            $.each(activities, function(i, activity){
                _content += '<option value="'+activity.id+'">'+activity.name+'</option>'
            });
            $('#associated_activities').append(_content);
            $('#associated_activities').val(associated.activity_id);
        });

        //Filling seals
        $.get('associates/'+associated.id+'/seals', function(data){
            let _content, _option;
            _content = '';
            $('#associated_seals').empty();

            $.each(data.seals, function(i, seal){
                _option = '<option value="'+seal.id+'">'+seal.name+'</option>'

                $.each(data.seals_selected, function(u, seal_selected){
                        if (seal_selected.id == seal.id) {
                            _option = '<option value="'+seal.id+'" selected="selected">'+seal.name+'</option>'
                        }
                });

                _content += _option;
            });
            $('#associated_seals').append(_content);
        });

        addInputPut($('#form_associated'), 'associated_method');

        if (associated.logotype_thumb) {
            $('#associated_preview-image').append('<img src="'+associated.logotype_thumb+'" style="display: block;">');
            $('#associated_preview-text').remove();
        }
    });


    $('#associates_grid').hide();
    $('#associates_actions').show();
})

$('#associated__update').on('click', function(){
    let _route, _formData;
    cleanError();

    _route  = 'associates/'+$('#associated_id').val();
    _formData = new FormData($('#form_associated')[0]);
    _formData.append('seals', $('#associated_seals').val());

    lockWindow();
    $.ajaxSetup({
        headers: {
            'csrftoken': $('input[name=_token]').val()
        }
    });
    $.ajax({
       url : _route,
       type: 'POST',
       data: _formData,
       contentType: false,
       processData: false,
       success: function(e){
            unlockWindow();
            $.growl.notice({ message: "Se ha actualizado el asociado." });
            $('#associates_actions').hide();
            $('#associates_grid').show();

            $.get('associates', function(associates){
                loadGridAssociates(associates);
            });
     },
     error:function(jqXHR, textStatus, errorThrown)
     {
            unlockWindow();
            $.growl.error({ message: "Ha ocurrido un error." });
     }

     });
});

$('#associated__close').on('click', function(){
    $('#associates_grid').show();
    $('#associates_actions').hide();
});

$(document).on('click', '.associated__delete', function(){
    let _id = $(this).data('index');
    swal(
        { title: '¿Borrar el asociado?',
          text: '¿Está usted seguro?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: 'Aceptar',
          cancelButtonText:'Cancelar',
          closeOnConfirm: true
        },
        function(){
            let _route = "associates/"+_id;
            lockWindow();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('input[name=_token]').val()
                }
            });
            $.ajax({
                url: _route,
                data: {},
                type: 'DELETE',
                success: function(e) {
                    unlockWindow();
                    $.growl.notice({ message: "Se ha borrado el asociado." });
                    $.get('associates', function(associates){
                        loadGridAssociates(associates);
                    });
                },
                error: function(){
                    unlockWindow();
                    $.growl.error({ message: "Ha ocurrido un error." });
                }
            });
        });
});

$(document).on('click', '.associated__published', function(){

    let _id = $(this).data('index');
    let _lastStatus = $(this).data('published');
    let _text;
    if (_lastStatus == 1) {
        _text = '¿Dejar de publicar el asociado?';
    } else if(_lastStatus == 0){
        _text = '¿Publicar el asociado?';
    }

    swal(
        { title: _text,
          text: '¿Está usted seguro?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: 'Aceptar',
          cancelButtonText:'Cancelar',
          closeOnConfirm: true
        },
        function(){
            let _route = "associates/"+_id+"/published";
            lockWindow();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('input[name=_token]').val()
                }
            });
            $.ajax({
                url: _route,
                data: {},
                type: 'PUT',
                success: function(e) {
                    unlockWindow();
                    $.growl.notice({ message: "Se ha cambiado el estado del asociado." });
                    $.get('associates', function(associates){
                        loadGridAssociates(associates);
                    });
                },
                error: function(){
                    unlockWindow();
                    $.growl.error({ message: "Ha ocurrido un error." });
                }
            });
    });
});

$(document).on('click', '.associated__edit-images', function(){
    let _id;
    _id = $(this).data('index');

    $('#associated-image_id').val(_id);
    $('#associates_grid').hide();
    $('#associates_images').show();
    $('.swiper-wrapper').empty();
    $('#associated-swiper-container').attr('data-number', -1);

    setTimeout(function () {
        $.get('contents/'+_id+'/6/1',function(data){
            startCarouselAssociated();
            if (data.images.length) {
                addImageToSliderAssociated(swiperAssociated,$('#associated-swiper-container'),data.images);
            }
        });
    }, 500);
});

$('#associated-image__close').on('click', function(){
    $('#associates_grid').show();
    $('#associates_images').hide();
});

function loadGridAssociates(associates)
{
    let _content, _published;
    _content = '';

    $('#associates_grid').empty();

    $.each(associates,function(k, associated){
        _published  = getSimbolPublished(associated.published);

        _content +=  '<div class="col-lg-3 col-md-4 col-sm-6 phs">'+
                          '<li class="item-account">'+
                            '<figure class="item-image">'+
                              '<img src="'+associated.logotype_thumb+'" alt="" />'+
                            '</figure>'+
                            '<span style="display: block; text-align: center;">'+associated.name_company+'</span>'+
                            '<div class="item__controls">'+
                              '<button type="button" data-index="'+associated.id+'" data-activity_name="'+associated.name_company+'" data-published="'+associated.published+'" class="btn btn-warning associated__published" title="'+_published.name+'">'+
                                '<i class="'+_published.simbol+'"></i>'+
                             '</button>'+
                             '<button type="button" data-index="'+associated.id+'" class="btn btn-success associated__edit"  data-target="" data-toggle="modal" title="Editar">'+
                                 '<i class="glyphicon glyphicon-pencil"></i>'+
                            '</button>'+
                            '<button type="button" data-index="'+associated.id+'" class="btn associated__edit-images"  data-target="" data-toggle="modal" title="Editar Imagenes">'+
                                 '<i class="glyphicon glyphicon-picture"></i>'+
                            '</button>'+
                            '<button type="button" data-index="'+associated.id+'" class="btn btn-danger associated__delete"  title="Eliminar">'+
                                '<i class="glyphicon glyphicon-trash"></i>'+
                              '</button>'+
                            '</div>'+
                          '</li>'+
                        '</div>';
    });

    $('#associates_grid').append(_content);
}

function cleanAssociatedModal()
{

    $('#associated_preview-image').empty();
    $('#associated_image').val('');

    let _previewText;

    _previewText =      '<label id="associated_preview-text">'+
                            '<i class="glyphicon glyphicon-picture"></i>'+
                            '<span class="u-ml3">Añadir Foto</span>'+
                        '</label>';

    $('#associated_preview-text').remove();
    $('#associated_image-container').prepend(_previewText);

    $('#associated_id').val('');
    $('#associated_activities').empty();
    $('#associated_seals').empty();
    $('#associated_name').val('');
    $('#associated_representative').val('');
    $('#associated_email').val('');
    $('#associated_cel').val('');
    $('#associated_phone').val('');
    $('#associated_website').val('');
    $('#associated_facebook').val('');
    $('#associated_address').val('');

    $('#associated__save').show();
    $('#associated__update').show();
}

function startCarouselAssociated()
{
    swiperAssociated = new Swiper('#associated-swiper-container', {
        loop: false,
        pagination: '#associated-swiper-pagination',
        nextButton: '#associated-swiper-button-next',
        prevButton: '#associated-swiper-button-prev',
        slidesPerView: 3,
        //centeredSlides: true,
        //paginationClickable: true,
        spaceBetween: 30,
    });
}

function addImageToSliderAssociated(swiper,swiperContainer,images)
{
  var number = swiperContainer.attr('data-number');
  if (number == "") {
    number = -1;
  }
  else {
    number = parseInt(number);
  }
  $.each(images,function(i,image) {
    number = number + 1;
    swiper.appendSlide('<div class="swiper-slide" data-index="'+number+'" style="display:flex;flex-direction:column"><img src="'+image.resource_thumb+'" alt="" /><button class="image-slider-associated__delete form-control" data-image_id="'+image.id+'" style="margin-top: 10px;max-width:8rem;">Eliminar</button></div>');
  });
    swiperContainer.attr('data-number',number);
}

$(document).on('click', '.image-slider-associated__delete', function(){
    deleteSlide($(this).parent().data('index'),$(this).data('image_id'),swiperAssociated,$('#associated-swiper-container'));
});
