function loadGridServices(services){
	var content= '';
	var published;
	if (services) {
		$.each(services,function(k,service){
			published  = getSimbolPublished(service.published);
			content = content +  '<div class="col-lg-3 col-md-4 col-sm-6 phs">'+
									          '<li class="item-account">'+
									            '<figure class="item-image">'+
									              '<img src="'+service.image+'" alt="" />'+
												'</figure>'+
												'<span style="display: block; text-align: center;">'+service.name+'</span>'+
									            '<div class="item__controls">'+
									              '<button type="button" data-index="'+service.id+'" data-service_name="'+service.name+'" data-published="'+service.published+'" class="btn btn-warning service_published" title="'+published.name+'">'+
									                '<i class="'+published.simbol+'"></i>'+
									             '</button>'+
												 '<button type="button" data-index="'+service.id+'" class="btn btn-success service_edit"  data-target="#add-services" data-toggle="modal" title="Editar">'+
													 '<i class="glyphicon glyphicon-pencil"></i>'+
												'</button>'+
												'<button type="button" data-index="'+service.id+'" class="btn service___edit-images"  data-target="#service-images-modal" data-toggle="modal" title="Agregar y eliminar imágenes">'+
													 '<i class="glyphicon glyphicon-picture"></i>'+
												'</button>'+
									              '<button type="button" data-index="'+service.id+'" class="btn btn-danger service_delete"  title="Eliminar">'+
									                '<i class="glyphicon glyphicon-trash"></i>'+
									              '</button>'+
									            '</div>'+
									          '</li>'+
									        '</div>';
		});
		$('#services_grid').empty();
		$('#services_grid').append(content);
	}
}
