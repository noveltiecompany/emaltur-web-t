function getServiceToEdit(serviceId)
{
	$.get('services/'+serviceId,function(data){
		if (data) {
			$('#service_id').val(data.service.id);
			$('#service_name').val(data.service.name);
			$('#service_title').val(data.service.title);
			$('#service_price').val(data.service.price);
			$('#service_price-concept').val(data.service.price_concept);
			
			$('#service_steps').val(data.service.steps);

			$('#service_product-id').val(data.service.product_id);

			$('#service_description').val(data.service.description);
			$('#service_format').val(data.service.format);
			
			if (data.service.format == 2) {
				$('.first-format').hide();
				$('.second-format').show();
			}
			
			$('#service_price-concept').val(data.service.price_concept);

			$('#service_description').val(data.service.description);
			
	 		if (data.service.image) {
				$('#service_preview_image').append('<img src="'+data.service.image+'" style="display: block;">');
				$('#service_preview_text').hide();
			}
			
			fillProductSelect(data.service.tag_id);
		}
		else {
			alert("error");
		}
	});
}
