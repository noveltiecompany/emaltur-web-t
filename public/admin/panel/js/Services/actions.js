var swiperService;

$(document).on('ready',function(){
	$.get('services',function(data){
		loadGridServices(data.services);
	});
});

$(document).on('click','.service_edit',function(){
	cleanError();
	cleanModalServices('edit');
	getServiceToEdit($(this).data('index'));
});

$(document).on('click','.service_delete',function(){
	deleteService($(this).data('index'));
});

$(document).on('click','.service_published',function(){
	changeStatusPublishedService($(this).data('index'),$(this).data('service_name'),$(this).data('published'));
});

$('#service_save').on('click',function(){
	saveService();
});

$('#service_update').on('click',function(){
	updateService();
});

$('#service_add').on('click',function(){
	cleanError();
	cleanModalServices("add");
	fillProductSelect(null);
});

$(document).on('click', '.service___edit-images', function(){
	$('#service-gallery_swiper-container').attr('data-number', -1);
	$('.swiper-wrapper').empty();
	getImagesToEdit($(this).data('index'));
});

$('#service__close').on('click', function(){
	$('#service-images-modal').modal('hide');
});

$('#service_format').on('change', function(){
	let _that;
	_that = $(this);

	if (_that.val() == 1) {
		$('.first-format').show();
		$('.second-format').hide();
	} else if(_that.val() == 2){
		$('.first-format').hide();
		$('.second-format').show();
	}
});

function cleanModalServices(operation){
	var _previewText = "";

	_previewText = '<label id="service_preview_text">'+
		  					'<i class="glyphicon glyphicon-picture"></i>'+
		  					'<span class="u-ml3">Añadir Foto</span>'+
							'</label>';

	$('#service_name').val('');
	$('#service_preview_image').empty();
	$('#service_image').val('');

	$('#service_preview_text').remove();

	//cleanEditor($('#service_description'));
	//cleanEditor($('#service_steps'));

	$('#div_image-container_services-modal').prepend(_previewText);

	$('#service_format').val(1);
	$('#service_title').val('');
	$('#service_price').val(0);
	$('#service_price-concept').val('');
	$('#service_steps').val('');
	$('#service_description').val('');

	$('.first-format').show();
	$('.second-format').hide();

	if (operation == 'add') {
		$('#service_method').remove();
		$('#service_update').hide();
		$('#service_save').show();
	}
	else if(operation =='edit'){
		$('#service_method').remove();
		$('#form_services').append('<input type="hidden" id="service_method" name="_method" value="PUT" />');
		$('#service_update').show();
		$('#service_save').hide();
	}
}

//He puesto esta funcion aqui porque no me permite llamarla desde otro archivo js
function changeStatusPublishedService(serviceId,serviceName,lastStatus)
{
	var text;
	if (lastStatus == 1) {
		text = "¿Dejar de publicar la característica "+serviceName+'?';
	}
	else if (lastStatus == 0) {
		text = "¿Publicar la característica "+serviceName+'?';
	}
	swal({
	  title: text,
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: "#DD6B55",
	  confirmButtonText: 'Aceptar',
	  cancelButtonText:'Cancelar',
	  closeOnConfirm: true },
	  function(){
		  var ruta = "services/change-published-status";
		 lockWindow();
		$.ajaxSetup({
		  headers: {
			  'X-CSRF-TOKEN': $('input[name=_token]').val()
		  }
		});
		$.ajax({
			url: ruta,
			data: {'serviceId':serviceId,'lastStatus':lastStatus},
			type: 'PUT',
			success: function(result) {
				unlockWindow();
				if (result.success == true) {
					$.growl.notice({ message: "Se ha cambiado el estado de la característica" });
					$.get('services',function(data){
						loadGridServices(data.services);
					});
				}
				if (result.success == false) {
					$.growl.error({ message: "Ha ocurrido un error al intentar cambiar el estado de la característica" });
				}
			}
		});
	  });
}

$('#service__close').on('click', function(){
	$('#service-modal_images').modal('hide');
});

function fillProductSelect(productSelected)
{
	let _options = '';
	$('#service_product-id').empty();
	$.get('products', function(products){

		$.each(products, function(i, product){
			_options += '<option value="'+product.id+'">'+product.name+'</option>';
		});
		$('#service_product-id').append(_options);

		if (productSelected != null) {
			$('#service_product-id').val(productSelected);
		}

	});
}

/* Service Gallery */
function startCarouelService()
{
		swiperService = new Swiper('#service-gallery_swiper-container', {
			loop: false,
			pagination: '#service-gallery_swiper-pagination',
			nextButton: '#service-gallery_swiper-button-next',
			prevButton: '#service-gallery_swiper-button-prev',
			slidesPerView: 3,
			//centeredSlides: true,
			//paginationClickable: true,
			spaceBetween: 30,
		});
}

$(document).on('click', '.btn_image-slider-service__delete', function(){
	deleteSlide($(this).parent().data('index'),$(this).data('image_id'),swiperService,$('#service-gallery_swiper-container'));
});
