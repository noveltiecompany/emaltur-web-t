$(document).on('ready', function(){
    $.get('activities', function(activities){
        loadGridActivities(activities);
    });
});

$('#activity__add').on('click', function(){
    cleanActivityModal();
    $('#activity-modal').modal('show');
    $('#activity__update').hide();
});

$('#activity__save').on('click', function(){
    let _route, _formData;
    cleanError();

    _route  = 'activities';
    _formData = new FormData($('#form_activity')[0]);

    lockWindow();
    $.ajaxSetup({
        headers: {
            'csrftoken': $('input[name=_token]').val()
        }
    });
    $.ajax({
       url : _route,
       type: 'POST',
       data: _formData,
       contentType: false,
       processData: false,
       success: function(e){
            unlockWindow();
            $.growl.notice({ message: "Se ha guardado el rubro." });
            $('#activity-modal').modal('hide');

            $.get('activities', function(activities){
                loadGridActivities(activities);
            });
     },
     error:function(jqXHR, textStatus, errorThrown)
     {
            unlockWindow();
            $.growl.error({ message: "Ha ocurrido un error." });
     }

     });
});

$(document).on('click', '.activity__edit', function(){
    let _activityId = $(this).data('index');

    $.get('activities/'+_activityId, function(activity){
        cleanActivityModal();
        $('#activity__save').hide();
        $('#activity_id').val(activity.id);
        $('#activity_name').val(activity.name);
        $('#activity_description').val(activity.description);
        addInputPut($('#form_activity'), 'activity_method');

        if (activity.image_thumb) {
            $('#activity_preview-image').append('<img src="'+activity.image_thumb+'" style="display: block;">');
            $('#activity_preview-text').remove();
        }
    });
    $('#activity-modal').modal('show');
})

$('#activity__update').on('click', function(){
    let _route, _formData;
    cleanError();

    _route  = 'activities/'+$('#activity_id').val();
    _formData = new FormData($('#form_activity')[0]);

    lockWindow();
    $.ajaxSetup({
        headers: {
            'csrftoken': $('input[name=_token]').val()
        }
    });
    $.ajax({
       url : _route,
       type: 'POST',
       data: _formData,
       contentType: false,
       processData: false,
       success: function(e){
            unlockWindow();
            $.growl.notice({ message: "Se ha actualizado el rubro." });
            $('#activity-modal').modal('hide');

            $.get('activities', function(activities){
                loadGridActivities(activities);
            });
     },
     error:function(jqXHR, textStatus, errorThrown)
     {
            unlockWindow();
            $.growl.error({ message: "Ha ocurrido un error." });
     }

     });
});

$('#activity__close').on('click', function(){
    $('#activity-modal').modal('hide');
});

$(document).on('click', '.activity__delete', function(){
    let _id = $(this).data('index');
    swal(
        { title: '¿Borrar la actividad?',
          text: '¿Está usted seguro?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: 'Aceptar',
          cancelButtonText:'Cancelar',
          closeOnConfirm: true
        },
        function(){
            let _route = "activities/"+_id;
            lockWindow();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('input[name=_token]').val()
                }
            });
            $.ajax({
                url: _route,
                data: {},
                type: 'DELETE',
                success: function(e) {
                    unlockWindow();
                    $.growl.notice({ message: "Se ha borrado el rubro." });
                    $.get('activities', function(activities){
                        loadGridActivities(activities);
                    });
                },
                error: function(){
                    unlockWindow();
                    $.growl.error({ message: "Ha ocurrido un error." });
                }
            });
        });
});

$(document).on('click', '.activity__published', function(){

    let _id = $(this).data('index');
    let _lastStatus = $(this).data('published');
    let _text;
    if (_lastStatus == 1) {
        _text = '¿Dejar de publicar el rubro?';
    } else if(_lastStatus == 0){
        _text = '¿Publicar el rubro?';
    }

    swal(
        { title: _text,
          text: '¿Está usted seguro?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: 'Aceptar',
          cancelButtonText:'Cancelar',
          closeOnConfirm: true
        },
        function(){
            let _route = "activities/"+_id+"/published";
            lockWindow();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('input[name=_token]').val()
                }
            });
            $.ajax({
                url: _route,
                data: {},
                type: 'PUT',
                success: function(e) {
                    unlockWindow();
                    $.growl.notice({ message: "Se ha cambiado el estado del rubro." });
                    $.get('activities', function(activities){
                        loadGridActivities(activities);
                    });
                },
                error: function(){
                    unlockWindow();
                    $.growl.error({ message: "Ha ocurrido un error." });
                }
            });
    });
});

function loadGridActivities(activities)
{
    let _content, _published;
    _content = '';

    $('#activities_grid').empty();

    $.each(activities,function(k, activity){
        _published  = getSimbolPublished(activity.published);

        _content +=  '<div class="col-lg-3 col-md-4 col-sm-6 phs">'+
                                          '<li class="item-account">'+
                                            '<figure class="item-image">'+
                                              '<img src="'+activity.image+'" alt="" />'+
                                            '</figure>'+
                                            '<span style="display: block; text-align: center;">'+activity.name+'</span>'+
                                            '<div class="item__controls">'+
                                              '<button type="button" data-index="'+activity.id+'" data-activity_name="'+activity.name+'" data-published="'+activity.published+'" class="btn btn-warning activity__published" title="'+_published.name+'">'+
                                                '<i class="'+_published.simbol+'"></i>'+
                                             '</button>'+
                                             '<button type="button" data-index="'+activity.id+'" class="btn btn-success activity__edit"  data-target="" data-toggle="modal" title="Editar">'+
                                                 '<i class="glyphicon glyphicon-pencil"></i>'+
                                            '</button>'+
                                            '<button type="button" data-index="'+activity.id+'" class="btn btn-danger activity__delete"  title="Eliminar">'+
                                                '<i class="glyphicon glyphicon-trash"></i>'+
                                              '</button>'+
                                            '</div>'+
                                          '</li>'+
                                        '</div>';
    });

    $('#activities_grid').append(_content);

}

function cleanActivityModal()
{
    $('#activity_id').val('');
    $('#activity_name').val('');
    $('#activity_description').val('');

    $('#activity_method').remove();

    $('#activity__save').show();
    $('#activity__update').show();

    $('#activity_image').val('');
    $('#activity_preview-image').empty();

    let _previewText;

    _previewText =      '<label id="activity_preview-text">'+
                            '<i class="glyphicon glyphicon-picture"></i>'+
                            '<span class="u-ml3">Añadir Foto</span>'+
                        '</label>';

    $('#activity_preview-text').remove();
    $('#activity_image-container').prepend(_previewText);
}