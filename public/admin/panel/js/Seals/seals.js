$(document).on('ready', function(){
    $.get('seals', function(seals){
        loadGridSeals(seals);
    });
});
  
$('#seal__add').on('click', function(){
    cleanSealModal();
    $('#seal-modal').modal('show');
    $('#seal__update').hide();
});

$('#seal__save').on('click', function(){
    let _route, _formData;
    cleanError();

    _route  = 'seals';
    _formData = new FormData($('#form_seal')[0]);

    lockWindow();
    $.ajaxSetup({
        headers: {
            'csrftoken': $('input[name=_token]').val()
        }
    });
    $.ajax({
       url : _route,
       type: 'POST',
       data: _formData,
       contentType: false,
       processData: false,
       success: function(e){
            unlockWindow();
            $.growl.notice({ message: "Se ha guardado el sello." });
            $('#seal-modal').modal('hide');

            $.get('seals', function(seals){
                loadGridSeals(seals);
            });
     },
     error:function(jqXHR, textStatus, errorThrown)
     {
            unlockWindow();
            $.growl.error({ message: "Ha ocurrido un error." });
     }

     });
});

$(document).on('click', '.seal__edit', function(){
    let _sealId = $(this).data('index');

    $.get('seals/'+_sealId, function(seal){
        cleanSealModal();
        $('#seal__save').hide();
        $('#seal_id').val(seal.id);
        $('#seal_name').val(seal.name);
        $('#seal_description').val(seal.description);
        addInputPut($('#form_seal'), 'seal_method');

        if (seal.image) {
            $('#seal_preview-image').append('<img src="'+seal.image+'" style="display: block;">');
            $('#seal_preview-text').remove();
        }
    });
    $('#seal-modal').modal('show');
})

$('#seal__update').on('click', function(){
    let _route, _formData;
    cleanError();

    _route  = 'seals/'+$('#seal_id').val();
    _formData = new FormData($('#form_seal')[0]);

    lockWindow();
    $.ajaxSetup({
        headers: {
            'csrftoken': $('input[name=_token]').val()
        }
    });
    $.ajax({
       url : _route,
       type: 'POST',
       data: _formData,
       contentType: false,
       processData: false,
       success: function(e){
            unlockWindow();
            $.growl.notice({ message: "Se ha actualizado el sello." });
            $('#seal-modal').modal('hide');

            $.get('seals', function(seals){
                loadGridSeals(seals);
            });
     },
     error:function(jqXHR, textStatus, errorThrown)
     {
            unlockWindow();
            $.growl.error({ message: "Ha ocurrido un error." });
     }

     });
});

$('#seal__close').on('click', function(){
    $('#seal-modal').modal('hide');
});

$(document).on('click', '.seal__delete', function(){
    let _id = $(this).data('index');
    swal(
        { title: '¿Borrar el sello?',
          text: '¿Está usted seguro?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: 'Aceptar',
          cancelButtonText:'Cancelar',
          closeOnConfirm: true
        },
        function(){
            let _route = "seals/"+_id;
            lockWindow();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('input[name=_token]').val()
                }
            });
            $.ajax({
                url: _route,
                data: {},
                type: 'DELETE',
                success: function(e) {
                    unlockWindow();
                    $.growl.notice({ message: "Se ha borrado el sello." });
                    $.get('seals', function(seals){
                        loadGridSeals(seals);
                    });
                },
                error: function(){
                    unlockWindow();
                    $.growl.error({ message: "Ha ocurrido un error." });
                }
            });
        });
});

$(document).on('click', '.seal__published', function(){

    let _id = $(this).data('index');
    let _lastStatus = $(this).data('published');
    let _text;
    if (_lastStatus == 1) {
        _text = '¿Dejar de publicar el sello?';
    } else if(_lastStatus == 0){
        _text = '¿Publicar el sello?';
    }

    swal(
        { title: _text,
          text: '¿Está usted seguro?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: 'Aceptar',
          cancelButtonText:'Cancelar',
          closeOnConfirm: true
        },
        function(){
            let _route = "seals/"+_id+"/published";
            lockWindow();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('input[name=_token]').val()
                }
            });
            $.ajax({
                url: _route,
                data: {},
                type: 'PUT',
                success: function(e) {
                    unlockWindow();
                    $.growl.notice({ message: "Se ha cambiado el estado del sello." });
                    $.get('seals', function(seals){
                        loadGridSeals(seals);
                    });
                },
                error: function(){
                    unlockWindow();
                    $.growl.error({ message: "Ha ocurrido un error." });
                }
            });
    });
});

function loadGridSeals(seals)
{   
    let _content, _published;
    _content = '';

    $('#seals_grid').empty();

    $.each(seals,function(k, seal){
        _published  = getSimbolPublished(seal.published);

        _content +=  '<div class="col-lg-3 col-md-4 col-sm-6 phs">'+
                        '<li class="item-account">'+
                          '<figure class="item-image">'+
                            '<img src="'+seal.image+'" alt="" />'+
                          '</figure>'+
                          '<span style="display: block; text-align: center;">'+seal.name+'</span>'+
                          '<div class="item__controls">'+
                            '<button type="button" data-index="'+seal.id+'" data-seal_name="'+seal.name+'" data-published="'+seal.published+'" class="btn btn-warning seal__published" title="'+_published.name+'">'+
                              '<i class="'+_published.simbol+'"></i>'+
                           '</button>'+
                           '<button type="button" data-index="'+seal.id+'" class="btn btn-success seal__edit"  data-target="" data-toggle="modal" title="Editar">'+
                               '<i class="glyphicon glyphicon-pencil"></i>'+
                          '</button>'+
                          '<button type="button" data-index="'+seal.id+'" class="btn btn-danger seal__delete"  title="Eliminar">'+
                              '<i class="glyphicon glyphicon-trash"></i>'+
                            '</button>'+
                          '</div>'+
                        '</li>'+
                      '</div>';
    });

    $('#seals_grid').append(_content);

}

function cleanSealModal()
{
    $('#seal_id').val('');
    $('#seal_name').val('');
    $('#seal_description').val('');

    $('#seal_method').remove();

    $('#seal__save').show();
    $('#seal__update').show();

    $('#seal_image').val('');
    $('#seal_preview-image').empty();

    let _previewText;

    _previewText =      '<label id="seal_preview-text">'+
                            '<i class="glyphicon glyphicon-picture"></i>'+
                            '<span class="u-ml3">Añadir Foto</span>'+
                        '</label>';

    $('#seal_preview-text').remove();
    $('#seal_image-container').prepend(_previewText);
}