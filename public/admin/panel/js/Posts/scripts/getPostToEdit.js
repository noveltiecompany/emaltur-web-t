function getPostToEdit(postId)
{
	$.get('posts/'+postId,function(data){
		post.id.val(data.post.id);
		post.title.val(data.post.title);

		post.content.val(data.post.content);
		post.content.summernote({
			lang: 'es-ES',
			toolbar: [
				['style', ['bold', 'italic', 'underline', 'clear']],
				['fontsize', ['fontsize']],
				['para', ['ul', 'ol', 'paragraph']],
			]
		});
		post.modalTitle.text("Información - "+data.post.title);

		let _options = '';
		$.get('tags', function(tags){
			$.each(tags, function(i, tag){
				_options += '<option value="'+tag.id+'">'+tag.name+'</option>';
			});
			$('#post_tag-id').empty();
			$('#post_tag-id').append(_options);
			$('#post_tag-id').val(data.post.tag_id);
		});
	});
}
