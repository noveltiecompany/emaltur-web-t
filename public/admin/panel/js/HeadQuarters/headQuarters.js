$(document).on('ready', function(){
	$.get('headquarters', function(headquarters){
		loadGridHeadQuarters(headquarters);
	});
});

$('#headquarter__create').on('click', function(){
	cleanHeadQuarterModal();
	$('#headquarter__update').hide();
});

$('#headquarter__save').on('click', function(){
	cleanError();

	let _route  = 'headquarters';
	let _formData = new FormData($('#form_headquarter')[0]);

	lockWindow();
	$.ajaxSetup({
		headers: {
			'csrftoken': $('input[name=_token]').val()
		}
	});
	$.ajax({
	   url : _route,
	   type: 'post',
	   data: _formData,
	   contentType: false,
	   processData: false,
	   success: function(e){
			unlockWindow();

			if (e.success == false ) {
				$.growl.error({ message: "Ha ocurrido un error" })
			}
			else if(e.success == true){
				$.growl.notice({ message: "Se ha guardado la sede" });
				$('#headquarter-modal').modal('hide');
				$.get('headquarters', function(headquarters){
					loadGridHeadQuarters(headquarters);
				});
			}
	 },
	 error:function(jqXHR, textStatus, errorThrown)
	 {
			unlockWindow();
			$('#product-error').append(msgError);

			$.each(jqXHR.responseJSON, function( key, value ) {
				if (key == "title") {
					$.each(value, function( errores, eror ) {
						$('#headquarter-error-title').append("<li class='error-block'>"+eror+"</li>");
					});
				}
			});
	 }

	 });

});

$(document).on('click', '.headquarter__edit', function(){
	cleanHeadQuarterModal();
	$('#headquarter__save').hide();
	addInputPut($('#form_headquarter'), 'headquarter_method');

	let _headquarterId;
	_headquarterId = $(this).data('index');

	$.get('headquarters/'+_headquarterId, function(headquarter){
		$('#headquarter_title').val(headquarter.title);
		$('#headquarter_id').val(headquarter.id);

		if (headquarter.image) {
			$('#headquarter_preview-image').append('<img src="'+headquarter.image+'" style="display: block;">');
			$('#headquarter_preview-text').remove();
		}

	});

});

$('#headquarter__update').on('click', function(){
	let _headquarterId = $('#headquarter_id').val();
	let _formData = new FormData($('#form_headquarter')[0]);

	let _route = 'headquarters/'+_headquarterId;

	lockWindow();
	$.ajaxSetup({
		headers: {
			'csrftoken': $('input[name=_token]').val()
		}
	});
	$.ajax({
	   url : _route,
	   type: 'POST',
	   data: _formData,
	   contentType: false,
	   processData: false,
	   success: function(e){
			unlockWindow();

			if (e.success == false ) {
				$.growl.error({ message: "Ha ocurrido un error" })
			}
			else if(e.success == true){
				$.growl.notice({ message: "Se ha actualizado la sede" });
				$('#headquarter-modal').modal('hide');
				$.get('headquarters', function(headquarters){
					loadGridHeadQuarters(headquarters);
				});
			}
	 	},
		 error:function(jqXHR, textStatus, errorThrown)
		 {
				unlockWindow();
				$('#product-error').append(msgError);

				$.each(jqXHR.responseJSON, function( key, value ) {
					if (key == "title") {
						$.each(value, function( errores, eror ) {
							$('#headquarter-error-title').append("<li class='error-block'>"+eror+"</li>");
						});
					}
				});
		 }

	 });
});

$(document).on('click', '.headquarter__published', function(){

	let _text, _route;

	if ($(this).data('published') == 1) {
		_text = "¿Dejar de publicar el valor "+$(this).data('headquarter_title')+"?";
	}
	else if ($(this).data('published') == 0) {
		_text = "¿Publicar el valor "+$(this).data('headquarter_title')+"?";
	}

	_route = 'headquarters/'+$(this).data('index')+'/published';

	swal({
	  title: _text,
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: "#DD6B55",
	  confirmButtonText: 'Aceptar',
	  cancelButtonText:'Cancelar',
	  closeOnConfirm: true },
	  function(){
		lockWindow();
		$.ajaxSetup({
		  headers: {
			  'X-CSRF-TOKEN': $('input[name=_token]').val()
		  }
		});
		$.ajax({
			url: _route,
			data: {},
			type: 'PUT',
			success: function(result) {
				unlockWindow();
				if (result.success == true) {
					$.growl.notice({ message: "Se ha cambiado el estado de esta sede" });

					$.get('headquarters',function(headquarters){
						loadGridHeadQuarters(headquarters);
					});
				}
				if (result.success == false) {
					$.growl.error({ message: "Ha ocurrido un error" });
				}
			}
		});
	  });

});

$(document).on('click', '.headquarter__delete', function(){

	let _that = $(this);

	swal({   title: 'Borrar la sede',
	text: '¿Está usted seguro?',
	type: 'warning',
	showCancelButton: true,
	confirmButtonColor: "#DD6B55",
	confirmButtonText: 'Aceptar',
	cancelButtonText: 'Cancelar',
	closeOnConfirm: true },
	function(){

	  let _route = 'headquarters/'+_that.data('index');
	  lockWindow();
	  $.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('input[name=_token]').val()
		}
	  });
	  $.ajax({
		  url: _route,
		  data: {},
		  type: 'DELETE',
		  success: function(result) {
			  unlockWindow();
			  if (result.success == true) {
					$.growl.notice({ message: "Se ha borrado la sede" });
			  		$.get('headquarters', function(headquarters){
			  			loadGridValues(headquarters);
			  		});
			  }
			  else if(result.success == false) {
				  $.growl.error({ message: "Ha ocurrido un error" });
			  }
		  }
	  });
	});

});

/* Funciones */
function loadGridHeadQuarters(headquarters)
{
	let _content= '';
	let _published;

	$.each(headquarters, function(i, headquarter){
		_published  = getSimbolPublished(headquarter.published);
		_content += '<div class="col-lg-3 col-md-4 col-sm-6 phs">'+
			          '<li class="item-account">'+
			            '<figure class="item-image">'+
			              '<img src="'+headquarter.image_thumb+'" alt="" />'+
						'</figure>'+
						'<span style="display: block; text-align: center;">'+headquarter.title+'</span>'+
			            '<div class="item__controls">'+
			              '<button type="button" data-index="'+headquarter.id+'" data-headquarter_title="'+headquarter.title+'" data-published="'+headquarter.published+'" class="btn btn-warning headquarter__published" title="'+_published.name+'">'+
			                '<i class="'+_published.simbol+'"></i>'+
			             '</button>'+
						 '<button type="button" data-index="'+headquarter.id+'" class="btn btn-success headquarter__edit"  data-target="#headquarter-modal" data-toggle="modal" title="Editar">'+
							 '<i class="glyphicon glyphicon-pencil"></i>'+
						'</button>'+
			            '<button type="button" data-index="'+headquarter.id+'" class="btn btn-danger headquarter__delete"  title="Eliminar">'+
			                '<i class="glyphicon glyphicon-trash"></i>'+
			              '</button>'+
			            '</div>'+
			          '</li>'+
			        '</div>';
	});

	$('#headquarters_grid').empty();
	$('#headquarters_grid').append(_content);
}

function cleanHeadQuarterModal()
{
	cleanError();
	$('#headquarter_title').val('');
	$('#headquarter_id').val('');
	$('#headquarter_method').remove();
	$('#headquarter_preview-image').empty();
	$('#headquarter_image').val('');

	let _previewText = 	'<label id="headquarter_preview-text">'+
		  					'<i class="glyphicon glyphicon-picture"></i>'+
		  					'<span class="u-ml3">Añadir Foto</span>'+
						'</label>';

	$('#headquarter_preview-text').remove();
	$('#headquarter_container-image').prepend(_previewText);

	$('#headquarter__save').show();
	$('#headquarter__update').show();

}


