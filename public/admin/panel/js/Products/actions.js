var swiperProduct;

$(document).on('ready',function(){
	
	$.get('products', function(products){
		loadGridProducts(products);
	});

});

$(document).on('click','#product__add',function(){
	cleanError();
	cleanModalProduct('add');
});

$('#product__save').on('click',function(){
	saveProduct();
});

$(document).on('click','.product__published',function(){
	changeStatusPublishedProduct($(this).data('index'),$(this).data('name'),$(this).data('published'));
});
	
$('#product__update').on('click',function(){
	updateProduct($('#product_id').val());
});

$(document).on('click','.product__edit',function(){
	cleanError();
	cleanModalProduct('edit');
	getProductToEdit($(this).data('index'));
});

$(document).on('click','.product_edit_images',function(){
	cleanError();
	cleanModalProduct();
	statusCloseModalProduct = true;
	getProductToEditImages($(this).data('index'));
});

$(document).on('click','.product__delete',function(){
	deleteProduct($(this).data('index'));
});

$('#product_close').on('click',function(){
	cleanDropzone('product-dropzone');
});

$('#button-product__close-modal').on('click',function(){
	$('#edit-item').modal('hide');
	cleanDropzone('product-dropzone');

});

$(document).on('click','.image-slide-button__delete',function(){
	deleteSlide($(this).parent().data('index'),$(this).data('image_id'),swiperProduct,$('#product-swiper-container'));
});


function cleanModalProduct(operation){

	$('#product_id').val('');
	// $('#product_category_id').val('');
	$('#product_name').val('');
	cleanEditor($('#product_description'));
	$('#product_description').val('');
	$('#product_image').val('');
	$('#product_title').val('');

	$('#product_featured').val(0);

	$('#product-swiper-container').show();
	$('#product-swiper-container').attr('data-number',"");
	$('.swiper-wrapper').empty();
	
	let _previewText;

	$('#product_preview-text').remove();
	_previewText = '<label id="product_preview-text">'+
		  					'<i class="glyphicon glyphicon-picture"></i>'+
		  					'<span class="u-ml3">Añadir Foto</span>'+
							'</label>';

	$('#product_container-image').prepend(_previewText);
	$('#product_preview-image').empty();

	if (operation == 'add') {
		editor2('product_description');
		$('#product_method').remove();
		$('#product__update').hide();
		$('#product__save').show();
	}
	else if(operation =='edit'){
		$('#product_method').remove();
		$('#form_product').append('<input type="hidden" id="product_method" name="_method" value="PUT" />');
		$('#product__update').show();
		$('#product__save').hide();
	}
}

function startCarouselProduct()
{
		swiperProduct = new Swiper('#product-swiper-container', {
			loop: false,
			pagination: '#product-swiper-pagination',
			nextButton: '#product-swiper-button-next',
			prevButton: '#product-swiper-button-prev',
			slidesPerView: 3,
			//centeredSlides: true,
			//paginationClickable: true,
			spaceBetween: 30,
		});
}
