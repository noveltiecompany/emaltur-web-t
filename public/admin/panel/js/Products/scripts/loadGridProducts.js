function loadGridProducts(products){
	
	var content= '';
	var published;
	if (products) {
		$.each(products,function(k,product){
			published  = getSimbolPublished(product.published);
			content = content +  '<div class="col-lg-3 col-md-4 col-sm-6 phs">'+
									          '<li class="item-account">'+
									            '<figure class="item-image">'+
									              '<img src="'+product.image_thumb+'" alt="" />'+
												'</figure>'+
												'<span style="display: block; text-align: center;">'+product.name+'</span>'+
									            '<div class="item__controls">'+
									              '<button type="button" data-index="'+product.id+'" data-name="'+product.name+'" data-published="'+product.published+'" class="btn btn-warning product__published" title="'+published.name+'">'+
									                '<i class="'+published.simbol+'"></i>'+
									             '</button>'+
												 '<button type="button" data-index="'+product.id+'" class="btn btn-success product__edit"  data-target="#product-modal" data-toggle="modal" title="Editar">'+
													 '<i class="glyphicon glyphicon-pencil"></i>'+
												'</button>'+
									              '<button type="button" data-index="'+product.id+'" class="btn btn-danger product__delete"  title="Eliminar">'+
									                '<i class="glyphicon glyphicon-trash"></i>'+
									              '</button>'+
									            '</div>'+
									          '</li>'+
									        '</div>';
		});
		$('#products_grid').empty();
		$('#products_grid').append(content);
	}
}
