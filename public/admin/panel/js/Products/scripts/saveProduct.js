function saveProduct(){
	cleanError();
	let _route  = 'products';
	let _formData = new FormData($('#form_product')[0]);

	lockWindow();
	$.ajaxSetup({
		headers: {
			'csrftoken': $('input[name=_token]').val()
		}
	});
	$.ajax({
	   url : _route,
	   type: 'post',
	   data: _formData,
	   contentType: false,
	   processData: false,
	   success: function(e){
		 unlockWindow();
		 successSaveProduct(e);
	 },
	 error:function(jqXHR, textStatus, errorThrown)
	 {
		  unlockWindow();
		  errorSaveProduct(jqXHR, textStatus, errorThrown);
	 }

	 });
}

function successSaveProduct(data){
	if (data.success == false ) {
		$.growl.error({ message: "Ha ocurrido un error" })
	}
	else if(data.success == true){
		$.growl.notice({ message: "Se ha guardado el servicio" });
		$('#product-modal').modal('hide');
		$.get('products', function(products){
			loadGridProducts(products);
		});
	}
};

function errorSaveProduct(jqXHR, textStatus, errorThrown){
	$('#product-error').append(msgError);
	$.each(jqXHR.responseJSON, function( key, value ) {
		if (key == "name") {
			$.each(value, function( errores, eror ) {
				$('#product-error-name').append("<li class='error-block'>"+eror+"</li>");
			});
		}
		else if (key == "description") {
			$.each(value, function( errores, eror ) {
				$('#product-error-description').append("<li class='error-block'>"+eror+"</li>");
			});
		}
		else if (key == "title") {
			$.each(value, function( errores, eror ) {
				$('#product-error-title').append("<li class='error-block'>"+eror+"</li>");
			});
		};
	});
};
