function getProductToEdit(productId){
	var ruta = 'products/'+productId;
	ajaxAll_GET(ruta,successGetProductToEdit);
}

function successGetProductToEdit(data){
	if (data.product) {
		$('#product_id').val(data.product.id);
		$('#product_name').val(data.product.name);
		$('#product_title').val(data.product.title);
		$('#product_description').val(data.product.description);
		editor2('product_description');

		$('#product_featured').val(data.product.featured_product);

		if (data.product.image) {
			$('#product_preview-text').remove();
			$('#product_preview-image').append('<img src="'+data.product.image+'" style="display: block;">');
		}
	}
	else {
		alert('Servicio no encontrado');
	}
}
