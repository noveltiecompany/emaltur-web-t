function changeStatusPublishedProduct(productId,productName,lastStatus)
{
	var text;
	if (lastStatus == 1) {
		text = "¿Dejar de publicar el servicio "+productName+'?';
	}
	else if (lastStatus == 0) {
		text = "¿Publicar el producto "+productName+'?';
	}

	swal({   title: text,
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: "#DD6B55",
	  confirmButtonText: 'Aceptar',
	  cancelButtonText:'Cancelar',
	  closeOnConfirm: true },
	  function(){
		  var ruta = "products/"+productId+"/published";
		 lockWindow();
		$.ajaxSetup({
		  headers: {
			  'X-CSRF-TOKEN': $('input[name=_token]').val()
		  }
		});
		$.ajax({
			url: ruta,
			data: {},
			type: 'PUT',
			success: function(result) {
				unlockWindow();
				if (result.success == true) {
					$.growl.notice({ message: "Se ha cambiado el estado del servicio" });
					$.get('products', function(products){
						loadGridProducts(products);
					});
				}
				if (result.success == false) {
					$.growl.error({ message: "Ha ocurrido un error al intentar cambiar el estado del servicio" });
				}
			}
		});
	  });
}
