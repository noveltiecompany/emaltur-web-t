function updateProduct(productId){
	cleanError();
	var ruta  = 'products/'+productId;
	var formData = new FormData($('#form_product')[0]);

	lockWindow();
	$.ajaxSetup({
		headers: {
			'csrftoken': $('input[name=_token]').val()
		}
	});
	$.ajax({
	   url : ruta,
	   type: 'post',
	   data: formData,
	   contentType: false,
	   processData: false,
	   success: function(e){
		 unlockWindow();
		 successUpdateProduct(e);
	 },
	 error:function(jqXHR, textStatus, errorThrown)
	 {
		  unlockWindow();
		  errorSaveProduct(jqXHR, textStatus, errorThrown);
	 }
	 });
}
function successUpdateProduct(data){
	if (data.success == false ) {
		$.growl.error({ message: "Ha ocurrido un error" })
	}
	else if(data.success == true){
		$.growl.notice({ message: "Se ha actualizado el servicio "});
		$('#product-modal').modal('hide');
		$.get('products', function(products){
			loadGridProducts(products);
		});
	}
}
