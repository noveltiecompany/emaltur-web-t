$(document).on('ready', function(){
	$.get('subscriptions', function(quantity){
		if (quantity == 0) {
			$.growl.warning({ message: "No se encuentran suscripciones registradas." });
			$('#excel-container').hide();
		} else {
		}
	});
});

$(document).on('click', '.view-subscription', function(){
    let _subscriptionId = $(this).data('subscription_id');

    $.get('subscriptions/'+_subscriptionId, function(subscription){
        $('#subscription_code').text(subscription.code);
        $('#subscription_date').text(subscription.date);
        $('#subscription_email').text(subscription.email);
        $('#subscription_name').text(subscription.name)
        $('#subscription_phone').text(subscription.phone)
        $('#subscription_interests').text(subscription.interests)
    });


});