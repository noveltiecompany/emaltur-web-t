/* A polyfill for browsers that don't support ligatures. */
/* The script tag referring to this file must be placed before the ending body tag. */

/* To provide support for elements dynamically added, this script adds
   method 'icomoonLiga' to the window object. You can pass element references to this method.
*/
(function () {
    'use strict';
    function supportsProperty(p) {
        var prefixes = ['Webkit', 'Moz', 'O', 'ms'],
            i,
            div = document.createElement('div'),
            ret = p in div.style;
        if (!ret) {
            p = p.charAt(0).toUpperCase() + p.substr(1);
            for (i = 0; i < prefixes.length; i += 1) {
                ret = prefixes[i] + p in div.style;
                if (ret) {
                    break;
                }
            }
        }
        return ret;
    }
    var icons;
    if (!supportsProperty('fontFeatureSettings')) {
        icons = {
            'web': '&#xe914;',
            'left': '&#xe911;',
            'right': '&#xe913;',
            'whatsapp': '&#xe912;',
            'close': '&#xe910;',
            'facebook': '&#xe90b;',
            'gplus': '&#xe90c;',
            'instagram': '&#xe90d;',
            'twitter': '&#xe90e;',
            'youtube': '&#xe90f;',
            'facebook-circle': '&#xe900;',
            'arrow-right': '&#xe901;',
            'arrow-left': '&#xe902;',
            'gplus-circle': '&#xe903;',
            'mail': '&#xe904;',
            'phone': '&#xe905;',
            'compass': '&#xe906;',
            'instagram-circle': '&#xe907;',
            'menu': '&#xe908;',
            'play': '&#xe909;',
            'twitter-circle': '&#xe90a;',
          '0': 0
        };
        delete icons['0'];
        window.icomoonLiga = function (els) {
            var classes,
                el,
                i,
                innerHTML,
                key;
            els = els || document.getElementsByTagName('*');
            if (!els.length) {
                els = [els];
            }
            for (i = 0; ; i += 1) {
                el = els[i];
                if (!el) {
                    break;
                }
                classes = el.className;
                if (/u-icon/.test(classes)) {
                    innerHTML = el.innerHTML;
                    if (innerHTML && innerHTML.length > 1) {
                        for (key in icons) {
                            if (icons.hasOwnProperty(key)) {
                                innerHTML = innerHTML.replace(new RegExp(key, 'g'), icons[key]);
                            }
                        }
                        el.innerHTML = innerHTML;
                    }
                }
            }
        };
        window.icomoonLiga();
    }
}());
